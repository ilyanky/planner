﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner
{
    public enum TaskStatus
    {
        Error,
        Complete,
        Processing,
        Waiting,
        Inactive,
        Refused
    }





    public sealed class ScheduleInfo
    {
        public int id { get; set; }



        public DateTime CreationDate { get; set; }



        private String _name = String.Empty;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value.Trim();
            }
        }

        private String _description = String.Empty;
        public string Description
        {
            get { return _description; }
            set
            {
                if ( value != null )
                    _description = value.Trim();
            }
        }

        public bool Active { get; set; }

        private String _triggerName = String.Empty;
        public string TriggerName
        {
            get { return _triggerName; }
            set
            {
                _triggerName = value.Trim();
            }
        }

        public DateTime StartDateTime { get; set; }

        private DateTime _endDate;
        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                if ( value != null )
                    _endDate = value;
            }
        }

        public int RepeatEvery { get; set; }

        private String _daysOfWeek;
        public string DaysOfWeek
        {
            get { return _daysOfWeek; }
            set
            {
                if ( value != null )
                    _daysOfWeek = value.Trim();
            }
        }

        private String _months;
        public string Months
        {
            get { return _months; }
            set
            {
                if ( value != null )
                    _months = value.Trim();
            }
        }

        private String _days;
        public string Days
        {
            get { return _days; }
            set
            {
                if ( value != null )
                    _days = value.Trim();
            }
        }

        private String _everyDay;
        public string EveryDay
        {
            get { return _everyDay; }
            set
            {
                if ( value != null )
                    _everyDay = value.Trim();
            }
        }

        public TaskStatus Status { get; set; }

        public static ScheduleInfo Empty
        {
            get
            {
                return new ScheduleInfo();
            }
        }

        public DateTime[] Dates { get; set; }


        public static int[] ParseDaysOfWeek(String days)
        {
            String temp = days.Replace(" ", "").Replace(",", "");
            int[] result = new int[temp.Length];
            for ( int i = 0; i < temp.Length; ++i )
                result[i] = Convert.ToInt16(temp.Substring(i, 1));

            return result;
        }


        public static String GetDaysOfWeek(int[] days)
        {
            String res = String.Empty;
            foreach ( var day in days ) {
                if ( day == 0 )
                    res += ", Понедельник";
                else if ( day == 1 )
                    res += ", Вторник";
                else if ( day == 2 )
                    res += ", Среда";
                else if ( day == 3 )
                    res += ", Четверг";
                else if ( day == 4 )
                    res += ", Пятница";
                else if ( day == 5 )
                    res += ", Суббота";
                else
                    res += ", Воскресенье";
            }

            if ( !res.Equals(String.Empty) )
                res = res.Remove(0, 2);

            return res;
        }


        public static bool DayOfWeekInRange(DateTime date, String daysOfWeek)
        {
            int[] range = ParseDaysOfWeek(daysOfWeek);
            foreach ( var day in range ) {
                if ( day == GetDayOfWeek(date) )
                    return true;
            }

            return false;
        }


        public String GetDetails()
        {
            if ( "Once".Equals(TriggerName.Trim()) ) {
                String res = "Запустить " + StartDateTime.ToShortDateString() +
                    " в " + StartDateTime.ToShortTimeString();
                return res;

            } else if ( "Daily".Equals(TriggerName.Trim()) ) {
                String res = "Начать с " + StartDateTime.ToShortDateString() +
                    " в " + StartDateTime.ToShortTimeString() + ". Запускать каждый " +
                    RepeatEvery.ToString() + " день до " + EndDate.ToString();
                return res;

            } else if ( "Weekly".Equals(TriggerName.Trim()) ) {
                String res = "Начать с " + StartDateTime.ToShortDateString() +
                    " в " + StartDateTime.ToShortTimeString() + ". Запускать каждую " +
                    RepeatEvery.ToString() + " неделю по следующим дням: " +
                    GetDaysOfWeek(ParseDaysOfWeek(DaysOfWeek)) +
                    ", до " + EndDate.ToString();
                return res;

            } else /* if ( "Monthly".Equals(task.TaskType) ) */
              {
                String res = String.Empty;
                if ( Days != null ) {
                    res = "Начать с " + StartDateTime.ToShortDateString() +
                        " в " + StartDateTime.ToShortTimeString() + ". Запускать каждый " +
                        Months + " месяц по след. числам " + Days +
                        ", до " + EndDate.ToShortDateString();
                } else {
                    res = "Начать с " + StartDateTime.ToShortDateString() +
                        " в " + StartDateTime.ToShortTimeString() + ". Запускать каждый " +
                        Months + " месяц в каждый " + EveryDay + " " +
                        GetDaysOfWeek(ParseDaysOfWeek(DaysOfWeek)) +
                        ", до " + EndDate.ToShortDateString();
                }

                return res;
            }
        }


        public static DateTime[] GetDates(ScheduleInfo sched, DateTime fromDt,
            DateTime toDt)
        {
            DateTime[] dates = new DateTime[0];
            if ( "Once".Equals(sched.TriggerName.Trim()) ) {

                if ( sched.StartDateTime < fromDt )
                    return dates;
                if ( sched.StartDateTime >= toDt )
                    return dates;
                //if ( !sched.Active ) аккуратно с этим условием - закоментил недавно
                //    return dates;

                dates = new DateTime[1];
                dates[0] = sched.StartDateTime;

            } else if ( "Daily".Equals(sched.TriggerName.Trim()) )
                dates = GetDailyTaskDates(sched, fromDt, toDt);
            else if ( "Weekly".Equals(sched.TriggerName.Trim()) )
                dates = GetWeeklyTaskDates(sched, fromDt, toDt);
            else /* if ( "Monthly".Equals(task.TaskType) ) */
                dates = GetMonthlyTaskDates(sched, fromDt, toDt);


            return dates;
        }


        public static DateTime[] GetDailyTaskDates(ScheduleInfo sched, DateTime fromDt,
            DateTime toDt)
        {
            var dateList = new LinkedList<DateTime>();

            //if ( !sched.Active )  аккуратно с этим условием - закоментил недавно
            //    return dateList.ToArray();

            var time = sched.StartDateTime;
            int days = GetDaysSpan(toDt, fromDt) + 1; // +1, потому что последнее
                                                      // число из диапазона тоже
                                                      // нужно рассматривать
            for ( int i = 0; i < days; ++i ) {
                DateTime date = fromDt.AddDays(i);
                int spanDays = GetDaysSpan(date, sched.StartDateTime);

                if ( spanDays < 0 )
                    continue;
                if ( spanDays > 0 && spanDays % sched.RepeatEvery != 0 )
                    continue;

                if ( CompareDate(date, sched.EndDate) >= 0 )
                    continue;

                if ( CompareDate(date, fromDt) == 0 &&
                        CompareTime(time, fromDt) < 0 )
                    continue;
                if ( CompareDate(date, toDt) == 0 &&
                        CompareTime(time, toDt) >= 0 )
                    continue;


                dateList.AddLast(new DateTime(date.Year, date.Month,
                    date.Day, time.Hour, time.Minute, time.Second));
            }

            return dateList.ToArray();
        }


        public static DateTime[] GetWeeklyTaskDates(ScheduleInfo sched, DateTime fromDt,
            DateTime toDt)
        {
            var dateList = new LinkedList<DateTime>();

            //if ( !sched.Active )  аккуратно с этим условием - закоментил недавно
            //    return dateList.ToArray();

            var time = sched.StartDateTime;
            int days = GetDaysSpan(toDt, fromDt) + 1; // +1, потому что последнее
                                                      // число из диапазона тоже
                                                      // нужно рассматривать
            for ( int i = 0; i < days; ++i ) {
                DateTime date = fromDt.AddDays(i);

                if ( !DayOfWeekInRange(date, sched.DaysOfWeek) )
                    continue;

                DateTime first = GetFirstDate(sched.StartDateTime,
                    GetDayOfWeek(date));
                int spanDays = GetDaysSpan(date, first);

                if ( spanDays < 0 )
                    continue;
                if ( spanDays > 0 && spanDays % sched.RepeatEvery != 0 )
                    continue;

                if ( CompareDate(date, sched.EndDate) >= 0 )
                    continue;

                if ( CompareDate(date, fromDt) == 0 &&
                        CompareTime(time, fromDt) < 0 )
                    continue;
                if ( CompareDate(date, toDt) == 0 &&
                        CompareTime(time, toDt) >= 0 )
                    continue;


                dateList.AddLast(new DateTime(date.Year, date.Month,
                        date.Day, time.Hour, time.Minute, time.Second));
            }

            return dateList.ToArray();
        }



        public static DateTime[] GetMonthlyTaskDates(ScheduleInfo sched, DateTime fromDt,
            DateTime toDt)
        {
            var dateList = new LinkedList<DateTime>();

            //if ( !sched.Active ) аккуратно с этим условием - закоментил недавно
            //    return dateList.ToArray();

            var time = sched.StartDateTime;
            int days = GetDaysSpan(toDt, fromDt) + 1; // +1, потому что последнее
                                                      // число из диапазона тоже
                                                      // нужно рассматривать
            for ( int i = 0; i < days; ++i ) {
                DateTime date = fromDt.AddDays(i);

                if ( date < sched.StartDateTime )
                    continue;

                if ( !DayInMonthRange(date, sched.Months) )
                    continue;

                if ( sched.Days != null ) {
                    if ( !DateInDaysRange(date, sched.Days) )
                        continue;
                } else {
                    if ( !DayOfWeekInRange(date, sched.DaysOfWeek) )
                        continue;

                    if ( !DateInEveryRange(date, sched.EveryDay) )
                        continue;
                }

                if ( CompareDate(date, sched.EndDate) >= 0 )
                    continue;

                if ( CompareDate(date, fromDt) == 0 &&
                        CompareTime(time, fromDt) < 0 )
                    continue;

                if ( CompareDate(date, toDt) == 0 &&
                        CompareTime(time, toDt) >= 0 )
                    continue;



                dateList.AddLast(new DateTime(date.Year, date.Month,
                        date.Day, time.Hour, time.Minute, time.Second));
            }


            //Console.WriteLine("------------------------------------------");
            //foreach ( var item in dateList ) {
            //    Console.WriteLine(item.ToString());
            //}
            //Console.WriteLine("------------------------------------------");

            return dateList.ToArray();
        }



        public static bool DayInMonthRange(DateTime date, String months)
        {
            int[] range = ParseMonths(months);
            foreach ( var month in range ) {
                if ( month == GetMonth(date) )
                    return true;
            }

            return false;
        }



        public static int[] ParseMonths(String months)
        {
            var temp = months.Replace(" ", "").Split(',');
            var result = new int[temp.Length];
            for ( int i = 0; i < temp.Length; ++i )
                result[i] = Convert.ToInt16(temp[i]);

            return result;
        }



        public static int GetMonth(DateTime date)
        {
            var calendar = new GregorianCalendar();
            return calendar.GetMonth(date);
        }



        public static bool DateInDaysRange(DateTime date, String days)
        {
            int[] range = ParseDaysOfMonth(days);
            foreach ( var day in range ) {
                if ( day == -1 && LastDayInMonth(date) )
                    return true;

                if ( day == GetDayOfMonth(date) )
                    return true;
            }

            return false;
        }



        public static int[] ParseDaysOfMonth(String days)
        {
            var temp = days.Replace(" ", "").Split(',');
            int[] result = new int[temp.Length];
            for ( int i = 0; i < temp.Length; ++i )
                result[i] = Convert.ToInt16(temp[i]);

            return result;
        }



        public static int GetDayOfMonth(DateTime date)
        {
            var calendar = new GregorianCalendar();
            return calendar.GetDayOfMonth(date);
        }


        public static bool LastDayInMonth(DateTime date)
        {
            var calendar = new GregorianCalendar();
            return date.Day == calendar.GetDaysInMonth(date.Year, date.Month);
        }



        public static int GetDayOfWeekCount(DateTime dayOfWeek)
        {
            var calendar = new GregorianCalendar();
            var day = calendar.GetDayOfWeek(dayOfWeek);

            int first = 1;
            for ( int i = 1; i < 8; ++i ) {
                var date = new DateTime(dayOfWeek.Year, dayOfWeek.Month, i);
                if ( calendar.GetDayOfWeek(date) == day )
                    first = i;
            }


            int count = 0;
            int days = calendar.GetDaysInMonth(dayOfWeek.Year, dayOfWeek.Month);
            for ( int i = first; i <= days; i += 7 )
                ++count;

            return count;
        }



        public static int GetDayOfWeekNumberInMonth(DateTime dayOfWeek)
        {
            var calendar = new GregorianCalendar();

            int count = -1;
            int days = calendar.GetDaysInMonth(dayOfWeek.Year, dayOfWeek.Month);
            for ( int i = dayOfWeek.Day; i <= days; i += 7 )
                ++count;

            return GetDayOfWeekCount(dayOfWeek) - count;
        }



        public static bool LastDayOfWeekInMonth(DateTime dayOfWeek)
        {
            return GetDayOfWeekNumberInMonth(dayOfWeek) == GetDayOfWeekCount(dayOfWeek);
        }



        public static bool DateInEveryRange(DateTime date, String everyDay)
        {
            int[] range = ParseEveryDay(everyDay);
            foreach ( var d in range ) {
                if ( d == -1 && LastDayOfWeekInMonth(date) )
                    return true;

                if ( d == GetDayOfWeekNumberInMonth(date) )
                    return true;
            }

            return false;
        }


        public static int[] ParseEveryDay(String everyDay)
        {
            var temp = everyDay.Replace(" ", "").Split(',');
            int[] result = new int[temp.Length];
            for ( int i = 0; i < temp.Length; ++i )
                result[i] = Convert.ToInt16(temp[i]);

            return result;
        }



        public static DateTime GetFirstDate(DateTime start, int dayOfWeek)
        {
            DateTime temp = start;
            while ( GetDayOfWeek(temp) != dayOfWeek )
                temp = temp.AddDays(1);

            //Console.WriteLine(temp.Day);
            return temp;
        }


        public static int GetDayOfWeek(DateTime date)
        {
            var calendar = new GregorianCalendar();
            DayOfWeek day = calendar.GetDayOfWeek(date);

            if ( day == DayOfWeek.Sunday )
                return 6;

            int d = (int)day;
            return --d;
        }


        public static int GetDaysSpan(DateTime one, DateTime two)
        {
            // поправить так, чтобы последователость аргументов была неважна

            DateTime one_onlyDate =
                new DateTime(one.Year, one.Month, one.Day);
            DateTime two_onlyDate =
                new DateTime(two.Year, two.Month, two.Day);

            return (one_onlyDate - two_onlyDate).Days;
        }


        public static int CompareTime(DateTime one, DateTime two)
        {
            DateTime one_time =
                new DateTime(1, 1, 1, one.Hour, one.Minute, one.Second);
            DateTime two_time =
                new DateTime(1, 1, 1, two.Hour, two.Minute, two.Second);

            return one_time.CompareTo(two_time);
        }


        public static int CompareDate(DateTime one, DateTime two)
        {
            DateTime one_date =
                new DateTime(one.Year, one.Month, one.Day, 1, 1, 1);
            DateTime two_date =
                new DateTime(two.Year, two.Month, two.Day, 1, 1, 1);

            return one_date.CompareTo(two_date);
        }



        public override string ToString()
        {
            String temp =
                "id: " + id.ToString() + "\n" +
            "Name: " + Name.Trim() + "\n" +
            "Description: " + Description.Trim() + "\n" +
            "Status: " + Status + "\n" +
            "Active: " + Active.ToString() + "\n" +
            "TriggerName: " + TriggerName.Trim() + "\n" +
            "StartDateTime: " + StartDateTime + "\n" +
            "EndDate: " + EndDate + "\n" +
            "RepeatEvery: " + RepeatEvery.ToString() + "\n" +
            "DaysOfWeek: " + DaysOfWeek + "\n" +
            "Days: " + Days + "\n" +
            "Months: " + Months + "\n" +
            "EveryDay: " + EveryDay + "\n";

            return temp;
        }

    }







    public sealed class TaskInfo
    {


        public int id { get; set; }


        private List<ScheduleInfo> _schedules;
        public ScheduleInfo[] Schedules
        {
            get { return _schedules.ToArray(); }
        }

        private String _name = String.Empty;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value.Trim();
            }
        }

        private String _description = String.Empty;
        public string Description
        {
            get { return _description; }
            set
            {
                if ( value != null )
                    _description = value.Trim();
            }
        }

        public bool Active { get; set; }

        private String _businessTaskName = String.Empty;
        public string BusinessTaskName
        {
            get { return _businessTaskName; }
            set
            {
                _businessTaskName = value.Trim();
            }
        }

        public TaskStatus Status { get; set; }
        public DateTime[] Dates { get; set; }
        public List<float> PerfomanceList { get; set; }

        private String _userName = String.Empty;
        public String UserName
        {
            get { return _userName; }
            set
            {
                _userName = value.Trim();
            }
        }

        public DateTime CreationDate { get; set; }


        public TaskInfo()
        {
            _schedules = new List<ScheduleInfo>();

            Name = String.Empty;
            Description = String.Empty;
            BusinessTaskName = String.Empty;
            PerfomanceList = new List<float>();
            UserName = String.Empty;
        }


        public void AddSchedule(ScheduleInfo schedule)
        {
            _schedules.Add(schedule);
        }


        public static DateTime[] GetDates(TaskInfo taskInfo, DateTime fromDt,
            DateTime toDt)
        {
            var list = new List<DateTime>();
            foreach ( var sched in taskInfo.Schedules )
                list.AddRange(ScheduleInfo.GetDates(sched, fromDt, toDt));

            return list.ToArray();
        }


        public DateTime[] GetDates()
        {
            var list = new List<DateTime>();
            foreach ( var sched in Schedules )
                list.AddRange(sched.Dates);

            return list.ToArray();
        }



        public static DateTime GetLastDateTime(TaskInfo task, DateTime day)
        {
            DateTime fromDt = new DateTime(day.Year, day.Month, day.Day, 0, 0, 0);
            DateTime toDt = fromDt.AddDays(1);
            var dates = GetDates(task, fromDt, toDt);
            if ( dates.Length == 0 )
                return DateTime.MinValue;

            DateTime last = dates.First();
            foreach ( var date in dates ) {
                if ( date > last )
                    last = date;
            }

            return last;
        }



        public override string ToString()
        {
            String temp =
                "id: " + id.ToString() + "\n" +
                   "Name: " + Name.Trim() + "\n" +
                   "Description: " + Description + "\n" +
                   "BusinessTaskName: " + BusinessTaskName.Trim() + "\n" +
                   "Active: " + Active.ToString() + "\n" +
                   "Status: " + Status + "\n" +
                   "UserName " + UserName + "\n" +
                   "Dates:\n";

            if ( Dates != null )
                foreach ( var date in Dates )
                    temp += date + "\n";

            temp += "Schedules: \n";
            if ( Schedules != null )
                foreach ( var sched in Schedules )
                    temp += sched.ToString() + "\n";

            return temp;
        }
    }
}