﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using System.Data.Linq;

namespace Planner
{
    public static class DataBaseAccess
    {
        private static readonly Debit_servDataContext _debitServ;
        private static readonly Sched_servDataContext _schedServ;
        private static readonly Object _lockObj;

        static DataBaseAccess()
        {
            var builder = new SqlConnectionStringBuilder();
            builder.DataSource = Config.GetDataSource();
            builder.InitialCatalog = Config.GetDatabase();
            builder.IntegratedSecurity = true;
            //            builder.UserID = "access";
            //            builder.Password = "777";

            _debitServ = new Debit_servDataContext(builder.ConnectionString);
            _schedServ = new Sched_servDataContext();

            _lockObj = new Object();
        }


        public static String GetSalt(String usrName)
        {
            var usr = from elem in _debitServ.USERS
                      where elem.UserName == usrName
                      select elem.Salt;

            if ( usr.Any() )
                return usr.First();

            return null;
        }


        public static String GetHashedPassword(String usrName)
        {
            var usr = from elem in _debitServ.USERS
                      where elem.UserName == usrName
                      select elem.HashedPassword;

            if ( usr.Any() )
                return usr.First();

            return null;
        }


        public static bool Auth(String usrName, String pswd)
        {
            //            String salt = String.Empty;
            //            salt = GetSalt(usrName);

            //            if (salt == null)
            //            {
            //                Console.WriteLine("Debug: there is no such user");
            //                return false;
            //            }
            //
            //            String rawSalted = salt + pswd.Trim();

            //            SHA1 sha1 = new SHA1CryptoServiceProvider();
            //            byte[] bytes = sha1.ComputeHash(
            //                Encoding.Unicode.GetBytes(rawSalted));
            //            String hashedPswd = Convert.ToBase64String(bytes);

            //            return hashedPswd == GetHashedPassword(usrName);

            String hashedPswd = GetHashedPassword(usrName.Trim());
            if ( hashedPswd == null )
                return false;
            return pswd.Equals(hashedPswd.Trim());
        }


        public static TaskInfo AddTask(TaskInfo taskInfo, StringBuilder errorMsg)
        {
            lock ( _lockObj ) {
                // провека на то, нет ли уже расписаний с таким именем

                var userId = from elem in _debitServ.USERS
                             where elem.UserName.Trim() == taskInfo.UserName
                             select elem.id_Polz;
                int userID = userId.First();
                var businessTaskId = from elem in _schedServ.BusinessTasks
                                     where elem.Name.Trim() == taskInfo.BusinessTaskName
                                        && !elem.Deleted
                                     select elem.id;

                DateTime now = DateTime.Now;
                Task task = new Task {
                    Name = taskInfo.Name,
                    Description = taskInfo.Description,
                    Date_create = now,
                    Active = taskInfo.Active,
                    UserID = userID,
                    BusinessTaskID = businessTaskId.First()
                };

                _schedServ.Tasks.InsertOnSubmit(task);
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    errorMsg.Append(e.ToString());
                    return null;
                }


                var taskQuery = from elem in _schedServ.Tasks
                                where elem.Name.Trim() == taskInfo.Name &&
                                   elem.Date_create == now && !elem.Deleted
                                select elem;
                var temp = taskQuery.FirstOrDefault();
                if ( temp == null ) {
                    errorMsg.Append("Возникла ошибка при добавлении задачи с именем "
                        + taskInfo.Name);
                    LogServerInfo("Возникла ошибка при добавлении задачи с именем "
                        + taskInfo.Name + " пользователем с именем " + taskInfo.UserName);
                    return null;
                }


                String descr = "Добавлена задача с именем " + taskInfo.Name
                    + " пользователем " + taskInfo.UserName;
                LogAddedTask(taskInfo.UserName, temp.id, descr, errorMsg);

                var res = GetTaskInfo(temp);
                var tempList = new List<ScheduleInfo>();
                foreach ( var sched in taskInfo.Schedules ) {
                    var tmp = AddShedule(sched, temp.id, errorMsg);
                    if ( tmp == null ) {
                        DeleteIfError(tempList);
                        DeleteIfError(temp.id);
                        return null;
                    }
                    tempList.Add(tmp);
                    res.AddSchedule(tmp);
                }

                return res;
            }
        }



        private static void DeleteIfError(List<ScheduleInfo> scheds)
        {
            lock ( _lockObj ) {
                foreach ( var sched in scheds ) {
                    var temp = from elem in _schedServ.Schedules
                               where sched.id == elem.id //&& !elem.Deleted
                               select elem;
                    if ( temp.Count() <= 0 )
                        continue;

                    _schedServ.Schedules.DeleteOnSubmit(temp.First());
                }

                _schedServ.SubmitChanges();
            }
        }


        private static void DeleteIfError(int taskID)
        {
            lock ( _lockObj ) {
                var userLog = from elem in _schedServ.UserLogs
                              where elem.id == taskID //&& !elem.Tasks.Deleted
                              select elem;
                if ( userLog.Count() <= 0 )
                    return;

                _schedServ.UserLogs.DeleteAllOnSubmit(userLog);
                _schedServ.SubmitChanges();

                var task = from elem in _schedServ.Tasks
                           where elem.id == taskID //&& !elem.Deleted
                           select elem;
                _schedServ.Tasks.DeleteOnSubmit(task.First());
                _schedServ.SubmitChanges();
            }
        }




        private static bool LogAddedTask(String userName, int taskID, String descr,
            StringBuilder errorMsg)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.UserLogTypes
                           where "TaskAdded" == elem.Name.Trim()
                           select elem.id;
                var userId = from elem in _debitServ.USERS
                             where elem.UserName.Trim() == userName
                             select elem.id_Polz;

                UserLog log = new UserLog {
                    UserID = userId.First(),
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    TaskID = taskID,
                    Description = descr
                };

                _schedServ.UserLogs.InsertOnSubmit(log);
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    errorMsg.Append(e.ToString());
                    return false;
                }

                return true;
            }
        }


        public static ScheduleInfo AddShedule(ScheduleInfo schedInfo,
            int taskID, StringBuilder errorMsg)
        {
            lock ( _lockObj ) {

                var task = from elem in _schedServ.Tasks
                           where taskID == elem.id && !elem.Deleted
                           select elem;
                Task temp = task.FirstOrDefault();
                if ( temp == null ) {
                    errorMsg.Append("Ошибка при добавлении нового расписания к задаче." +
                        "c id = " + taskID + ". Возможно, такая задача не существует!");
                    return null;
                }


                var taskInfo = GetTaskInfo(temp);
                var triggerId = from elem in _schedServ.TriggerTypes
                                where elem.Name.Trim() == schedInfo.TriggerName
                                select elem.id;

                DateTime now = DateTime.Now;
                Schedule newShedule = GetSchedule(schedInfo, (short)triggerId.First(), taskID, now);
                _schedServ.Schedules.InsertOnSubmit(newShedule);
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    errorMsg.Append(e.ToString());
                    return null;
                }


                var schedQuery = from elem in _schedServ.Schedules
                                 where elem.Name.Trim() == schedInfo.Name &&
                                     elem.Date_create == now
                                 select elem;
                var sched = schedQuery.First();

                String descr = "Добавлено расписание с именем " + schedInfo.Name
                    + " и id = " + sched.id + " пользователем " + taskInfo.UserName;
                LogAddedSchedule(taskInfo.UserName, (short)task.First().id, descr, errorMsg);

                return GetScheudleInfo(sched);
            }
        }


        private static bool LogAddedSchedule(String userName, int taskID, String descr,
            StringBuilder errorMsg)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.UserLogTypes
                           where "ScheduleAdded" == elem.Name.Trim()
                           select elem.id;
                var userId = from elem in _debitServ.USERS
                             where elem.UserName.Trim() == userName
                             select elem.id_Polz;

                UserLog log = new UserLog {
                    UserID = userId.First(),
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    TaskID = taskID,
                    Description = descr
                };

                _schedServ.UserLogs.InsertOnSubmit(log);
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    errorMsg.Append(e.ToString());
                    return false;
                }


                return true;
            }
        }



        public static TaskInfo[] GetTasks(DateTime fromDt, DateTime toDt,
            bool onlyActiveTasks, bool onlyActiveSchedules)
        {
            lock ( _lockObj ) {

                var result = new LinkedList<TaskInfo>();

                IQueryable<Schedule> relevantSchedules;
                if ( onlyActiveTasks && onlyActiveSchedules ) {
                    relevantSchedules = from elem in _schedServ.Schedules
                                        where elem.StartDateTime < toDt &&
                                            elem.Task.Active && elem.Active &&
                                            !elem.Deleted
                                        select elem;
                } else if ( !onlyActiveTasks && onlyActiveSchedules ) {
                    relevantSchedules = from elem in _schedServ.Schedules
                                        where elem.StartDateTime < toDt &&
                                            elem.Active && !elem.Deleted
                                        select elem;
                } else if ( onlyActiveTasks && !onlyActiveSchedules ) {
                    relevantSchedules = from elem in _schedServ.Schedules
                                        where elem.StartDateTime < toDt &&
                                            elem.Task.Active && !elem.Deleted
                                        select elem;
                } else {
                    relevantSchedules = from elem in _schedServ.Schedules
                                        where elem.StartDateTime < toDt &&
                                            !elem.Deleted
                                        select elem;
                }


                var map = new Dictionary<int, TaskInfo>();
                foreach ( var sched in relevantSchedules ) {
                    if ( map.ContainsKey(sched.TaskID) )
                        map[sched.TaskID].AddSchedule(
                            GetScheudleInfo(sched));
                    else {
                        var temp = GetTaskInfo(sched);
                        temp.AddSchedule(GetScheudleInfo(sched));
                        map[sched.TaskID] = temp;
                    }
                }


                var relevantTasks = map.Values.ToArray();
                foreach ( var task in relevantTasks ) {
                    foreach ( var sched in task.Schedules ) {
                        sched.Dates = ScheduleInfo.
                            GetDates(sched, fromDt, toDt);
                    }
                    task.Dates = task.GetDates();
                    if ( task.Dates.Length <= 0 )
                        continue;
                    result.AddLast(task);
                }


                return result.ToArray();
            }
        }



        public static TaskInfo GetTask(int taskID)
        {
            lock ( _lockObj ) {

                var resultQuery = from elem in _schedServ.Tasks
                                  where elem.id == taskID && !elem.Deleted
                                  select elem;
                var result = resultQuery.FirstOrDefault();
                if ( result == null )
                    return null;

                return GetTaskInfo(result);
            }
        }



        public static String GetUserName(int id)
        {
            lock ( _lockObj ) {
                return _debitServ.USERS.First((x) => x.id_Polz == id).
                    UserName;
            }
        }


        private static Schedule GetSchedule(ScheduleInfo schedule,
            int triggerId, int taskId, DateTime now)
        {
            Schedule newSched = new Schedule {
                Name = schedule.Name,
                Description = schedule.Description,
                Active = schedule.Active,
                TriggerID = triggerId,
                StartDateTime = schedule.StartDateTime,
                DaysOfWeek = schedule.DaysOfWeek,
                Months = schedule.Months,
                Days = schedule.Days,
                TaskID = taskId,
                EveryDay = schedule.EveryDay,
                Date_create = now
            };


            if ( "Once".Equals(schedule.TriggerName) ) {
                newSched.EndDate = null;
                newSched.RepeatEvery = null;
            } else {
                newSched.EndDate = schedule.EndDate;
                newSched.RepeatEvery = (short)schedule.RepeatEvery;
            }

            return newSched;
        }


        public static ScheduleInfo GetScheudleInfo(
            Schedule sched)
        {
            var temp = new ScheduleInfo();
            temp.id = sched.id;
            temp.Name = sched.Name;
            temp.Description = sched.Description;
            temp.Active = sched.Active;
            temp.TriggerName = sched.TriggerType.Name;
            temp.StartDateTime = sched.StartDateTime;
            if ( sched.EndDate != null )
                temp.EndDate = (DateTime)sched.EndDate;
            if ( sched.RepeatEvery != null )
                temp.RepeatEvery = (int)sched.RepeatEvery;
            temp.DaysOfWeek = sched.DaysOfWeek;
            temp.Months = sched.Months;
            temp.Days = sched.Days;
            temp.EveryDay = sched.EveryDay;
            temp.Status = TaskStatus.Waiting;

            return temp;
        }


        public static TaskInfo GetTaskInfo(Schedule sched)
        {
            return GetTaskInfo(sched.Task);
        }



        public static TaskInfo GetTaskInfo(ScheduleInfo schedule)
        {
            lock ( _lockObj ) {
                var taskQuery = from elem in _schedServ.Schedules
                                where elem.id == schedule.id
                                select elem;
                var task = taskQuery.FirstOrDefault();
                if ( task == null )
                    return null;

                return GetTaskInfo(task);
            }
        }



        public static TaskInfo GetTaskInfo(Task task)
        {
            var temp = new TaskInfo();
            temp.id = task.id;
            temp.Name = task.Name;
            temp.Description = task.Description;
            temp.Active = task.Active;
            temp.BusinessTaskName = task.BusinessTask.Name;
            temp.CreationDate = task.Date_create;
            temp.UserName = GetUserName(task.UserID);
            temp.Status = TaskStatus.Waiting;

            return temp;
        }


        public static bool IsValid(int taskID, int schedID)
        {
            lock ( _lockObj ) {
                var task = from elem in _schedServ.Tasks
                           where taskID == elem.id && !elem.Deleted &&
                            elem.Active
                           select elem;
                if ( task.Count() <= 0 )
                    return false;


                var sched = from elem in _schedServ.Schedules
                            where schedID == elem.id && !elem.Deleted &&
                                elem.Active
                            select elem;
                if ( sched.Count() <= 0 )
                    return false;

                return true;
            }
        }



        private static bool LogDeletedTask(String userName, int id, String descr,
            StringBuilder errorMsg)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.UserLogTypes
                           where "TaskDeleted" == elem.Name.Trim()
                           select elem.id;
                var userId = from elem in _debitServ.USERS
                             where elem.UserName.Trim() == userName
                             select elem.id_Polz;

                UserLog log = new UserLog {
                    UserID = userId.First(),
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    TaskID = id,
                    Description = descr
                };

                _schedServ.UserLogs.InsertOnSubmit(log);
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    errorMsg.Append(e.ToString());
                    return false;
                }

                return true;
            }
        }



        public static bool LogUserEnter(String userName, String descr,
            StringBuilder errorMsg)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.UserLogTypes
                           where "LoggedIn" == elem.Name.Trim()
                           select elem.id;
                var userId = from elem in _debitServ.USERS
                             where elem.UserName.Trim() == userName
                             select elem.id_Polz;

                UserLog log = new UserLog {
                    UserID = userId.First(),
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    TaskID = null,
                    Description = descr
                };

                _schedServ.UserLogs.InsertOnSubmit(log);
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    errorMsg.Append(e.ToString());
                    //LogServerInfo("Ошибка изменения бд. Exception: " + e);
                    return false;
                }

                return true;
            }
        }



        public static bool LogUserExit(String userName, String descr,
            StringBuilder errorMsg)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.UserLogTypes
                           where "LoggedOut" == elem.Name.Trim()
                           select elem.id;
                var userId = from elem in _debitServ.USERS
                             where elem.UserName.Trim() == userName
                             select elem.id_Polz;

                UserLog log = new UserLog {
                    UserID = userId.First(),
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    TaskID = null,
                    Description = descr
                };

                _schedServ.UserLogs.InsertOnSubmit(log);
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    errorMsg.Append(e.ToString());
                    //LogServerInfo("Ошибка изменения бд. Exception: " + e);
                    return false;
                }

                return true;
            }
        }



        private static bool LogDeletedSchedule(String userName, int taskID,
            String descr, StringBuilder errorMsg)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.UserLogTypes
                           where "ScheduleDeleted" == elem.Name.Trim()
                           select elem.id;
                var userId = from elem in _debitServ.USERS
                             where elem.UserName.Trim() == userName
                             select elem.id_Polz;

                UserLog log = new UserLog {
                    UserID = userId.First(),
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    TaskID = taskID,
                    Description = descr
                };

                _schedServ.UserLogs.InsertOnSubmit(log);
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    errorMsg.Append(e.ToString());
                    return false;
                }

                return true;
            }
        }


        public static bool DeleteTask(String userName, int taskID,
            StringBuilder errorMsg)
        {
            lock ( _lockObj ) {

                var taskQuery = from elem in _schedServ.Tasks
                                where taskID == elem.id && !elem.Deleted
                                select elem;
                var task = taskQuery.FirstOrDefault();
                if ( task == null ) {
                    errorMsg.Append("Выбранная задача не найдена, либо была уже удалена");
                    LogServerInfo("Задача с id = " + taskID + " не была найдена." +
                        " Вызвана функция на удаление этой задачи");
                    return false;
                }

                var scheds = from elem in _schedServ.Schedules
                             where task.id == elem.Task.id && !elem.Deleted
                             select elem;
                foreach ( var sched in scheds )
                    DeleteSchedule(userName, sched.id, taskID, errorMsg);


                task.Deleted = true;
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    errorMsg.Append("Не удалось удалить задачу с id = " +
                        task.id + "Подробнее: \n" + e);
                    //LogTaskActivity(task.id,
                    //    "Не удалось удалить задачу с id = " +
                    //    task.id + "Подробнее: \n" + e);
                    return false;
                }

                String descr = "Пользователь " + userName +
                    " удалил задачу с именем " + task.Name;
                LogDeletedTask(userName, task.id, descr, errorMsg);
                return true;
            }
        }


        public static bool DeleteSchedule(String userName, int scheduleID,
            int taskID, StringBuilder errorMsg)
        {
            lock ( _lockObj ) {
                var schedQuery = from elem in _schedServ.Schedules
                                 where scheduleID == elem.id && !elem.Deleted
                                 select elem;
                var sched = schedQuery.FirstOrDefault();
                if ( sched == null ) {
                    errorMsg.Append("Возникла ошибка при удалении расписания с id = " +
                        scheduleID);
                    LogTaskActivity(taskID, "Возникла ошибка при удалении расписания с id = " +
                        scheduleID);
                    return false;
                }


                sched.Deleted = true;
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    errorMsg.Append("Не удалось удалить расписание с id = " +
                        sched.id + "Подробнее: \n" + e);
                    //LogTaskActivity(sched.TaskID,
                    //    "Не удалось удалить расписание с id = " +
                    //    sched.id + "Подробнее: \n" + e);
                    return false;
                }

                String descr = "Пользователь " + userName +
                    " удалил расписание с именем " + sched.Name.Trim() +
                    " задачи " + sched.Task.Name.Trim();
                LogDeletedSchedule(userName, taskID, descr, errorMsg);
                return true;
            }
        }


        public static bool LogStartedTask(int taskID, int scheduleID, String businessTaskName)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.TaskLogTypes
                           where "Started" == elem.Name.Trim()
                           select elem.id;
                var schedQuery = from elem in _schedServ.Schedules
                                 where scheduleID == elem.id && !elem.Deleted
                                 select elem;
                var temp = schedQuery.FirstOrDefault();
                if ( temp == null ) {
                    return false;
                }

                String description = "Запущен процесс 'TaskExecutor' с параметром " +
                    businessTaskName;
                DateTime now = DateTime.Now;
                TaskLog log = new TaskLog {
                    TaskID = taskID,
                    ScheduleID = scheduleID,
                    DT = now,
                    TypeID = type.First(),
                    Description = description
                };
                _schedServ.TaskLogs.InsertOnSubmit(log);
                _schedServ.SubmitChanges();

                return true;
            }
        }



        public static void LogTaskActivity(int id, String description)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.TaskLogTypes
                           where "Activity" == elem.Name.Trim()
                           select elem.id;
                //var task = from elem in _schedServ.Tasks
                //           where taskName == elem.Name.Trim() && !elem.Deleted
                //           select elem.id;

                DateTime now = DateTime.Now;
                TaskLog log = new TaskLog {
                    TaskID = id,
                    DT = now,
                    TypeID = type.First(),
                    Description = description
                };
                _schedServ.TaskLogs.InsertOnSubmit(log);
                _schedServ.SubmitChanges();
            }
        }


        public static void LogTaskActivity(String taskName, String description)
        {
            lock ( _lockObj ) {
                var task = from elem in _schedServ.Tasks
                           where taskName == elem.Name.Trim() && !elem.Deleted
                           select elem.id;
                LogTaskActivity(task.First(), description);
            }
        }


        public static void LogUpdaterActivity(int taskID, String description)
        {
            lock ( _lockObj ) {
                var task = from elem in _schedServ.Tasks
                           where taskID == elem.id && !elem.Deleted
                           select elem;
                var temp = task.FirstOrDefault();
                if ( temp == null )
                    return;

                var type = from elem in _schedServ.TaskLogTypes
                           where "EveryDayUpdaterActivity" == elem.Name.Trim()
                           select elem.id;

                DateTime now = DateTime.Now;
                TaskLog log = new TaskLog {
                    TaskID = temp.id,
                    DT = now,
                    TypeID = type.First(),
                    Description = description
                };
                _schedServ.TaskLogs.InsertOnSubmit(log);
                _schedServ.SubmitChanges();
            }
        }



        public static void LogFinishedTask(int taskID, int scheduleID, int exitCode, DateTime startDT)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.TaskLogTypes
                           where "Finished" == elem.Name.Trim()
                           select elem.id;
                var schedQuery = from elem in _schedServ.Schedules
                                 where scheduleID == elem.id && !elem.Deleted
                                 select elem;
                var temp = schedQuery.FirstOrDefault();
                if ( temp == null )
                    return;

                String description = startDT.ToString();
                TaskLog log = new TaskLog {
                    TaskID = taskID,
                    ScheduleID = scheduleID,
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    Description = description,
                    ExitCode = (short)exitCode
                };
                _schedServ.TaskLogs.InsertOnSubmit(log);
                _schedServ.SubmitChanges();
            }
        }



        public static void LogServerInfo(String descr)
        {
            lock ( _lockObj ) {
                ServerLog log = new ServerLog {
                    DT = DateTime.Now,
                    Description = descr
                };
                _schedServ.ServerLogs.InsertOnSubmit(log);
                _schedServ.SubmitChanges();
            }
        }



        public static void LogOutputDataReceived(int id, String data)
        {
            lock ( _lockObj ) {
                Console.WriteLine("output data received");


                var type = from elem in _schedServ.TaskLogTypes
                           where "OutputDataReceived" == elem.Name.Trim()
                           select elem.id;
                String description = "Данные: " + data;

                TaskLog log = new TaskLog {
                    TaskID = id,
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    Description = description,
                    ExitCode = null
                };
                _schedServ.TaskLogs.InsertOnSubmit(log);
                _schedServ.SubmitChanges();
            }
        }


        public static void LogErrorDataReceived(int id, String data)
        {
            lock ( _lockObj ) {
                Console.WriteLine("error data received");


                var type = from elem in _schedServ.TaskLogTypes
                           where "ErrorDataReceived" == elem.Name.Trim()
                           select elem.id;
                String description = "Ошибка: " + data;

                TaskLog log = new TaskLog {
                    TaskID = id,
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    Description = description,
                    ExitCode = null
                };
                _schedServ.TaskLogs.InsertOnSubmit(log);
                _schedServ.SubmitChanges();
            }
        }



        private static bool InOneMinuteRange(
            DateTime rangePoint, DateTime dateTime)
        {
            DateTime from = rangePoint.AddMinutes(-1);
            DateTime to = rangePoint.AddMinutes(1);

            int first = ScheduleInfo.CompareTime(dateTime, from);
            int second = ScheduleInfo.CompareTime(dateTime, to);

            return first >= 0 && second <= 0;
        }


        public static LinkedList<TaskInfo> GetStatuses(DateTime fromDt, DateTime toDt)
        {
            lock ( _lockObj ) {
                var tasks = new LinkedList<TaskInfo>();
                var log = from elem in _schedServ.TaskLogs
                          where "Finished" == elem.TaskLogType.Name.Trim() &&
                            elem.DT >= fromDt && elem.DT <= toDt
                          select elem;
                foreach ( var logElem in log ) {
                    var tempScheds = logElem.Task.Schedules;
                    ScheduleInfo temp = null;
                    foreach ( var sched in tempScheds ) {
                        if ( !InOneMinuteRange(logElem.DT, sched.StartDateTime) )
                            continue;
                        temp = GetScheudleInfo(sched);
                        break;
                    }
                    if ( temp == null )
                        continue;

                    if ( logElem.ExitCode == 0 )
                        temp.Status = TaskStatus.Complete;
                    else
                        temp.Status = TaskStatus.Error;
                    DateTime[] arr = new DateTime[1];
                    arr[0] = new DateTime(logElem.DT.Year, logElem.DT.Month,
                        logElem.DT.Day, temp.StartDateTime.Hour,
                        temp.StartDateTime.Minute, temp.StartDateTime.Second);
                    temp.Dates = arr;

                    TaskInfo tempTask;
                    try {
                        tempTask = tasks.First(
                            (x) => x.id == logElem.Task.id
                        );
                        tempTask.AddSchedule(temp);
                    } catch ( InvalidOperationException e ) {
                        tempTask = new TaskInfo {
                            id = logElem.Task.id,
                        };
                        tempTask.AddSchedule(temp);
                        tasks.AddLast(tempTask);
                    }
                }

                foreach ( var task in tasks ) {
                    task.Dates = task.GetDates();
                    //task.Status = task.Schedules[task.Schedules.Length - 1].Status;
                }

                return tasks;
            }
        }



        public static TaskStatus GetTaskStatus(int taskID, DateTime dateTime)
        {
            lock (_lockObj) {
                var entries = from elem in _schedServ.TaskLogs
                              where "Finished" == elem.TaskLogType.Name.Trim() &&
                                  elem.Task.id == taskID
                              select elem;

                TaskLog result = null;
                foreach ( var item in entries ) {
                    if ( DateTime.Parse(item.Description.Trim()) == dateTime )
                        result = item;
                }
                if ( result == null )
                    return TaskStatus.Waiting;

                if ( result.ExitCode == 0 )
                    return TaskStatus.Complete;
                else
                    return TaskStatus.Error;
            }
        }



        public static UserLogInfo[] GetUserLog(String userName)
        {
            lock ( _lockObj ) {
                var user = from elem in _debitServ.USERS
                           where elem.UserName.Trim() == userName
                           select elem.id_Polz;
                var log = from elem in _schedServ.UserLogs
                          where elem.UserID == user.First()
                          select elem;

                var res = new List<UserLogInfo>();
                foreach ( var item in log ) {
                    UserLogInfo info = new UserLogInfo();
                    info.UserName = userName;
                    info.DateTime = item.DT;
                    info.TypeName = item.UserLogType.Name.Trim();
                    var task = item.Task;
                    if ( task != null )
                        info.TaskName = task.Name.Trim();
                    else
                        info.TaskName = "null";
                    info.Description = item.Description.Trim();
                    res.Add(info);
                }

                return res.ToArray();
            }
        }


        public static TaskLogInfo[] GetTaskLog(int taskID)
        {
            lock ( _lockObj ) {
                var log = from elem in _schedServ.TaskLogs
                          where elem.Task.id == taskID //&& !elem.Task.Deleted
                          select elem;

                var res = new List<TaskLogInfo>();
                foreach ( var item in log ) {
                    TaskLogInfo info = new TaskLogInfo();
                    info.TaskID = taskID;
                    info.TaskName = item.Task.Name.Trim();
                    info.DateTime = item.DT;
                    info.TypeName = item.TaskLogType.Name.Trim();
                    info.Description = item.Description.Trim();
                    if ( item.ExitCode == null )
                        info.ExitCode = "null";
                    else
                        info.ExitCode = ((short)item.ExitCode).ToString();
                    res.Add(info);
                }

                return res.ToArray();
            }
        }




        public static Tuple<DateTime, String>[] GetServerLog()
        {
            lock ( _lockObj ) {
                var log = from elem in _schedServ.ServerLogs
                          select elem;

                var res = new List<Tuple<DateTime, String>>();
                foreach ( var item in log ) {
                    var entry = new Tuple<DateTime, String>(
                        item.DT,
                        item.Description
                    );
                    res.Add(entry);
                }

                return res.ToArray();
            }
        }




        public static void LogActivatedTask(int taskID, String userName, String descr)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.UserLogTypes
                           where "TaskActivated" == elem.Name.Trim()
                           select elem.id;
                var userId = from elem in _debitServ.USERS
                             where elem.UserName.Trim() == userName
                             select elem.id_Polz;

                UserLog log = new UserLog {
                    UserID = userId.First(),
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    TaskID = taskID,
                    Description = descr
                };

                _schedServ.UserLogs.InsertOnSubmit(log);
                _schedServ.SubmitChanges();

                //return true;
            }
        }




        public static void LogDeactivatedTask(int taskID, String userName, String descr)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.UserLogTypes
                           where "TaskDeactivated" == elem.Name.Trim()
                           select elem.id;
                var userId = from elem in _debitServ.USERS
                             where elem.UserName.Trim() == userName
                             select elem.id_Polz;

                UserLog log = new UserLog {
                    UserID = userId.First(),
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    TaskID = taskID,
                    Description = descr
                };

                _schedServ.UserLogs.InsertOnSubmit(log);
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    //errorMsg.Append(e.ToString());
                    //LogServerInfo("Ошибка изменения бд. Exception: " + e);
                    //return false;
                }

                //return true;
            }
        }




        public static void LogActivatedSchedule(int taskID, String userName, String descr)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.UserLogTypes
                           where "ScheduleActivated" == elem.Name.Trim()
                           select elem.id;
                var userId = from elem in _debitServ.USERS
                             where elem.UserName.Trim() == userName
                             select elem.id_Polz;

                UserLog log = new UserLog {
                    UserID = userId.First(),
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    TaskID = taskID,
                    Description = descr
                };

                _schedServ.UserLogs.InsertOnSubmit(log);
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    return;
                }
            }
        }




        public static void LogDeactivatedSchedule(int taskID, String userName,
            String descr)
        {
            lock ( _lockObj ) {
                var type = from elem in _schedServ.UserLogTypes
                           where "ScheduleDeactivated" == elem.Name.Trim()
                           select elem.id;
                var userId = from elem in _debitServ.USERS
                             where elem.UserName.Trim() == userName
                             select elem.id_Polz;

                UserLog log = new UserLog {
                    UserID = userId.First(),
                    DT = DateTime.Now,
                    TypeID = type.First(),
                    TaskID = taskID,
                    Description = descr
                };

                _schedServ.UserLogs.InsertOnSubmit(log);
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    return;
                }
            }
        }




        public static ScheduleInfo ActivateSchedule(int schedID, String userName)
        {
            lock ( _lockObj ) {
                var schedQuery = from elem in _schedServ.Schedules
                                 where elem.id == schedID &&
                                 !elem.Deleted && !elem.Active //&& elem.Task.Active
                                 select elem;
                var sched = schedQuery.FirstOrDefault();
                if ( sched == null ) {
                    LogServerInfo("Не удалось активировать расписание с id = " +
                        schedID);
                    return null;
                }


                sched.Active = true;
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    //LogTaskActivity(sched.Task.id, "Возникла ошибка при которой" +
                    //    " серверу не удалось активировать расписание под именем " +
                    //    sched.Name.Trim() + " с id = " + sched.id + "Подробнее: \n" + e);
                    return null;
                }


                String description = "Пользователь " + userName +
                    " активировал расписание " + sched.Name.Trim() + " задачи " +
                    sched.Task.Name.Trim();
                LogActivatedSchedule(sched.Task.id, userName, description);
                return GetScheudleInfo(sched);
            }
        }




        public static bool DeactivateSchedule(int schedID, String userName)
        {
            lock ( _lockObj ) {
                var schedQuery = from elem in _schedServ.Schedules
                                 where elem.id == schedID && !elem.Deleted &&
                                     elem.Active //&& elem.Task.Active
                                 select elem;
                var sched = schedQuery.FirstOrDefault();
                if ( sched == null ) {
                    LogServerInfo("Не удалось деактивировать расписание с id = " +
                        schedID);
                    return false;
                }


                sched.Active = false;
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    //LogTaskActivity(sched.Task.id, "Возникла ошибка при которой" +
                    //    " серверу не удалось деактивировать расписание под именем " +
                    //    sched.Name.Trim() + " с id = " + sched.id + ". Подробнее \n" + e);
                    return false;
                }


                String description = "Пользователь " + userName +
                    " деактивировал расписание " + sched.Name.Trim() + " задачи " +
                    sched.Task.Name.Trim();
                LogDeactivatedSchedule(sched.Task.id, userName, description);
                return true;
            }
        }




        public static bool ActivateTask(int taskID, String userName)
        {
            lock ( _lockObj ) {
                var taskQuery = from elem in _schedServ.Tasks
                                where elem.id == taskID && !elem.Deleted &&
                                 !elem.Active
                                select elem;
                var task = taskQuery.FirstOrDefault();
                if ( task == null ) {
                    LogServerInfo("Не найдена задача с id = " + taskID +
                        ". Вызывалась функция активации задачи");
                    return false;
                }


                var scheds = from elem in _schedServ.Schedules
                             where elem.Task.id == taskID && !elem.Deleted &&
                                 !elem.Active && !elem.Task.Active
                             select elem;
                foreach ( var sched in scheds )
                    ActivateSchedule(sched.id, userName);



                task.Active = true;
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    //LogTaskActivity(task.id, "Возникла ошибка при которой" +
                    //    " серверу не удалось активировать задачу под именем " +
                    //    task.Name.Trim() + " с id = " + task.id + ". Подробнее: \n" + e);
                    return false;
                }



                String description = "Пользователь " + userName +
                    " активировал задачу " + task.Name.Trim();
                LogActivatedTask(task.id, userName, description);
                return true;
            }
        }




        public static bool DeactivateTask(int taskID, String userName)
        {
            lock ( _lockObj ) {
                var taskQuery = from elem in _schedServ.Tasks
                                where elem.id == taskID && !elem.Deleted &&
                                    elem.Active
                                select elem;
                var task = taskQuery.FirstOrDefault();
                if ( task == null ) {
                    LogServerInfo("Не найдена задача с id = " + taskID +
                        ". Вызывалась функция деактивации задачи");
                    return false;
                }


                var scheds = from elem in _schedServ.Schedules
                             where elem.Task.id == taskID && !elem.Deleted &&
                                 elem.Active && elem.Task.Active
                             select elem;
                foreach ( var sched in scheds )
                    DeactivateSchedule(sched.id, userName);


                task.Active = false;
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    //LogTaskActivity(task.id, "Возникла ошибка при которой" +
                    //    " серверу не удалось деактивировать задачу под именем " +
                    //    task.Name.Trim() + " с id = " + task.id + ". Подробнее: \n" + e);
                    return false;
                }


                String description = "Пользователь " + userName +
                    " деактивировал задачу " + task.Name.Trim();
                LogDeactivatedTask(task.id, userName, description);
                return true;
            }
        }




        public static void UpdateBusinessTask(String[] tasks)
        {
            lock ( _lockObj ) {
                var btasks = from elem in _schedServ.BusinessTasks
                             where !elem.Deleted
                             select elem;

                foreach ( var btask in btasks ) {
                    String res = tasks.FirstOrDefault((x) => x == btask.Name.Trim());
                    if ( res != null )
                        continue;

                    btask.Deleted = true;
                    //_schedServ.BusinessTasks.
                    _schedServ.SubmitChanges();
                }

                foreach ( var t in tasks ) {
                    var temp = from elem in _schedServ.BusinessTasks
                               where t == elem.Name.Trim()
                               select elem;
                    if ( temp.Count() != 0 ) {
                        var task = temp.First();
                        if ( !task.Deleted )
                            continue;

                        task.Deleted = false;
                        task.UpdateDate = TaskExecutor.LastModified();
                        task.Descriprion = "Занесено в базу в " + DateTime.Now +
                            ". Версия: " + TaskExecutor.GetVersion();

                        //_schedServ.BusinessTasks.InsertOnSubmit(task);
                        _schedServ.SubmitChanges();
                        continue;
                    }

                    BusinessTask btask = new BusinessTask {
                        Name = t,
                        UpdateDate = TaskExecutor.LastModified(),
                        Descriprion = "Занесено в базу в " + DateTime.Now +
                            ". Версия: " + TaskExecutor.GetVersion(),
                        Deleted = false
                    };
                    _schedServ.BusinessTasks.InsertOnSubmit(btask);
                    _schedServ.SubmitChanges();
                }
            }
        }



        public static bool LogPerformance(int scheduleID, float value)
        {
            lock ( _lockObj ) {
                var taskQuery = from elem in _schedServ.Schedules
                                where elem.id == scheduleID && !elem.Deleted
                                select elem;
                var task = taskQuery.FirstOrDefault();
                if ( task == null ) {
                    LogServerInfo("Не найдена задача с id = " + scheduleID +
                        ". Вызывалась функция записи производительности задачи");
                    return false;
                }


                _schedServ.PerformanceLogs.InsertOnSubmit(new PerformanceLog {
                    DT = DateTime.Now,
                    ScheduleID = scheduleID,
                    Value = value
                });
                try {
                    _schedServ.SubmitChanges();
                } catch ( Exception e ) {
                    return false;
                }

                return true;
            }
        }


        public static TaskLogExecutionInfo[] GetTaskExecutionLog(
            int taskID, DateTime day)
        {
            lock ( _lockObj ) {
                var typeStartedQuery = from elem in _schedServ.TaskLogTypes
                                       where "Started" == elem.Name.Trim()
                                       select elem.id;
                var typeFinishedQuery = from elem in _schedServ.TaskLogTypes
                                        where "Finished" == elem.Name.Trim()
                                        select elem.id;
                var startedID = typeStartedQuery.First();
                var finishedID = typeFinishedQuery.First();


                var taskQuery = from elem in _schedServ.Tasks
                                where taskID == elem.id && !elem.Deleted
                                select elem;
                var task = taskQuery.FirstOrDefault();
                if ( task == null ) {
                    LogServerInfo("Не найдена задача с id = " + taskID +
                        ". Вызывалась функция получения лога запуска и завершения задачи");
                    return null;
                }


                var fromDt = new DateTime(day.Year, day.Month, day.Day, 0, 0, 0);
                var toDt = fromDt.AddDays(1);
                var result = new List<TaskLogExecutionInfo>();
                var relevantTasks = from elem in _schedServ.TaskLogs
                                    where elem.DT >= fromDt && elem.DT < toDt &&
                                        elem.TaskID == taskID &&
                                        (elem.TypeID == startedID || elem.TypeID == finishedID)
                                    orderby elem.DT ascending
                                    select elem;
                foreach ( var item in relevantTasks ) {
                    if ( item.TypeID == startedID ) {
                        var temp = new TaskLogExecutionInfo {
                            ScheduleID = (int)item.ScheduleID,
                            ScheduleName = item.Schedule.Name.Trim(),
                            StartDateTime = item.DT
                        };
                        result.Add(temp);
                    } else {
                        var res = result.Find((x) => x.ScheduleID == item.ScheduleID);
                        res.EndDateTime = item.DT;
                        res.ExitCode = (int)item.ExitCode;
                    }
                }


                result.Sort((x, y) => x.StartDateTime.CompareTo(y.StartDateTime));
                return result.ToArray();
            }
        }



        public static float[] GetStoredCpuPerformance(TaskLogExecutionInfo info)
        {
            lock ( _lockObj ) {
                var logQuery = from elem in _schedServ.PerformanceLogs
                               where elem.ScheduleID == info.ScheduleID &&
                                elem.DT >= info.StartDateTime && elem.DT <= info.EndDateTime
                               orderby elem.DT ascending
                               select elem;

                var res = new List<float>();
                foreach ( var item in logQuery ) {
                    res.Add((float)item.Value);
                }

                return res.ToArray();
            }
        }


    }
}