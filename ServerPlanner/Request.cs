﻿using System;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Planner
{
    public enum RequestType
    {
        LogIn,
        GetTasks,
        GetTaskTypes,
        GetTaskStatuses,
        UpdateTasks,
        AddTask,
        GetCpuPerformance,
        GetServerTime,
        RemoveTask,
        RemoveSchedule,
        GetUserLog,
        GetTaskLog,
        CheckSessionHash,
        LogOut,
        ActivateTask,
        DeactivateTask,
        ActivateSchedule,
        DeactivateSchedule,
        AddSchedule,
        GetServerLog,
        GetTaskVersion,
        GetTaskExecutionLog,
        GetStoredCpuPerformance,
        None
    };

    internal class Request
    {
        private readonly JObject _root;
        public RequestType Type
        {
            get { return (RequestType)_root["requestType"].ToObject<int>(); }
            set { _root["requestType"] = (int)value; }
        }
        public bool IsSuccessful
        {
            get { return (bool)_root["isSuccessful"]; }
            set { _root["isSuccessful"] = value; }
        }
        public String ErrorMsg
        {
            get { return (String)_root["errorMsg"]; }
            set { _root["errorMsg"] = value; }
        }


        public Request()
        {
            _root = new JObject();
            Type = RequestType.None;
        }


        public Request(String json)
        {
            try
            {
                _root = JObject.Parse(json);
            }
            catch (Exception e)
            {
            }
        }




        public Request(RequestType type)
            : this()
        {
            Type = type;
        }


        public Request SetValue(String key, JToken v)
        {
            _root[key] = v;
            return this;
        }


        public Request SetValue(String key, TaskInfo taskInfo)
        {
            //String json = JsonConvert.SerializeObject(taskInfo);
            _root[key] = JObject.FromObject(taskInfo);
            return this;
        }



        public Request SetValue(String key, ScheduleInfo schedInfo)
        {
            //String json = JsonConvert.SerializeObject(taskInfo);
            _root[key] = JObject.FromObject(schedInfo);
            return this;
        }



        public Request SetValue(String key, UserLogInfo userLog)
        {
            //String json = JsonConvert.SerializeObject(taskInfo);
            _root[key] = JObject.FromObject(userLog);
            return this;
        }



        public Request SetValue(String key, TaskLogExecutionInfo info)
        {
            //String json = JsonConvert.SerializeObject(taskInfo);
            _root[key] = JObject.FromObject(info);
            return this;
        }



        public Request SetValue(String key, TaskInfo[] list)
        {
            JArray arr = new JArray();
            foreach (var task in list)
                arr.Add(JObject.FromObject(task));

            return SetValue(key, arr);
        }


        public Request SetValue(String key, UserLogInfo[] list)
        {
            JArray arr = new JArray();
            foreach (var task in list)
                arr.Add(JObject.FromObject(task));

            return SetValue(key, arr);
        }


        public Request SetValue(String key, TaskLogInfo[] list)
        {
            JArray arr = new JArray();
            foreach (var task in list)
                arr.Add(JObject.FromObject(task));

            return SetValue(key, arr);
        }


        public Request SetValue(String key, String[] strs)
        {
            JArray arr = new JArray();
            foreach (var str in strs)
                arr.Add(str);

            return SetValue(key, arr);
        }


        public Request SetValue(String key, float[] floats)
        {
            JArray arr = new JArray();
            foreach ( var elem in floats )
                arr.Add(elem);

            return SetValue(key, arr);
        }


        public Request SetValue(String key, Tuple<DateTime, String>[] serverLog)
        {
            JArray arr = new JArray();
            foreach ( var item in serverLog ) {
                JObject obj = new JObject();
                obj["DateTime"] = item.Item1;
                obj["Description"] = item.Item2;
                arr.Add(obj);
            }
            return SetValue(key, arr);
        }



        public Request SetValue(String key, Tuple<int, float>[] performance)
        {
            JArray arr = new JArray();
            foreach ( var item in performance ) {
                JObject obj = new JObject();
                obj["taskID"] = item.Item1;
                obj["performance"] = item.Item2;
                arr.Add(obj);
            }
            return SetValue(key, arr);
        }



        public Request SetValue(String key, TaskLogExecutionInfo[] list)
        {
            JArray arr = new JArray();
            foreach ( var task in list )
                arr.Add(JObject.FromObject(task));

            return SetValue(key, arr);
        }





        public String GetString(String key)
        {
            return (String)_root[key];
        }


        public int GetInt(String key)
        {
            return (int)_root[key];
        }


        public TimeSpan GetTimeSpan(String key)
        {
            return (TimeSpan)_root[key];
        }


        public DateTime GetDateTime(String key)
        {
            return (DateTime)_root[key];
        }


        public DateTimeOffset GetDateTimeOffset(String key)
        {
            return (DateTimeOffset)_root[key];
        }


        public bool GetBool(String key)
        {
            return (bool)_root[key];
        }


        public TaskInfo GetTaskInfo(String key)
        {
            TaskInfo result = _root[key].ToObject<TaskInfo>();
            var arr = ((JObject)_root[key]).GetValue("Schedules");
            foreach (var sched in arr)
                result.AddSchedule(sched.ToObject<ScheduleInfo>());

            return result;
        }


        public Tuple<DateTime, String>[] GetServerLogTuple(String key)
        {
            var arr = (JArray)_root[key];
            var res = new List<Tuple<DateTime, String>>(arr.Count);
            foreach ( var item in arr ) {
                JObject obj = (JObject)item;
                var temp = new Tuple<DateTime, String>(
                    (DateTime)obj["DateTime"],
                    (String)obj["Description"]
                );
                res.Add(temp);
            }

            return res.ToArray();
        }


        public Tuple<int, float>[] GetPerformanceTuple(String key)
        {
            var arr = (JArray)_root[key];
            var res = new List<Tuple<int, float>>(arr.Count);
            foreach ( var item in arr ) {
                JObject obj = (JObject)item;
                var temp = new Tuple<int, float>(
                    (int)obj["taskID"],
                    (float)obj["performance"]
                );
                res.Add(temp);
            }

            return res.ToArray();
        }



        public ScheduleInfo GetScheduleInfo(String key)
        {
            return _root[key].ToObject<ScheduleInfo>();
        }



        public TaskLogExecutionInfo GetTaskLogExecutionInfo(String key)
        {
            return _root[key].ToObject<TaskLogExecutionInfo>();
        }



        public IList<T> GetList<T>(String key)
        {
            return _root[key].Select(t => t.ToObject<T>()).ToList();
        }


        public UserLogInfo[] GetUserLogArray(String key)
        {
            return _root[key].Select(t => t.ToObject<UserLogInfo>()).ToArray();
        }


        public TaskLogExecutionInfo[] GetTaskExecutionLogArray(String key)
        {
            return _root[key].Select(t => t.ToObject<TaskLogExecutionInfo>()).
                ToArray();
        }


        public TaskLogInfo[] GetTaskLogArray(String key)
        {
            return _root[key].Select(t => t.ToObject<TaskLogInfo>()).ToArray();
        }


        public TaskInfo[] GetTaskInfoArray(String key)
        {
            JArray arr = (JArray)_root[key];
            TaskInfo[] result = new TaskInfo[arr.Count];
            for (int i = 0; i < arr.Count; ++i)
            {
                result[i] = arr[i].ToObject<TaskInfo>();
                var schedules = ((JObject)arr[i]).GetValue("Schedules");
                foreach (var sched in schedules)
                    result[i].AddSchedule(sched.ToObject<ScheduleInfo>());
            }

            return result;
        }


        public String[] GetStringArray(String key)
        {
            return _root[key].Select(t => t.ToString()).ToArray();
        }


        public float[] GetFloatArray(String key)
        {
            return _root[key].Select(t => (float)t).ToArray();
        }


        public float GetFloat(String key)
        {
            return (float)_root[key];
        }





        public static T DeserializeObject<T>(String json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }


        public static String GetJson(Object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }


        public static void Test()
        {
            String obj = JsonConvert.SerializeObject(new DateTime[3]);

            DateTime[] arr = JsonConvert.DeserializeObject<DateTime[]>(obj);

            foreach (var some in arr)
            {
                Console.WriteLine(some);
            }
        }





        public override string ToString()
        {
            return _root.ToString();
        }


        public byte[] GetUnicodeBytes()
        {
            return GetUnicodeBytes(this);
        }


        public static byte[] GetUnicodeBytes(Request request)
        {
            return Encoding.Unicode.GetBytes(
                request.ToString());
        }


        public static Request GetRequest(byte[] jsonBytes)
        {
            String str = Encoding.Unicode.GetString(jsonBytes);
            return new Request(str);
        }


        public void Clear()
        {
            _root.RemoveAll();
        }
    }
}
