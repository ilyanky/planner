﻿using System;
using System.IO;
using Newtonsoft.Json.Linq;


namespace Planner
{
    internal static class Config
    {
        private const String CONFIG_PATH =
            "config.cfg";
        private static readonly JObject _cfgObject;


        static Config()
        {
            try
            {
                string strCfg;
                using (var reader = new StreamReader(CONFIG_PATH))
                {
                    strCfg = reader.ReadToEnd();
                }
                _cfgObject = JObject.Parse(strCfg);
                _cfgObject["defaultConfig"] = false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                _cfgObject = DefaultConfig();
            }



        }


        private static JObject DefaultConfig()
        {
            var cfgObject = new JObject();
            cfgObject["dataSource"] = "192.168.20.150";
            cfgObject["database"] = "Debit_serv";
            cfgObject["userID"] = "access";
            cfgObject["password"] = "777";
            cfgObject["defaultConfig"] = true;
            cfgObject["connectionsQueueMaxLength"] = 100;
            cfgObject["listeningPort"] = 2323;
            return cfgObject;
        }


        public static String GetValue(String key)
        {
            return (String)_cfgObject[key];
        }


        public static String GetDataSource()
        {
            return GetValue("dataSource");
        }


        public static int GetPort()
        {
            return (int)_cfgObject["listeningPort"];
        }


        public static int GetBacklog()
        {
            return (int)_cfgObject["connectionsQueueMaxLength"];
        }


        public static String GetDatabase()
        {
            return (String)_cfgObject["database"];
        }
    }
}
