﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;


namespace Planner
{
    internal sealed class Server
    {
        private readonly Socket _server;
        private readonly Dictionary<String, Session> _sessionMap;
        private readonly Dictionary<RequestType,
            Func<Request, Request>> _requestMap;

        private readonly String[] _taskTypes;
        private readonly String _dllVersion;


        private DateTime _lastTasksModified;
        private DateTime _lastTasksStatusesModified;


        private Server()
        {
            _server = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            _sessionMap = new Dictionary<string, Session>();

            _requestMap = new Dictionary<RequestType,
                Func<Request, Request>>();
            _requestMap[RequestType.LogIn] = LogIn;
            _requestMap[RequestType.AddTask] = AddTask;
            _requestMap[RequestType.UpdateTasks] = UpdateTasks;
            _requestMap[RequestType.GetTasks] = GetTasks;
            _requestMap[RequestType.GetTaskTypes] = GetTaskTypes;
            _requestMap[RequestType.GetTaskStatuses] = GetTaskStatuses;
            _requestMap[RequestType.GetCpuPerformance] = GetCpuPerformance;
            _requestMap[RequestType.GetServerTime] = GetServerTime;
            _requestMap[RequestType.RemoveTask] = RemoveTask;
            _requestMap[RequestType.RemoveSchedule] = DeleteSchedule;
            _requestMap[RequestType.GetUserLog] = GetUserLog;
            _requestMap[RequestType.GetTaskLog] = GetTaskLog;
            _requestMap[RequestType.CheckSessionHash] = CheckSessionHash;
            _requestMap[RequestType.LogOut] = LogOut;
            _requestMap[RequestType.AddSchedule] = AddSchedule;
            _requestMap[RequestType.ActivateTask] = ActivateTask;
            _requestMap[RequestType.ActivateSchedule] = ActivateSchedule;
            _requestMap[RequestType.DeactivateTask] = DeactivateTask;
            _requestMap[RequestType.DeactivateSchedule] = DeactivateSchedule;
            _requestMap[RequestType.GetServerLog] = GetServerLog;
            _requestMap[RequestType.GetTaskVersion] = GetTaskVersion;
            _requestMap[RequestType.GetTaskExecutionLog] = GetTaskExecutionLog;
            _requestMap[RequestType.GetStoredCpuPerformance] = GetStoredCpuPerformance;


            _taskTypes = TaskExecutor.GetTypes();
            _dllVersion = TaskExecutor.GetVersion().ToString();
        }





        private void ReadCallback(IAsyncResult ar)
        {
            MessageStateObject state = (MessageStateObject)ar.AsyncState;
            Socket handler = state.WorkSocket;

            int bytesRead = 0;
            try {
                bytesRead = handler.EndReceive(ar);

                // !!!! Очень опасное условие. Ставлю из-за того, что документация
                // Microsoft очень неоднозначна
                if ( bytesRead == 0 ) {
                    String descr = "Закрыто соединение с сокетом " +
                        handler.LocalEndPoint + " (по локальному адресу) " +
                        handler.RemoteEndPoint + "(по глобальному адресу)";
                    DataBaseAccess.LogServerInfo(descr);
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                    return;
                }
            } catch ( Exception e ) {
                String descr = "Cокет неожиданно прервал соединение " +
                    handler.LocalEndPoint + " (по локальному адресу) " +
                    handler.RemoteEndPoint + "(по глобальному адресу).\nException:\n";
                descr += e.ToString();
                DataBaseAccess.LogServerInfo(descr);
                return;
            }

            state.FlushBuffer(bytesRead);
            if ( state.IsComplete ) {
                Request request = Request.GetRequest(state.GetMessage());
                if ( !CheckAuth(request) ) {
                    request.IsSuccessful = false;
                    request.SetValue("authFail", true);
                    request.ErrorMsg = "Для выполнения этого действия " +
                        "требуется авторизация.";
                    Send(handler, request);
                } else
                    Send(handler, _requestMap[request.Type](request));
            }

            handler.BeginReceive(state.Buffer, 0, MessageStateObject.BufferSize,
                0, ReadCallback, state);
        }


        private void Send(Socket handler, Request request)
        {
            byte[] bytes = request.GetUnicodeBytes();

            byte[] size = BitConverter.GetBytes(bytes.Length);

            byte[] res = new byte[bytes.Length + size.Length];
            for ( int i = 0; i < size.Length; ++i )
                res[i] = size[i];
            int j = 0;
            for ( int i = size.Length; i < res.Length; ++i ) {
                res[i] = bytes[j++];
            }

            try {
                //handler.Send(size);
                //handler.Send(bytes);
                handler.Send(res);
            } catch ( Exception e ) {
                String descr = "Ошибка при отправке данных по сокету " +
                    handler.LocalEndPoint + " (по локальному адресу) " +
                    handler.RemoteEndPoint + "(по глобальному адресу).\nException:\n";
                descr += e.ToString();
                DataBaseAccess.LogServerInfo(descr);
            }
        }


        private bool CheckAuth(Request request)
        {
            if ( request.Type == RequestType.LogIn )
                return true;

            String hash = request.GetString("sessionHash");
            if ( hash == null )
                return false;

            var res = _sessionMap.FirstOrDefault((x) => x.Key == hash);
            if ( res.Key == null )
                return false;

            res.Value.ResetTimer();
            return true;
        }



        private Request CheckSessionHash(Request request)
        {
            Request respond = new Request(RequestType.CheckSessionHash);

            var hash = request.GetString("sessionHash");
            var res = _sessionMap.FirstOrDefault((x) => x.Key == hash);

            respond.IsSuccessful = true;

            if ( res.Key == null ) {
                respond.SetValue("contains", false);
            } else {
                respond.SetValue("userName", res.Value.UserName);
                respond.SetValue("contains", true);
            }


            return respond;
        }



        public Request LogIn(Request request)
        {
            Request respond = new Request(RequestType.LogIn);

            var usrName = request.GetString("userName");
            var pswd = request.GetString("password");

            if ( !DataBaseAccess.Auth(usrName, pswd) ) {
                respond.IsSuccessful = false;
                respond.ErrorMsg = "Логин или пароль введен неверно!";
                return respond;
            }

            var sessionHash = Session.GetHash(usrName, pswd);
            Session session = new Session(usrName, pswd);
            session.Ended += SessionEndedHandler;
            _sessionMap.Add(sessionHash, session);
            respond.IsSuccessful = true;
            respond.SetValue("sessionHash", sessionHash);

            var error = new StringBuilder();
            DataBaseAccess.LogUserEnter(usrName,
                "Пользователь " + usrName + " вошел в систему", error);

            return respond;
        }



        public Request LogOut(Request request)
        {
            Request respond = new Request(RequestType.LogOut);

            var sessionHash = request.GetString("sessionHash");

            var session = _sessionMap.First((x) => x.Key == sessionHash);
            String userName = session.Value.UserName;
            _sessionMap.Remove(session.Key);

            respond.IsSuccessful = true;
            var error = new StringBuilder();
            DataBaseAccess.LogUserExit(userName,
                "Пользователь " + userName + " вышел из системы", error);

            return respond;
        }



        private void SessionEndedHandler(Object o, EventArgs e)
        {
            var session = (Session)o;
            String descr = "Удалена сессия с хешем " + session.SessionHash +
                " пользователя " + session.UserName;
            _sessionMap.Remove(session.SessionHash);
            DataBaseAccess.LogServerInfo(descr);
        }



        public Request AddTask(Request request)
        {
            Request respond = new Request(RequestType.AddTask);
            StringBuilder errorMsg = new StringBuilder();

            bool result = TaskPlanner.AddTask(
                request.GetTaskInfo("taskInfo"), errorMsg);

            if ( !result ) {
                respond.IsSuccessful = false;
                respond.ErrorMsg = errorMsg.ToString();
                return respond;
            }

            _lastTasksModified = DateTime.Now;
            respond.IsSuccessful = true;
            return respond;
        }



        public Request ActivateTask(Request request)
        {
            Request respond = new Request(RequestType.ActivateTask);
            StringBuilder errorMsg = new StringBuilder();

            var taskID = request.GetInt("taskID");
            var userName = request.GetString("userName");
            bool result = TaskPlanner.ActivateTask(taskID, userName, errorMsg);

            if ( !result ) {
                respond.IsSuccessful = false;
                respond.ErrorMsg = errorMsg.ToString();
                return respond;
            }


            _lastTasksModified = DateTime.Now;
            respond.IsSuccessful = true;
            return respond;
        }



        public Request AddSchedule(Request request)
        {
            Request respond = new Request(RequestType.AddSchedule);
            StringBuilder errorMsg = new StringBuilder();

            var sched = request.GetScheduleInfo("schedule");
            var taskID = request.GetInt("taskID");
            bool result = TaskPlanner.AddSchedule(sched, taskID, errorMsg);

            if ( !result ) {
                respond.IsSuccessful = false;
                respond.ErrorMsg = errorMsg.ToString();
                return respond;
            }

            _lastTasksModified = DateTime.Now;
            respond.IsSuccessful = true;
            return respond;
        }



        public Request ActivateSchedule(Request request)
        {
            Request respond = new Request(RequestType.ActivateSchedule);
            StringBuilder errorMsg = new StringBuilder();

            var userName = request.GetString("userName");
            var taskID = request.GetInt("taskID");
            var schedID = request.GetInt("scheduleID");
            bool result = TaskPlanner.ActivateSchedule(userName, taskID,
                schedID, errorMsg);

            if ( !result ) {
                respond.IsSuccessful = false;
                respond.ErrorMsg = errorMsg.ToString();
                return respond;
            }


            _lastTasksModified = DateTime.Now;
            respond.IsSuccessful = true;
            return respond;
        }



        public Request RemoveTask(Request request)
        {
            Request respond = new Request(RequestType.RemoveTask);
            StringBuilder errorMsg = new StringBuilder();

            var taskID = request.GetInt("taskID");
            var userName = request.GetString("userName");
            bool result = TaskPlanner.DeleteTask(userName, taskID, errorMsg);

            if ( !result ) {
                respond.IsSuccessful = false;
                respond.ErrorMsg = errorMsg.ToString();
                return respond;
            }


            _lastTasksModified = DateTime.Now;
            respond.IsSuccessful = true;
            return respond;
        }



        public Request DeactivateTask(Request request)
        {
            Request respond = new Request(RequestType.DeactivateTask);
            StringBuilder errorMsg = new StringBuilder();

            var taskID = request.GetInt("taskID");
            var userName = request.GetString("userName");
            bool result = TaskPlanner.DeactivateTask(taskID, userName, errorMsg);

            if ( !result ) {
                respond.IsSuccessful = false;
                respond.ErrorMsg = errorMsg.ToString();
                return respond;
            }


            _lastTasksModified = DateTime.Now;
            respond.IsSuccessful = true;
            return respond;
        }



        public Request DeleteSchedule(Request request)
        {
            Request respond = new Request(RequestType.RemoveSchedule);
            StringBuilder errorMsg = new StringBuilder();

            var taskID = request.GetInt("taskID");
            var schedID = request.GetInt("schedID");
            var userName = request.GetString("userName");
            bool result = TaskPlanner.DeleteSchedule(userName, taskID, schedID, errorMsg);

            if ( !result ) {
                respond.IsSuccessful = false;
                respond.ErrorMsg = errorMsg.ToString();
                return respond;
            }


            _lastTasksModified = DateTime.Now;
            respond.IsSuccessful = true;
            return respond;
        }



        public Request DeactivateSchedule(Request request)
        {
            Request respond = new Request(RequestType.DeactivateSchedule);
            StringBuilder errorMsg = new StringBuilder();

            var taskID = request.GetInt("taskID");
            var schedID = request.GetInt("schedID");
            var userName = request.GetString("userName");
            bool result = TaskPlanner.DeactivateSchedule(userName, 
                taskID, schedID, errorMsg);

            if ( !result ) {
                respond.IsSuccessful = false;
                respond.ErrorMsg = errorMsg.ToString();
                return respond;
            }


            _lastTasksModified = DateTime.Now;
            respond.IsSuccessful = true;
            return respond;
        }




        public Request UpdateTasks(Request request)
        {
            Request respond = new Request(RequestType.UpdateTasks);

            DateTime temp = request.GetDateTime("lastTasksModified");
            DateTime temp2 = request.GetDateTime("lastTasksStatusesModified");
            if ( temp == _lastTasksModified && temp2 == _lastTasksStatusesModified ) {
                respond.IsSuccessful = true;
                respond.SetValue("updated", false);
                return respond;
            }

            DateTime fromDt = request.GetDateTime("fromDateTime");
            DateTime toDt = request.GetDateTime("toDateTime");
            bool onlyActiveTasks = request.GetBool("onlyActiveTasks");
            bool onlyActiveSchedules = request.GetBool("onlyActiveSchedules");


            var tasks = DataBaseAccess.GetTasks(fromDt, toDt,
                onlyActiveTasks, onlyActiveSchedules);
            var statuses = TaskPlanner.GetTasksStatuses(fromDt, toDt,
                onlyActiveTasks, onlyActiveSchedules);


            respond.SetValue("updated", true);
            respond.SetValue("taskList", tasks);
            respond.SetValue("statuses", statuses);
            respond.SetValue("lastTasksModified", _lastTasksModified);
            respond.SetValue("lastTasksStatusesModified", _lastTasksStatusesModified);
            respond.IsSuccessful = true;

            return respond;
        }



        public Request GetTasks(Request request)
        {
            Request respond = new Request(RequestType.GetTasks);

            DateTime fromDt = request.GetDateTime("fromDateTime");
            DateTime toDt = request.GetDateTime("toDateTime");
            bool onlyActiveTasks = request.GetBool("onlyActiveTasks");
            bool onlyActiveSchedules = request.GetBool("onlyActiveSchedules");

            var tasks = DataBaseAccess.GetTasks(fromDt, toDt,
                onlyActiveTasks, onlyActiveSchedules);
            var statuses = TaskPlanner.GetTasksStatuses(fromDt, toDt,
                onlyActiveTasks, onlyActiveSchedules);

            respond.SetValue("taskList", tasks);
            respond.SetValue("statuses", statuses);
            respond.IsSuccessful = true;

            return respond;
        }



        public Request GetTaskTypes(Request request)
        {
            Request respond = new Request(RequestType.GetTaskTypes);

            respond.SetValue("types", _taskTypes);
            respond.IsSuccessful = true;

            return respond;
        }



        public Request GetTaskVersion(Request request)
        {
            Request respond = new Request(RequestType.GetTaskVersion);

            respond.SetValue("version", _dllVersion);
            respond.IsSuccessful = true;

            return respond;
        }



        public Request GetTaskStatuses(Request request)
        {
            Request respond = new Request(RequestType.GetTaskStatuses);

            DateTime fromDt = request.GetDateTime("fromDt");
            DateTime toDt = request.GetDateTime("toDt");
            respond.SetValue("statuses", TaskPlanner.GetTasksStatuses(fromDt, toDt, true, true));
            respond.IsSuccessful = true;

            return respond;
        }



        public Request GetCpuPerformance(Request request)
        {
            Request respond = new Request(RequestType.GetCpuPerformance);
            StringBuilder errorMsg = new StringBuilder();

            var taskID = request.GetInt("taskID");
            var perf = TaskPlanner.GetCpuPerformance(taskID, errorMsg);

            if ( perf == null ) {
                respond.IsSuccessful = false;
                respond.ErrorMsg = errorMsg.ToString();
                return respond;
            }

            respond.SetValue("cpuPerformance", perf);
            respond.IsSuccessful = true;
            return respond;
        }



        public Request GetServerTime(Request request)
        {
            Request respond = new Request(RequestType.GetServerTime);

            respond.SetValue("serverTime", DateTimeOffset.UtcNow);
            respond.IsSuccessful = true;

            return respond;
        }



        public Request GetUserLog(Request request)
        {
            Request respond = new Request(RequestType.GetUserLog);

            String userName = request.GetString("userName");
            respond.SetValue("userLog", DataBaseAccess.GetUserLog(userName));
            respond.IsSuccessful = true;

            return respond;
        }



        public Request GetTaskLog(Request request)
        {
            Request respond = new Request(RequestType.GetTaskLog);

            var taskID = request.GetInt("taskID");
            respond.SetValue("taskLog", DataBaseAccess.GetTaskLog(taskID));
            respond.IsSuccessful = true;

            return respond;
        }


        public Request GetServerLog(Request request)
        {
            Request respond = new Request(RequestType.GetServerLog);

            respond.SetValue("serverLog", DataBaseAccess.GetServerLog());
            respond.IsSuccessful = true;

            return respond;
        }


        public Request GetTaskExecutionLog(Request request)
        {
            Request respond = new Request(RequestType.GetTaskExecutionLog);

            var taskID = request.GetInt("taskID");
            var day = request.GetDateTime("day");

            var res = DataBaseAccess.GetTaskExecutionLog(taskID, day);
            if ( res == null ) {
                respond.IsSuccessful = false;
                return respond;
            }

            respond.SetValue("taskExecutionLog", res);
            respond.IsSuccessful = true;

            return respond;
        }



        public Request GetStoredCpuPerformance(Request request)
        {
            Request respond = new Request(RequestType.GetTaskExecutionLog);

            var execInfo = request.GetTaskLogExecutionInfo("execInfo");

            var res = DataBaseAccess.GetStoredCpuPerformance(execInfo);
            if ( res == null ) {
                respond.IsSuccessful = false;
                return respond;
            }

            respond.SetValue("perfLog", res);
            respond.IsSuccessful = true;

            return respond;
        }



        public void TaskStartedHandler(Object o, EventArgs e)
        {
            _lastTasksStatusesModified = DateTime.Now;
        }


        public void TaskEndedHandler(Object o, EventArgs e)
        {
            _lastTasksStatusesModified = DateTime.Now;
        }



        public void Start()
        {
            DataBaseAccess.UpdateBusinessTask(_taskTypes);
            TaskPlanner.JustTest();

            _lastTasksModified = DateTime.Now;
            _lastTasksStatusesModified = DateTime.Now;
            TaskPlanner.TaskStarted += TaskStartedHandler;
            TaskPlanner.TaskEnded += TaskEndedHandler;


            try {
                var localIp = Config.GetValue("localServerIpAddress");
                _server.Bind(localIp.Equals("")
                    ? new IPEndPoint(IPAddress.Any, Config.GetPort())
                    : new IPEndPoint(IPAddress.Parse(localIp), Config.GetPort())
                );
                _server.Listen(Config.GetBacklog());
                DataBaseAccess.LogServerInfo("Сервер успешно запущен");

                while ( true ) {
                    Console.WriteLine("Debug: Waiting for a connection...");

                    Socket handler = _server.Accept();

                    MessageStateObject state = new MessageStateObject(handler);
                    handler.BeginReceive(state.Buffer, 0, MessageStateObject.BufferSize,
                        0, ReadCallback, state);
                }
            } catch ( Exception e ) {
                DataBaseAccess.LogServerInfo("Ошибка сервера. Exception:\n" + e);
            }
        }


        public void Close()
        {
            _server.Close();
            _server.Dispose();
            DataBaseAccess.LogServerInfo("Сервер успешно заершил работу");
        }


        public static void Main(string[] args)
        {
            try {
                Server server = new Server();
                server.Start();

                server.Close();
            } catch ( Exception e ) {
                DataBaseAccess.LogServerInfo("Ошибка сервера: Подробне:\n" + e);
            }
        }
    }


}