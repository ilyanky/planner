﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;


namespace Planner
{
    // вообще, можно провести DDoS атаку на эту часть программы
    // нужно потом добавить точки защиты от этого

    internal static class TaskPlanner
    {



        // словарь используется как множество, ибо нет конкурентного множества в .NET
        private static readonly
            ConcurrentDictionary<TaskTimer, byte> _taskMap =
                new ConcurrentDictionary<TaskTimer, byte>();
        private static readonly Timer _updateTimer =
            new Timer(UpdateTimerCallback, null, TimeSpan.Zero,
                new TimeSpan(1, 0, 0, 0)
            );




        public static event EventHandler<EventArgs> TaskEnded;
        private static void OnTaskEnded(EventArgs e)
        {
            var temp = Volatile.Read(ref TaskEnded);
            if ( temp != null )
                temp(null, e);
        }


        public static event EventHandler<EventArgs> TaskStarted;
        private static void OnTaskStarted(EventArgs e)
        {
            var temp = Volatile.Read(ref TaskStarted);
            if ( temp != null )
                temp(null, e);
        }



        private static void UpdateTimerCallback(Object o)
        {
            DateTime fromDt = DateTime.Now;
            DateTime toDt = DateTime.Today.AddDays(2);

            var tasks = DataBaseAccess.GetTasks(fromDt, toDt, true, true);
            foreach ( var task in tasks ) {
                foreach ( var sched in task.Schedules ) {
                    var dates = ScheduleInfo.GetDates(sched, fromDt, toDt);
                    foreach ( var date in dates ) {
                        Console.WriteLine(task.Name + " " + sched.Name + " " + date);

                        TaskTimer temp = new TaskTimer(new TaskTimer.TaskTimerKey(sched.id, date), task.id,
                            task.BusinessTaskName, date - fromDt);
                        temp.Ended += TaskEndedHandler;
                        temp.Started += TaskStartedHandler;
                        if ( _taskMap.TryAdd(temp, 0) ) {
                            DataBaseAccess.LogUpdaterActivity(task.id, "Задача(таймер) под именем " +
                                task.Name + " запланированная на " + date +
                                " успешно добавлена в очередь");
                        } else {
                            DataBaseAccess.LogUpdaterActivity(task.id, "Ошибка!" +
                                "Не удалось добавить задачу(таймер) под именем " +
                                task.Name + " запланированную на " + date);
                        }
                    }
                }
            }
        }


        public static bool AddTask(TaskInfo task, StringBuilder errorMsg)
        {
            // обработать ситуацию, когда срок выхода задачи истекает раньше,
            // чем она успевает сюда придти.
            // в таких случаях, когда, например, пакет слишком долго шел от клиента


            var res = DataBaseAccess.AddTask(task, errorMsg);
            if ( res == null )
                return false;

            DateTime fromDt = DateTime.Now;
            DateTime toDt = DateTime.Today.AddDays(2);

            DateTime now = DateTime.Now;
            foreach ( var sched in res.Schedules ) {
                var dates = ScheduleInfo.GetDates(sched, fromDt, toDt);
                foreach ( var date in dates ) {
                    Console.WriteLine("aaaaaaaaaaaaaaaaa" + (date - now));

                    TaskTimer temp = new TaskTimer(new TaskTimer.TaskTimerKey(sched.id, date), res.id,
                        task.BusinessTaskName, date - now);
                    temp.Ended += TaskEndedHandler;
                    temp.Started += TaskStartedHandler;
                    if ( _taskMap.TryAdd(temp, 0) ) {
                        DataBaseAccess.LogTaskActivity(res.id, "Задача(таймер) под именем " +
                            res.Name + " запланированная на " + date +
                            " успешно добавлена в очередь");
                    } else {
                        DataBaseAccess.LogTaskActivity(res.id, "Ошибка!" +
                            "Не удалось добавить задачу(таймер) под именем " +
                            res.Name + " запланированную на " + date);
                        errorMsg.Append(" Не удалось добавить задачу в очередь на выполнение");
                    }
                }
            }

            return true;
        }



        public static bool AddSchedule(ScheduleInfo schedule, int taskID,
            StringBuilder errorMsg)
        {
            // обработать ситуацию, когда срок выхода задачи истекает раньше,
            // чем она успевает сюда придти.
            // в таких случаях, когда, например, пакет слишком долго шел от клиента


            var sched = DataBaseAccess.AddShedule(schedule, taskID, errorMsg);
            if ( sched == null )
                return false;

            var task = DataBaseAccess.GetTask(taskID);
 
            DateTime fromDt = DateTime.Now;
            DateTime toDt = DateTime.Today.AddDays(2);

            DateTime now = DateTime.Now;
            var dates = ScheduleInfo.GetDates(schedule, fromDt, toDt);
            foreach ( var date in dates ) {
                Console.WriteLine("aaaaaaaaaaaaaaaaa" + (date - now));

                TaskTimer temp = new TaskTimer(new TaskTimer.TaskTimerKey(sched.id, date), taskID,
                    task.BusinessTaskName, date - now);
                temp.Ended += TaskEndedHandler;
                temp.Started += TaskStartedHandler;
                if ( _taskMap.TryAdd(temp, 0) ) {
                    DataBaseAccess.LogTaskActivity(taskID, "Задача(таймер) под именем " +
                        task.Name + " запланированная на " + date +
                        " успешно добавлена в очередь");
                } else {
                    DataBaseAccess.LogTaskActivity(taskID, "Ошибка!" +
                        "Не удалось добавить задачу(таймер) под именем " +
                        task.Name + " запланированную на " + date);
                    errorMsg.Append(" Не удалось добавить задачу в очередь на выполнение");
                }
            }

            return true;
        }



        public static bool DeleteTask(String userName, int taskID, StringBuilder errorMsg)
        {
            bool result = DataBaseAccess.DeleteTask(userName, taskID, errorMsg);

            foreach ( var task in _taskMap ) {
                if ( task.Key.TaskID != taskID )
                    continue;

                byte temp;
                if ( _taskMap.TryRemove(task.Key, out temp) ) {
                    task.Key.Dispose();
                    DataBaseAccess.LogTaskActivity(taskID, "Задача(таймер) с id = " +
                        task.Key.TaskID + " запланированная на " + task.Key.Key.DateTime +
                        " успешно удалена из очереди");
                } else {
                    errorMsg.Append(" Не удалось удалить задачу(таймер) с id = " +
                        task.Key.TaskID + " запланированную на " + task.Key.Key.DateTime +
                        " из очереди");
                    DataBaseAccess.LogTaskActivity(taskID, "Не удалось удалить задачу(таймер) с id = " +
                        task.Key.TaskID + " запланированную на " + task.Key.Key.DateTime +
                        " из очереди");
                }
            }

            return result;
        }


        public static bool DeleteSchedule(String userName, int taskID,
            int schedID, StringBuilder errorMsg)
        {
            bool result = DataBaseAccess.DeleteSchedule(userName, schedID, taskID, errorMsg);

            foreach ( var task in _taskMap ) {
                if ( task.Key.TaskID != taskID )
                    continue;
                if ( task.Key.Key.ScheduleID != schedID )
                    continue;

                byte temp;
                if ( _taskMap.TryRemove(task.Key, out temp) ) {
                    task.Key.Dispose();
                    DataBaseAccess.LogTaskActivity(taskID, "Задача(таймер) с id = " +
                        task.Key.TaskID + " запланированная на " + task.Key.Key.DateTime +
                        " успешно удалена из очереди");
                } else {
                    errorMsg.Append(" Не удалось удалить задачу(таймер) с id = " +
                        task.Key.TaskID + " запланированную на " + task.Key.Key.DateTime +
                        " из очереди");
                    DataBaseAccess.LogTaskActivity(taskID, "Не удалось удалить задачу(таймер) с id = " +
                        task.Key.TaskID + " запланированную на " + task.Key.Key.DateTime +
                        " из очереди");
                }
            }

            return result;
        }




        public static bool TaskProcessing(int taskID, DateTime dateTime)
        {
            foreach ( var task in _taskMap ) {
                if ( task.Key.TaskID != taskID )
                    continue;
                if ( task.Key.Key.DateTime != dateTime )
                    continue;

                if ( task.Key.Status == TaskStatus.Processing )
                    return true;
                else
                    return false;
            }

            return false;
        }




        public static TaskInfo[] GetTasksStatuses(DateTime fromDt, DateTime toDt,
            bool onlyActiveTasks, bool onlyActiveSchedules)
        {
            var result = new List<TaskInfo>();

            var relevantTasks = DataBaseAccess.GetTasks(fromDt, toDt,
                onlyActiveTasks, onlyActiveSchedules);
            foreach ( var task in relevantTasks ) {
                // +1, потому что последнее число из диапазона тожe нужно рассматривать
                int days = ScheduleInfo.GetDaysSpan(toDt, fromDt) + 1;
                for ( int i = 0; i < days; ++i ) {
                    DateTime date = fromDt.AddDays(i);
                    DateTime last = TaskInfo.GetLastDateTime(task, date);
                    if ( last == DateTime.MinValue )
                        continue;

                    if ( !task.Active ) {
                        DateTime[] arr = new DateTime[1];
                        arr[0] = date;
                        result.Add(new TaskInfo {
                            id = task.id,
                            Name = task.Name,
                            Status = TaskStatus.Inactive,
                            Dates = arr
                        });
                    } else if ( TaskProcessing(task.id, last) ) {
                        DateTime[] arr = new DateTime[1];
                        arr[0] = date;
                        result.Add(new TaskInfo {
                            id = task.id,
                            Name = task.Name,
                            Status = TaskStatus.Processing,
                            Dates = arr
                        });
                    } else {
                        DateTime[] arr = new DateTime[1];
                        arr[0] = date;
                        result.Add(new TaskInfo {
                            id = task.id,
                            Name = task.Name,
                            Status = DataBaseAccess.GetTaskStatus(task.id, last),
                            Dates = arr
                        });
                    }
                }
            }


            //foreach ( var item in result ) {
            //    Console.WriteLine(item.Name);
            //    Console.WriteLine(item.Status);
            //}


            return result.ToArray();
        }


        public static Tuple<int, float>[] GetCpuPerformance(int taskID, StringBuilder errorMsg)
        {
            var list = new List<Tuple<int, float>>();
            foreach ( var item in _taskMap ) {
                if ( item.Key.TaskID != taskID )
                    continue;

                if ( item.Key.Status != TaskStatus.Processing )
                    continue;

                var temp = new Tuple<int, float>(item.Key.Key.ScheduleID,
                    item.Key.GetCpuPerformance());
                list.Add(temp);
            }

            return list.ToArray();

            //var key = new TaskTimer.TaskTimerKey(sched.id, sched.Dates[0]);

            //try {
            //    var temp = _taskMap.First(
            //        (x) => x.Key.Key.ScheduleID == sched.id
            //    ).Key.GetCpuPerformance();
            //    if ( temp == -1 ) {
            //        errorMsg.Append("Задача еще ожидает выполнения");
            //        return -1;
            //    }
            //    return temp;
            //} catch ( Exception e ) {
            //    if ( key.DateTime <= DateTime.Now )
            //        errorMsg.Append("Задача уже была выполнена");
            //    return -1;
            //}
        }


        public static bool DeactivateTask(int taskID, String userName, StringBuilder errorMsg)
        {
            bool result = DataBaseAccess.DeactivateTask(taskID, userName);

            if ( !result ) {
                errorMsg.Append("Не удалось деактивировать задачу!" +
                    " Возможно, такой задачи не существует");
                DataBaseAccess.LogTaskActivity(taskID,
                    "Не удалось деактивировать задачу с id = " + taskID +
                    "! Возможно, такой задачи не существует");
            }

            foreach ( var task in _taskMap ) {
                if ( task.Key.TaskID != taskID )
                    continue;

                byte temp;
                if ( _taskMap.TryRemove(task.Key, out temp) ) {
                    task.Key.Dispose();
                    DataBaseAccess.LogTaskActivity(taskID, "Задача(таймер) с id = " +
                        task.Key.TaskID + " запланированная на " + task.Key.Key.DateTime +
                        " успешно удалена из очереди");
                } else {
                    errorMsg.Append(" Не удалось удалить задачу(таймер) с id = " +
                        task.Key.TaskID + " запланированную на " + task.Key.Key.DateTime +
                        " из очереди");
                    DataBaseAccess.LogTaskActivity(taskID, "Не удалось удалить задачу(таймер) с id = " +
                        task.Key.TaskID + " запланированную на " + task.Key.Key.DateTime +
                        " из очереди");
                }
            }

            return result;
        }



        public static bool DeactivateSchedule(String userName, int taskID,
            int schedID, StringBuilder errorMsg)
        {
            bool result = DataBaseAccess.DeactivateSchedule(schedID, userName);

            if ( !result ) {
                errorMsg.Append("Не удалось деактивировать расписание!" + 
                    " Возможно, такого расписания не существует");
                DataBaseAccess.LogTaskActivity(taskID,
                    "Не удалось деактивировать расписание с id = " + schedID + 
                    "! Возможно, такого расписаения не существует");
            }

            foreach ( var task in _taskMap ) {
                if ( task.Key.TaskID != taskID )
                    continue;
                if ( task.Key.Key.ScheduleID != schedID )
                    continue;

                byte temp;
                if ( _taskMap.TryRemove(task.Key, out temp) ) {
                    task.Key.Dispose();
                    DataBaseAccess.LogTaskActivity(taskID, "Задача(таймер) с id = " +
                        task.Key.TaskID + " запланированная на " + task.Key.Key.DateTime +
                        " успешно удалена из очереди");
                } else {
                    errorMsg.Append(" Не удалось удалить задачу(таймер) с id = " +
                        task.Key.TaskID + " запланированную на " + task.Key.Key.DateTime +
                        " из очереди");
                    DataBaseAccess.LogTaskActivity(taskID, "Не удалось удалить задачу(таймер) с id = " +
                        task.Key.TaskID + " запланированную на " + task.Key.Key.DateTime +
                        " из очереди");
                }
            }


            return result;
        }


        public static bool ActivateSchedule(String userName, int taskID,
            int schedID, StringBuilder errorMsg)
        {
            var schedule = DataBaseAccess.ActivateSchedule(schedID, userName);
            if ( schedule == null ) {
                errorMsg.Append("Не удалось активировать расписание! Возможно, такого расписания не существует!");
                DataBaseAccess.LogTaskActivity(taskID,
                    "Не удалось активировать расписание с id = " + schedID +
                    "! Возможно, такого расписаения не существует");
                return false;
            }

            DateTime fromDt = DateTime.Now;
            DateTime toDt = DateTime.Today.AddDays(2);
            var dates = ScheduleInfo.GetDates(schedule, fromDt, toDt);
            var task = DataBaseAccess.GetTaskInfo(schedule);
            if ( task == null ) {
                errorMsg.Append("Возникла ошибка при ативаации расписания");
                DataBaseAccess.LogServerInfo("Возникла ошибка при ативаации расписания c id = " +
                    taskID + " задачи с id = " + taskID);
                return false;
            }


            DateTime now = DateTime.Now;
            foreach ( var date in dates ) {
                Console.WriteLine("aaaaaaaaaaaaaaaaa" + (date - now));

                TaskTimer temp = new TaskTimer(new TaskTimer.TaskTimerKey(schedule.id, date), task.id,
                    task.BusinessTaskName, date - now);
                temp.Ended += TaskEndedHandler;
                temp.Started += TaskStartedHandler;
                if ( _taskMap.TryAdd(temp, 0) ) {
                    DataBaseAccess.LogTaskActivity(taskID, "Задача(таймер) с id = " +
                        taskID + " запланированная на " + temp.Key.DateTime +
                        " успешно добавлена в очередь");
                } else {
                    DataBaseAccess.LogTaskActivity(taskID, "Ошибка!" +
                        "Не удалось добавить задачу(таймер) с id = " +
                        taskID + " запланированную на " + temp.Key.DateTime);
                    errorMsg.Append(" Не удалось добавить задачу в очередь на выполнение");
                }
            }


            return true;
        }



        public static bool ActivateTask(int taskID, String userName,
            StringBuilder errorMsg)
        {
            // обработать ситуацию, когда срок выхода задачи истекает раньше,
            // чем она успевает сюда придти.
            // в таких случаях, когда, например, пакет слишком долго шел от клиента


            bool res = DataBaseAccess.ActivateTask(taskID, userName);
            if ( !res ) {
                errorMsg.Append("Не удалось активировать задачу! Возможно, такой задачи не существует!");
                DataBaseAccess.LogTaskActivity(taskID,
                    "Не удалось активировать задачу с id = " + taskID +
                    "! Возможно, такой задачи не существует");
                return false;
            }

            DateTime fromDt = DateTime.Now;
            DateTime toDt = DateTime.Today.AddDays(2);

            DateTime now = DateTime.Now;
            var tasks = DataBaseAccess.GetTasks(fromDt, toDt, true, true);
            foreach ( var task in tasks ) {
                foreach ( var sched in task.Schedules ) {
                    var dates = ScheduleInfo.GetDates(sched, fromDt, toDt);
                    foreach ( var date in dates ) {
                        Console.WriteLine("aaaaaaaaaaaaaaaaa" + (date - now));

                        TaskTimer temp = new TaskTimer(new TaskTimer.TaskTimerKey(sched.id, date), taskID,
                            task.BusinessTaskName, date - now);
                        temp.Ended += TaskEndedHandler;
                        temp.Started += TaskStartedHandler;
                        if ( _taskMap.TryAdd(temp, 0) ) {
                            DataBaseAccess.LogTaskActivity(taskID, "Задача(таймер) с id = " +
                                taskID + " запланированная на " + temp.Key.DateTime +
                                " успешно добавлена в очередь");
                        } else {
                            DataBaseAccess.LogTaskActivity(taskID, "Ошибка!" +
                                "Не удалось добавить задачу(таймер) с id = " +
                                taskID + " запланированную на " + temp.Key.DateTime);
                            errorMsg.Append(" Не удалось добавить задачу в очередь на выполнение");
                        }
                    }
                }
            }

            return res;
        }



        private static void TaskEndedHandler(Object o, EventArgs e)
        {
            var task = (TaskTimer)o;
            byte temp;
            if ( _taskMap.TryRemove(task, out temp) )
                DataBaseAccess.LogTaskActivity(task.TaskID,
                    "Задача(таймер) успещно удалена из очереди после завершения");
            else {
                DataBaseAccess.LogTaskActivity(task.TaskID,
                    "Ошибка при попытке удалить задачу(таймер) из очереди");
            }

            OnTaskEnded(new EventArgs());

            //String descr = "Удалена задача с  " + session.SessionHash +
            //    " пользователя " + session.UserName;
            //DataBaseAccess.LogServerInfo(descr);
        }



        private static void TaskStartedHandler(Object o, EventArgs e)
        {
            OnTaskStarted(new EventArgs());
        }


        public static void JustTest()
        {

        }

    }
}
