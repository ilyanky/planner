﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Planner
{
    // могут быть баги, если слишком маленькое время выполнения ( < 1 сек. )
    internal sealed class TaskTimer
    {
        public sealed class TaskTimerKey
        {
            public int ScheduleID { get; private set; }
            public DateTime DateTime { get; private set; }

            public TaskTimerKey(int scheduleID, DateTime dateTime)
            {
                ScheduleID = scheduleID;
                DateTime = dateTime;
            }


            public override bool Equals(object obj)
            {
                if ( obj is TaskTimerKey ) {
                    TaskTimerKey key = (TaskTimerKey)obj;
                    return ScheduleID == key.ScheduleID &&
                        DateTime == key.DateTime;
                } else
                    return false;
            }


            public bool Equals(TaskTimerKey key)
            {
                if ( (Object)key == null )
                    return false;
                else
                    return ScheduleID == key.ScheduleID &&
                        DateTime == key.DateTime;
            }

            public override int GetHashCode()
            {
                return ScheduleID.GetHashCode() ^
                    DateTime.GetHashCode();
            }
        }



        public event EventHandler<EventArgs> Ended;
        private void OnEnded(EventArgs e)
        {
            var temp = Volatile.Read(ref Ended);
            if ( temp != null )
                temp(this, e);
        }


        public event EventHandler<EventArgs> Started;
        private void OnStarted(EventArgs e)
        {
            var temp = Volatile.Read(ref Started);
            if ( temp != null )
                temp(this, e);
        }



        public TaskTimerKey Key { get; private set; }
        //public String TaskName { get; private set; }


        private readonly Timer _timer;
        private readonly Process _process;
        private readonly PerformanceCounterCategory _perfCat =
            new PerformanceCounterCategory("Процесс");
        private PerformanceCounter _cpuCounter;

        private readonly Timer _cpuPerfTimer;


        public String BusinessTask { get; private set; }
        public TaskStatus Status { get; private set; } // атомарно?
        public int ProcessID { get; private set; }
        public int TaskID { get; private set; }


        private const int MESSAGE_LENGTH = 100;
        private StringBuilder _outputDataBuilder;
        private StringBuilder _errorDataBuilder;




        public TaskTimer(TaskTimerKey key, int taskID, String businessTask)
        {
            Key = key;
            TaskID = taskID;
            BusinessTask = businessTask;

            _timer = new Timer(Run);
            _process = new Process();
            Status = TaskStatus.Waiting;

            _outputDataBuilder = new StringBuilder();
            _errorDataBuilder = new StringBuilder();

            _cpuPerfTimer = new Timer(CpuPerformanceCallback);
        }


        public TaskTimer(TaskTimerKey key, int taskID, String businessTask,
            TimeSpan dueTime) : this(key, taskID, businessTask)
        {
            Start(dueTime);
        }


        private void Start(TimeSpan span)
        {
            // !!!!! нужно задать минимумы и максимумы

            //TimeSpan span = DateTime.Now - _executionDates.Last.Value;
            _timer.Change(span, TimeSpan.Zero);
        }


        private void Run(Object stateInfo)
        {
            if ( !DataBaseAccess.IsValid(TaskID, Key.ScheduleID) ) {
                Console.WriteLine("not valid");
                Status = TaskStatus.Refused;
                return;
            }


            DataBaseAccess.LogStartedTask(TaskID, Key.ScheduleID, BusinessTask);


            _process.StartInfo.FileName = "TaskExecutor.exe";
            _process.StartInfo.Arguments = "-r " + BusinessTask;


            // Set UseShellExecute to false for redirection.
            _process.StartInfo.UseShellExecute = false;
            _process.StartInfo.RedirectStandardOutput = true;
            _process.EnableRaisingEvents = true;
            _process.StartInfo.RedirectStandardError = true;

            _process.ErrorDataReceived += ErrorDataReceivedHandler;
            _process.OutputDataReceived += OutputDataReceiveedHandler;
            _process.Exited += ExitedHandler;

            Console.WriteLine("started");
            _process.Start();
            // Start the asynchronous read of the sort output stream.
            _process.BeginOutputReadLine();
            _process.BeginErrorReadLine();
            ProcessID = _process.Id;

            if ( _cpuCounter == null )
                InitCounter();

            _cpuPerfTimer.Change(0, 1000);

            Status = TaskStatus.Processing;
            OnStarted(new EventArgs());
        }


        private void InitCounter()
        {
            var names = _perfCat.GetInstanceNames();
            foreach ( var name in names ) {
                PerformanceCounter counter =
                    new PerformanceCounter("Процесс", "Идентификатор процесса");
                counter.InstanceName = name;

                if ( counter.NextValue() == ProcessID ) {
                    var temp = new PerformanceCounter("Процесс",
                        "% загруженности процессора", name);
                    _cpuCounter = temp;
                    break;
                }
            }
        }


        public float GetCpuPerformance()
        {
            if ( Status != TaskStatus.Processing )
                return -1;

            if ( _cpuCounter == null )
                InitCounter();

            // осторожнее с Environment.ProcessorCount
            return _cpuCounter.NextValue() / Environment.ProcessorCount;
        }


        private void ExitedHandler(Object o, EventArgs e)
        {
            if ( _process.ExitCode != 0 )
                Status = TaskStatus.Error;
            else
                Status = TaskStatus.Complete;

            DataBaseAccess.LogFinishedTask(TaskID, Key.ScheduleID, _process.ExitCode, Key.DateTime);
            Console.WriteLine(_outputDataBuilder.Length);
            DataBaseAccess.LogOutputDataReceived(TaskID, _outputDataBuilder.ToString());
            if ( _errorDataBuilder.Length != 0 )
                DataBaseAccess.LogErrorDataReceived(TaskID, _errorDataBuilder.ToString());

            OnEnded(new EventArgs());

            _timer.Dispose();
            _process.Dispose();
        }


        private void OutputDataReceiveedHandler(object sendingProcess,
            DataReceivedEventArgs outLine)
        {
            if ( !String.IsNullOrEmpty(outLine.Data) )
                _outputDataBuilder.Append(outLine.Data);
        }


        private void ErrorDataReceivedHandler(object sendingProcess,
            DataReceivedEventArgs outLine)
        {
            if ( !String.IsNullOrEmpty(outLine.Data) )
                _errorDataBuilder.Append(outLine.Data);
        }



        public override bool Equals(object obj)
        {
            if ( obj is TaskTimer ) {
                TaskTimer task = (TaskTimer)obj;
                return Key.Equals(task.Key);
            } else
                return false;
        }


        public bool Equals(TaskTimer task)
        {
            if ( (Object)task == null )
                return false;
            else
                return Key.Equals(task.Key);
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }


        public void Dispose()
        {
            _timer.Change(-1, -1);
            _timer.Dispose();
            _process.Dispose();
        }


        private void CpuPerformanceCallback(Object stateInfo)
        {
            if ( Status != TaskStatus.Processing )
                return;

            if ( _cpuCounter == null )
                InitCounter();

            // осторожнее с Environment.ProcessorCount
            float value = _cpuCounter.NextValue() / Environment.ProcessorCount;
            DataBaseAccess.LogPerformance(Key.ScheduleID, value);
        }
    }
}
