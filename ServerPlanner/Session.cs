﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Planner
{
    internal sealed class Session
    {
        private Timer _endTimer;


        public String SessionHash { get; private set; }
        public String UserName { get; private set; }


        public event EventHandler<EventArgs> Ended;
        private void OnEnded(EventArgs e)
        {
            var temp = Volatile.Read(ref Ended);
            if (temp != null)
                temp(this, e);
        }



        public Session(String usrName, String pswd)
        {
            _endTimer = new Timer(EndCallback, this, new TimeSpan(1, 0, 0, 0), new TimeSpan(0));
            UserName = usrName;
            SessionHash = GetHash(usrName, pswd);
        }



        public static String GetHash(String usrName, String pswd)
        {
            String saltedUser = usrName + pswd.Trim() + DateTime.Now;
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] bytes = sha1.ComputeHash(
                Encoding.Unicode.GetBytes(saltedUser));
            return Convert.ToBase64String(bytes);
        }


        private void EndCallback(object state)
        {
            _endTimer.Dispose();
            OnEnded(new EventArgs());
        }


        public void ResetTimer()
        {
            _endTimer.Change(new TimeSpan(1, 0, 0, 0), new TimeSpan(0));
        }
    }
}
