﻿using System;
using System.IO;
using System.Net.Sockets;

namespace Planner
{
    /// <summary>
    /// Класс для хранения состояния при асинхронном чтении данных от клиента
    /// </summary>
    internal sealed class MessageStateObject
    {
        // 1 Кб
        public const int BufferSize = 1024;

        // текущий работающий сокет. связывается с конкретным клиентом
        public Socket WorkSocket { get; set; }
        public bool IsComplete { get; private set; }


        private readonly byte[] _buff = new byte[BufferSize];
        public byte[] Buffer
        {
            get { return _buff; }
        }
        // буфер для того, чтобы собрать полное сообщения от клиета
        private readonly MemoryStream _memory = new MemoryStream();
        // длина только самого сообщения
        private int _messageLength = -1;



        private void ResetState()
        {
            IsComplete = false;
            _messageLength = -1;
            _memory.SetLength(0);
            _memory.Seek(0, SeekOrigin.Begin);

        }


        public MessageStateObject(Socket workSocket)
        {
            WorkSocket = workSocket;
            IsComplete = false;
        }


        public void FlushBuffer(int bytesRead)
        {
            _memory.Write(_buff, 0, bytesRead);

            if ( _messageLength == -1 ) {
                if ( _memory.Length < 4 )
                    return;

                byte[] bytes = new byte[4];
                _memory.Seek(0, SeekOrigin.Begin);
                _memory.Read(bytes, 0, 4);

                //                if ( BitConverter.IsLittleEndian )
                //                    Array.Reverse(bytes);

                _messageLength = BitConverter.ToInt32(bytes, 0);

                _memory.Seek(0, SeekOrigin.End);
            }

            if ( _memory.Length - 4 == _messageLength )
                IsComplete = true;
        }


        public byte[] GetMessage()
        {
            //            memory.Seek(0, SeekOrigin.Begin);
            //            byte[] bytes = memory.ToArray();

            _memory.Seek(4, SeekOrigin.Begin);
            byte[] bytes = new byte[_memory.Length - 4];
            _memory.Read(bytes, 0, bytes.Length);

            ResetState();
            return bytes;
        }

    }
}
