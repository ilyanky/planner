﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner
{
    public sealed class TaskLogInfo
    {
        public int TaskID { get; set; }
        public int ScheduleID { get; set; }
        public String ScheduleName { get; set; }
        public String TaskName { get; set; }
        public DateTime DateTime { get; set; }
        public String TypeName { get; set; }
        public String Description { get; set; }
        public String ExitCode { get; set; }
    }
}
