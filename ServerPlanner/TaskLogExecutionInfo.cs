﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner
{
    public sealed class TaskLogExecutionInfo
    {
        public int ScheduleID { get; set; }
        public String ScheduleName { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int ExitCode { get; set; }
    }
}
