﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner
{
    public sealed class UserLogInfo
    {
        public String UserName { get; set; }
        public DateTime DateTime { get; set; }
        public String TypeName { get; set; }
        public String TaskName { get; set; }
        public String Description { get; set; }
    }
}
