﻿using System;
using System.Net;
using System.Net.Configuration;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Planner
{
    public sealed class ClientPlanner
    {
        private readonly Socket _client;
        private String _sessionHash;

        private DateTime _lastTasksModified;
        private DateTime _lastTasksStatusesModified;

        //public String StatusMsg { get; private set; }
        //public String ErrorMsg { get; private set; }
        //public TaskInfo[] TaskList { get; private set; }
        //public String[] TaskTypes { get; private set; }
        //public IList<TaskInfo> Statuses { get; private set; }
        //public float CpuPerformance { get; private set; }
        //public DateTime ServerTime { get; private set; }
        //public UserLogInfo[] UserLog { get; private set; }
        //public TaskLogInfo[] TaskLog { get; private set; }

        public String UserName { get; private set; }
        public bool Connected { get; private set; }






        private Object _lockObj = new Object();



        //        public ClientPlanner(int port)
        //        {
        //            _client = new Socket(AddressFamily.InterNetwork,
        //                SocketType.Stream, ProtocolType.Tcp);
        //            _client.Connect(IPAddress.Any, port);
        //
        //            Console.WriteLine("Connected");
        //        }

        public ClientPlanner()
        {
            _client = new Socket(AddressFamily.InterNetwork, SocketType.Stream,
                ProtocolType.Tcp);

            if ( File.Exists("./hash") )
                _sessionHash = File.ReadAllText("./hash");
            //else
            //    File.Create("./hash");

            _lastTasksModified = DateTime.MinValue;
            _lastTasksStatusesModified = DateTime.MinValue;
        }


        public ClientPlanner(String dns, int port) : this()
        {
            _client.Connect(dns, port);
            Connected = true;
        }



        public bool Connect(String dns, int port, StringBuilder errorMsg)
        {
            try {
                _client.Connect(dns, port);
                Connected = true;
                return true;
            } catch ( Exception e ) {
                errorMsg.Append(e);
                return false;
            }
        }



        public bool TryRestoreLastConnection(String dns, int port)
        {
            try {
                _client.Connect(dns, port);
                Connected = true;
                String temp;
                return CheckSessionHash(out temp);
            } catch ( Exception e ) {
                return false;
            }
        }



        private Request SendRequest(Request request, StringBuilder b)
        {
            lock ( _lockObj ) {

                request.SetValue("sessionHash", _sessionHash);

                byte[] bytes = request.GetUnicodeBytes();
                byte[] sizeBytes = BitConverter.GetBytes(bytes.Length);

                //            if ( BitConverter.IsLittleEndian )
                //                Array.Reverse(size);

                byte[] res = new byte[bytes.Length + sizeBytes.Length];
                for ( int i = 0; i < sizeBytes.Length; ++i )
                    res[i] = sizeBytes[i];
                int j = 0;
                for ( int i = sizeBytes.Length; i < res.Length; ++i ) {
                    res[i] = bytes[j++];
                }

                _client.Send(res);

                //b.Append("Debug: Отправлено: " + _client.Send(sizeBytes) + "\n");
                //b.Append("Debug: Отправлено: " + _client.Send(bytes) + "\n");
                //b.Append("Debug: Waiting for respond from a server" + "\n");

                sizeBytes = new byte[4];
                _client.Receive(sizeBytes);
                int dsize = BitConverter.ToInt32(sizeBytes, 0);


                bytes = new byte[dsize];
                int temp = 0;
                while ( temp != dsize )
                    temp += _client.Receive(bytes, temp, dsize - temp, 0);
                //_client.Receive()


                return Request.GetRequest(bytes);
            }
        }



        public bool CheckSessionHash(out String errorMsg)
        {
            if ( _sessionHash == null ) {
                errorMsg = null;
                return false;
            }

            StringBuilder msg = new StringBuilder();

            Request request = new Request(RequestType.CheckSessionHash);
            request.SetValue("sessionHash", _sessionHash);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                return false;
            }

            if ( !request.GetBool("contains") ) {
                errorMsg = request.ErrorMsg;
                return false;
            }


            UserName = request.GetString("userName");
            errorMsg = null;
            return true;
        }



        public bool LogIn(String login, String pswd, out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            // исключения

            Request request = new Request(RequestType.LogIn);
            request.SetValue("userName", login)
                   .SetValue("password", pswd);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                return false;
            }

            _sessionHash = request.GetString("sessionHash");
            UserName = login;
            File.WriteAllText("./hash", _sessionHash, Encoding.Unicode);
            errorMsg = null;
            return true;
        }



        public bool LogOut(out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            // исключения

            Request request = new Request(RequestType.LogOut);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                return false;
            }

            UserName = null;
            _sessionHash = null;
            File.Delete("./hash");
            errorMsg = null;
            return true;
        }




        public bool UpdateTasks(DateTime fromDt, DateTime toDt,
            bool onlyActiveTasks, bool onlyActiveSchedules,
            out TaskInfo[] taskList, out TaskInfo[] statuses,
            out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request(RequestType.UpdateTasks);
            request.SetValue("fromDateTime", fromDt).
                SetValue("toDateTime", toDt).
                SetValue("lastTasksModified", _lastTasksModified).
                SetValue("lastTasksStatusesModified", _lastTasksStatusesModified).
                SetValue("onlyActiveTasks", onlyActiveTasks).
                SetValue("onlyActiveSchedules", onlyActiveSchedules);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                taskList = null;
                statuses = null;
                return false;
            }

            bool updated = request.GetBool("updated");
            if ( !updated ) {
                taskList = null;
                errorMsg = null;
                statuses = null;
                return true;
            }


            taskList = request.GetTaskInfoArray("taskList");
            statuses = request.GetTaskInfoArray("statuses");
            _lastTasksModified = request.GetDateTime("lastTasksModified");
            _lastTasksStatusesModified = request.GetDateTime("lastTasksStatusesModified");
            errorMsg = null;
            return true;
        }



        public bool GetTasks(DateTime fromDt, DateTime toDt,
            bool onlyActiveTasks, bool onlyActiveSchedules,
            out TaskInfo[] taskList, out TaskInfo[] statuses,
            out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request(RequestType.GetTasks);
            request.SetValue("fromDateTime", fromDt).
                SetValue("toDateTime", toDt).
                SetValue("onlyActiveTasks", onlyActiveTasks).
                SetValue("onlyActiveSchedules", onlyActiveSchedules);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                taskList = null;
                statuses = null;
                return false;
            }


            taskList = request.GetTaskInfoArray("taskList");
            statuses = request.GetTaskInfoArray("statuses");
            errorMsg = null;
            return true;
        }




        public bool GetTaskTypes(out String[] types, out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request(RequestType.GetTaskTypes);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = "Что-то пошло не так";
                types = null;
                return false;
            }


            errorMsg = null;
            types = request.GetStringArray("types");
            return true;
        }





        public bool GetTaskVersion(out String version, out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request(RequestType.GetTaskVersion);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = "Что-то пошло не так";
                version = null;
                return false;
            }


            errorMsg = null;
            version = request.GetString("version");
            return true;
        }




        public bool GetTaskStatutes(out TaskInfo[] statuses, out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request(RequestType.GetTaskStatuses);
            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                statuses = null;
                return false;
            }

            statuses = request.GetTaskInfoArray("statuses");
            errorMsg = null;
            return true;
        }



        public bool GetCpuPerformance(int taskID, out Tuple<int, float>[] cpuPerf,
            out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request(RequestType.GetCpuPerformance);
            request.SetValue("taskID", taskID);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                cpuPerf = null;
                return false;
            }

            cpuPerf = request.GetPerformanceTuple("cpuPerformance");
            if ( cpuPerf == null ) {
                errorMsg = "Задача еще не запущена";
                return false;
            }

            errorMsg = null;
            return true;
        }



        public bool AddTask(TaskInfo taskInfo, out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.SetValue("taskInfo", taskInfo);
            request.Type = RequestType.AddTask;

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                return false;
            }

            errorMsg = null;
            return true;
        }



        public bool ActivateTask(int taskID, out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.SetValue("taskID", taskID);
            request.SetValue("userName", UserName);
            request.Type = RequestType.ActivateTask;

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                return false;
            }


            errorMsg = null;
            return true;
        }



        public bool AddSchedule(int taskID, ScheduleInfo schedInfo,
            out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.SetValue("taskID", taskID);
            request.SetValue("schedule", schedInfo);
            request.Type = RequestType.AddSchedule;

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                return false;
            }


            errorMsg = null;
            return true;
        }



        public bool ActivateSchedule(int taskID, int scheduleID,
            out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.SetValue("taskID", taskID);
            request.SetValue("scheduleID", scheduleID);
            request.SetValue("userName", UserName);
            request.Type = RequestType.ActivateSchedule;

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                return false;
            }


            errorMsg = null;
            return true;
        }



        public bool RemoveTask(int taskID, out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.SetValue("taskID", taskID);
            request.SetValue("userName", UserName);
            request.Type = RequestType.RemoveTask;

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                return false;
            }


            errorMsg = null;
            return true;
        }



        public bool DeactivateTask(int taskID, out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.SetValue("taskID", taskID);
            request.SetValue("userName", UserName);
            request.Type = RequestType.DeactivateTask;

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                return false;
            }


            errorMsg = null;
            return true;
        }




        public bool DeleteSchedule(int taskID, int schedID,
            out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.SetValue("schedID", schedID);
            request.SetValue("taskID", taskID);
            request.SetValue("userName", UserName);
            request.Type = RequestType.RemoveSchedule;

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                return false;
            }


            errorMsg = null;
            return true;
        }




        public bool DeactivateSchedule(int taskID, int schedID,
            out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.SetValue("schedID", schedID);
            request.SetValue("taskID", taskID);
            request.SetValue("userName", UserName);
            request.Type = RequestType.DeactivateSchedule;

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                return false;
            }


            errorMsg = null;
            return true;
        }




        public bool GetServerTime(out DateTimeOffset serverTime,
            out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.Type = RequestType.GetServerTime;

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                serverTime = DateTimeOffset.MinValue;
                return false;
            }


            errorMsg = null;
            serverTime = request.GetDateTime("serverTime");
            return true;
        }





        public bool GetUserLog(out UserLogInfo[] userLog, out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.Type = RequestType.GetUserLog;
            request.SetValue("userName", UserName);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                userLog = null;
                return false;
            }


            errorMsg = null;
            userLog = request.GetUserLogArray("userLog");
            return true;
        }




        public bool GetTaskLog(int taskID, out TaskLogInfo[] taskLog,
            out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.Type = RequestType.GetTaskLog;
            request.SetValue("taskID", taskID);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                taskLog = null;
                return false;
            }


            errorMsg = null;
            taskLog = request.GetTaskLogArray("taskLog");
            return true;
        }




        public bool GetServerLog(out Tuple<DateTime, String>[] serverLog,
            out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.Type = RequestType.GetServerLog;

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                serverLog = null;
                return false;
            }


            errorMsg = null;
            serverLog = request.GetServerLogTuple("serverLog");
            return true;
        }




        public bool GetTaskExecutionLog(int taskID, DateTime day,
            out TaskLogExecutionInfo[] log, out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.Type = RequestType.GetTaskExecutionLog;
            request.SetValue("taskID", taskID);
            request.SetValue("day", day);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                log = null;
                return false;
            }


            errorMsg = null;
            log = request.GetTaskExecutionLogArray("taskExecutionLog");
            return true;
        }



        public bool GetStoredCpuPerformance(TaskLogExecutionInfo info,
            out float[] perfLog, out String errorMsg)
        {
            StringBuilder msg = new StringBuilder();

            Request request = new Request();
            request.Type = RequestType.GetStoredCpuPerformance;
            request.SetValue("execInfo", info);

            request = SendRequest(request, msg);

            if ( !request.IsSuccessful ) {
                errorMsg = request.ErrorMsg;
                perfLog = null;
                return false;
            }


            errorMsg = null;
            perfLog = request.GetFloatArray("perfLog");
            return true;
        }




        public void Close()
        {
            _client.Shutdown(SocketShutdown.Both);
            _client.Close();
        }
    }
}
