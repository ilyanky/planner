﻿using System;
using System.Collections.Generic;
using System.Reflection;

using System.Diagnostics;
using System.IO;

namespace Planner
{
    public sealed class TaskExecutor
    {
        public enum ExitCode
        {
            Successful,
            NonCritical,
            WrongUsage,
            UnknownTask,
            ErrorInTask
        }


        private static readonly
            Dictionary<String, Func<String[], ExitCode>> _paramMap;


        static TaskExecutor()
        {
            _paramMap =
                new Dictionary<String, Func<String[], ExitCode>>();
            _paramMap.Add("-h", Help);
            _paramMap.Add("-r", Run);
            _paramMap.Add("-t", Types);
        }



        public static String[] GetTypes()
        {
            List<String> list = new List<String>();

            var methodInfo = (typeof(TaskLib.TaskLib)).GetMethods(
                BindingFlags.Public | BindingFlags.Static);
            foreach ( var method in methodInfo ) {
                if ( !method.IsSpecialName )
                    list.Add(method.Name);
            }

            return list.ToArray();
        }



        private static void PrintErrorMsg()
        {
            Console.Error.WriteLine("Ошибка! Неизвестные парамаетры.");
            Console.Error.WriteLine("Использование: ... Пример: ....");
            Console.Error.WriteLine("Введите 'TaskExecutor.exe -h' для " +
                                    "получения краткой спраки по использованию");
        }


        private static int Execute(String[] args)
        {
            Console.WriteLine("all good");
            Console.WriteLine("params: ");
            foreach ( var param in args ) {
                Console.WriteLine(param);
            }

            var exitCode = (int)_paramMap[args[0].Trim()](args);
            return exitCode;
        }




        private static ExitCode Help(String[] args)
        {
            if ( args.Length != 1 ) {
                PrintErrorMsg();
                return ExitCode.WrongUsage;
            }

            Console.WriteLine("Справка...");
            return ExitCode.Successful;
        }


        private static ExitCode Run(String[] args)
        {
            if ( args.Length != 2 ) {
                PrintErrorMsg();
                return ExitCode.WrongUsage;
            }

            var methodInfo = (typeof(TaskLib.TaskLib)).GetMethod(args[1].Trim());
            if ( methodInfo == null ) {
                Console.Error.WriteLine("Ошибка! " +
                                        "Задача с таким именем " +
                                        "отсутствует в библиотеке");
                return ExitCode.UnknownTask;
            }

            int result = (int)methodInfo.Invoke(null, null);
            if ( result != 0 )
                return ExitCode.ErrorInTask;

            return ExitCode.Successful;
        }


        private static ExitCode Types(String[] args)
        {
            var methodInfo = (typeof(TaskLib.TaskLib)).GetMethods(
                BindingFlags.Public | BindingFlags.Static);
            foreach ( var method in methodInfo ) {
                if ( !method.IsSpecialName )
                    Console.WriteLine(method.Name);
            }

            return ExitCode.Successful;
        }



        public static DateTime LastModified()
        {
            return File.GetLastWriteTime("./TaskLib.dll");
        }


        public static Version GetVersion()
        {
            return Assembly.GetAssembly(typeof(TaskLib.TaskLib)).GetName().Version;
        }



        static int Main(string[] args)
        {
            if ( args.Length < 1 ) {
                PrintErrorMsg();
                return (int)ExitCode.WrongUsage;
            }

            return Execute(args);
        }
    }
}
