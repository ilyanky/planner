﻿namespace Planner
{
    partial class DayCell
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dayLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dayLabel
            // 
            this.dayLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dayLabel.BackColor = System.Drawing.Color.Transparent;
            this.dayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dayLabel.ForeColor = System.Drawing.Color.DimGray;
            this.dayLabel.Location = new System.Drawing.Point(0, 0);
            this.dayLabel.Name = "dayLabel";
            this.dayLabel.Size = new System.Drawing.Size(214, 106);
            this.dayLabel.TabIndex = 0;
            this.dayLabel.Text = "0";
            this.dayLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.dayLabel.Click += new System.EventHandler(this.dayLabel_Click);
            this.dayLabel.MouseCaptureChanged += new System.EventHandler(this.dayLabel_MouseCaptureChanged);
            this.dayLabel.MouseEnter += new System.EventHandler(this.DayLabel_MouseEnter);
            this.dayLabel.MouseLeave += new System.EventHandler(this.DayLabel_MouseLeave);
            // 
            // DayCell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.dayLabel);
            this.DoubleBuffered = true;
            this.Name = "DayCell";
            this.Size = new System.Drawing.Size(214, 106);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label dayLabel;
    }
}
