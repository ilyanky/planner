﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Planner
{
    public partial class NewScheduleForm : Form
    {
        private Panel _currentPanel;
        private RadioButton _currentRadioButton;

        public ScheduleInfo ScheduleInfo { get; private set; }


        public NewScheduleForm()
        {
            InitializeComponent();

            ScheduleInfo = new ScheduleInfo();

            ToggleNameDescriptionPanel();
        }


        private static void PaintBackground(Control control, Color first,
            Color second, Single angle, PaintEventArgs e)
        {
            Rectangle r = new Rectangle(control.Location, control.Size);
            using ( var brush =
                new LinearGradientBrush(r, first, second, angle) ) {
                e.Graphics.FillRectangle(brush, r);
            }
        }


        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            PaintBackground(topPanel,
                Color.DodgerBlue, Color.SteelBlue, 90F,
                e);
        }


        private void AddTaskForm_Resize(object sender, EventArgs e)
        {
            //            topPanel.Invalidate();
            //            contentPanel.Invalidate();
            //            bottomPanel.Invalidate();

            this.Invalidate();
        }



        private String GetDaysString()
        {
            String result = String.Empty;

            if ( mondayCheckBox.Checked )
                result = ", Понедельник";
            if ( tuesdayCheckBox.Checked )
                result += ", Вторник";
            if ( wednesdayCheckBox.Checked )
                result += ", Среда";
            if ( thursdayCheckBox.Checked )
                result += ", Четверг";
            if ( fridayCheckBox.Checked )
                result += ", Пятница";
            if ( saturdayCheckBox.Checked )
                result += ", Суббота";
            if ( sundayCheckBox.Checked )
                result += ", Воскресенье";

            result = result.Remove(0, 2);

            return result;
        }


        private String CollectDaysOfWeek()
        {
            String result = String.Empty;

            if ( mondayCheckBox.Checked )
                result = ", 0";
            if ( tuesdayCheckBox.Checked )
                result += ", 1";
            if ( wednesdayCheckBox.Checked )
                result += ", 2";
            if ( thursdayCheckBox.Checked )
                result += ", 3";
            if ( fridayCheckBox.Checked )
                result += ", 4";
            if ( saturdayCheckBox.Checked )
                result += ", 5";
            if ( sundayCheckBox.Checked )
                result += ", 6";

            if ( result.Length != 0 )
                result = result.Remove(0, 2);

            return result;
        }


        private void nextButton_Click(object sender, EventArgs e)
        {
            if ( _currentPanel.Name == nameDescriptionPanel.Name ) {
                if ( nameTextBox.Text == String.Empty ) {
                    MessageBox.Show("Имя не может быть пустым", "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ToggleNameDescriptionPanel();
                ToggleTriggersPanel();
            } else if ( _currentPanel.Name == triggersPanel.Name ) {
                ToggleTriggersPanel();
                ToggleChosenTrigger();
            } else if ( _currentPanel.Name == addSchedFinishPanel.Name ) {

                ScheduleInfo = GetScheduleInfo();
                DialogResult = DialogResult.OK;
                this.Close();

            } else { // все панели, отражающие триггер
                if ( _currentRadioButton == null ) {
                    MessageBox.Show("Выберите нужный триггер");
                    return;
                }
                ToggleChosenTrigger();
                ToggleAddSchedFinishPanel();
            }
        }



        private ScheduleInfo GetScheduleInfo()
        {
            var schedInfo = new ScheduleInfo();
            schedInfo.Name = nameTextBox.Text;
            schedInfo.Description = descriptionTextBox.Text;

            if ( onceRadioButton.Checked ) {

                schedInfo.StartDateTime = GetDateTime(
                    onceDatePicker.Value, onceTimePicker.Value);
                schedInfo.Active = true;
                schedInfo.TriggerName = "Once";

                return schedInfo;

            } else if ( dailyRadioButton.Checked ) {

                schedInfo.StartDateTime = GetDateTime(
                    dailyDatePicker.Value, dailyTimePicker.Value);
                schedInfo.Active = true;
                schedInfo.EndDate = dailyEndDatePicker.Value;
                schedInfo.RepeatEvery = Int32.Parse(everyDayTextBox.Text);
                schedInfo.TriggerName = "Daily";

                return schedInfo;

            } else if ( weeklyRadioButton.Checked ) {

                schedInfo.StartDateTime = GetDateTime(
                    weeklyDatePicker.Value, weeklyTimePicker.Value);
                schedInfo.Active = true;
                schedInfo.EndDate = weeklyEndDatePicker.Value;
                schedInfo.RepeatEvery = Int32.Parse(everyWeekTextBox.Text);
                schedInfo.DaysOfWeek = CollectDaysOfWeek();
                schedInfo.TriggerName = "Weekly";

                return schedInfo;

            } else { // if ( monthlyRadioButton.Checked )

                schedInfo.StartDateTime = GetDateTime(
                    monthlyDatePicker.Value, monthlyTimePicker.Value);
                schedInfo.Active = true;
                schedInfo.TriggerName = "Monthly";
                schedInfo.EndDate = monthlyEndDatePicker.Value;
                schedInfo.Months = monthPicker1.Months;
                if ( inNumbersRadioButton.Checked ) {
                    schedInfo.Days = dayOfMonthPicker1.Days;
                } else {
                    schedInfo.EveryDay = everyDayPicker1.EveryDay;
                    schedInfo.DaysOfWeek = weekDayPicker1.DaysOfWeek;
                }

                return schedInfo;
            }
        }



        private void prevButton_Click(object sender, EventArgs e)
        {
            if ( _currentPanel.Name.Equals(
                triggersPanel.Name) ) {
                ToggleTriggersPanel();
                ToggleNameDescriptionPanel();
            } else if ( _currentPanel.Name.Equals(
                addSchedFinishPanel.Name) ) {
                ToggleAddSchedFinishPanel();
                ToggleChosenTrigger();
            } else { // все панели, отражающие дни
                ToggleChosenTrigger();
                ToggleTriggersPanel();
            }
        }



        private void ToggleChosenTrigger()
        {
            if ( onceRadioButton.Checked ) {
                ToggleOnceTriggeringPanel();
                _currentRadioButton = onceRadioButton;
            } else if ( dailyRadioButton.Checked ) {
                ToggleDailyTriggeringPanel();
                _currentRadioButton = dailyRadioButton;
            } else if ( weeklyRadioButton.Checked ) {
                ToggleWeeklyTriggeringPanel();
                _currentRadioButton = weeklyRadioButton;
            } else if ( monthlyRadioButton.Checked )  {
                ToggleMonthlyTriggeringPanel();
                _currentRadioButton = monthlyRadioButton;
            } else {
                _currentRadioButton = null;
            }
        }


        private void ToggleNameDescriptionPanel()
        {
            if ( nameDescriptionPanel.Visible ) {
                nameDescriptionPanel.Visible = false;
                prevButton.Visible = true;
                chooseNameLabel.Font =
                    new Font(chooseNameLabel.Font, FontStyle.Regular);
            } else {
                _currentPanel = nameDescriptionPanel;
                nameDescriptionPanel.Visible = true;
                prevButton.Visible = false;
                chooseNameLabel.Font =
                    new Font(chooseNameLabel.Font, FontStyle.Underline);
            }
        }


        private void ToggleTriggersPanel()
        {
            if ( triggersPanel.Visible ) {
                triggersPanel.Visible = false;
                chooseTriggerLabel.Font =
                    new Font(chooseTriggerLabel.Font, FontStyle.Regular);
            } else {
                _currentPanel = triggersPanel;
                triggersPanel.Visible = true;
                chooseTriggerLabel.Font =
                    new Font(chooseTriggerLabel.Font, FontStyle.Underline);
            }
        }


        private void ToggleOnceTriggeringPanel()
        {
            if ( onceTriggeringPanel.Visible ) {
                onceTriggeringPanel.Visible = false;
                triggerLabel.Text = "Триггер";
                triggerLabel.Font =
                    new Font(triggerLabel.Font, FontStyle.Regular);
            } else {
                _currentPanel = onceTriggeringPanel;
                onceTriggeringPanel.Visible = true;
                triggerLabel.Text = "Однократно";
                triggerLabel.Font =
                    new Font(triggerLabel.Font, FontStyle.Underline);
            }
        }


        private void ToggleDailyTriggeringPanel()
        {
            if ( dailyTriggeringPanel.Visible ) {
                dailyTriggeringPanel.Visible = false;
                triggerLabel.Text = "Триггер";
                triggerLabel.Font =
                    new Font(triggerLabel.Font, FontStyle.Regular);
            } else {
                _currentPanel = dailyTriggeringPanel;
                dailyTriggeringPanel.Visible = true;
                triggerLabel.Text = "Ежедневно";
                triggerLabel.Font =
                    new Font(triggerLabel.Font, FontStyle.Underline);
            }
        }


        private void ToggleWeeklyTriggeringPanel()
        {
            if ( weeklyTriggeringPanel.Visible ) {
                weeklyTriggeringPanel.Visible = false;
                triggerLabel.Text = "Триггер";
                triggerLabel.Font =
                    new Font(triggerLabel.Font, FontStyle.Regular);
            } else {
                _currentPanel = weeklyTriggeringPanel;
                weeklyTriggeringPanel.Visible = true;
                triggerLabel.Text = "Еженедельно";
                triggerLabel.Font =
                    new Font(triggerLabel.Font, FontStyle.Underline);
            }
        }


        private void ToggleMonthlyTriggeringPanel()
        {
            if ( monthlyTriggeringPanel.Visible ) {
                monthlyTriggeringPanel.Visible = false;
                triggerLabel.Text = "Триггер";
                triggerLabel.Font =
                    new Font(triggerLabel.Font, FontStyle.Regular);
            } else {
                _currentPanel = monthlyTriggeringPanel;
                monthlyTriggeringPanel.Visible = true;
                triggerLabel.Text = "Ежемесячно";
                triggerLabel.Font =
                    new Font(triggerLabel.Font, FontStyle.Underline);
            }
        }


        private void ToggleAddSchedFinishPanel()
        {
            if ( addSchedFinishPanel.Visible ) {
                addSchedFinishPanel.Visible = false;
                finishLabel.Font =
                    new Font(finishLabel.Font, FontStyle.Regular);
                nextButton.Text = "Далее >";
            } else {
                _currentPanel = addSchedFinishPanel;
                addSchedFinishPanel.Visible = true;
                finishLabel.Font =
                    new Font(finishLabel.Font, FontStyle.Underline);
                nextButton.Text = "Готово";

                finalNameTextBox.Text = nameTextBox.Text;
                finalDescriptionTextBox.Text = descriptionTextBox.Text;
                var sched = GetScheduleInfo();
                finalMoreTextBox.Text = sched.GetDetails();

                if ( onceRadioButton.Checked ) {

                    finalTriggerTextBox.Text = onceRadioButton.Text;
                    //finalMoreTextBox.Text = "Запустить " + onceDatePicker.Text +
                    //    " в " + onceTimePicker.Text;

                } else if ( dailyRadioButton.Checked ) {

                    finalTriggerTextBox.Text = dailyRadioButton.Text;
                    //finalMoreTextBox.Text = "Начать с " + dailyDatePicker.Text +
                    //    " в " + dailyTimePicker.Text + ". Запускать каждый " +
                    //    everyDayTextBox.Text + " день до " +
                    //    dailyEndDatePicker.Text;

                } else if ( weeklyRadioButton.Checked ) {

                    finalTriggerTextBox.Text = weeklyRadioButton.Text;
                    //finalMoreTextBox.Text = "Начать с " + weeklyDatePicker.Text +
                    //    " в " + weeklyTimePicker.Text + ". Запускать каждую " +
                    //    everyWeekTextBox.Text + " неделю по следующим дням: " +
                    //    GetDaysString() + ", до " + weeklyEndDatePicker.Text;

                } else {

                    finalTriggerTextBox.Text = monthlyRadioButton.Text;
                    

                }
            }
        }


        private DateTime GetDateTime(DateTime date, DateTime time)
        {
            return new DateTime(date.Year, date.Month, date.Day,
                time.Hour, time.Minute, time.Second);
        }


        private void inNumbersRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if ( inNumbersRadioButton.Checked ) {
                daysLabel.Enabled = true;
                dayOfMonthPicker1.Enabled = true;

            } else {
                daysLabel.Enabled = false;
                dayOfMonthPicker1.Enabled = false;
            }
        }


        private void inDaysRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if ( inDaysRadioButton.Checked ) {
                everyDayPicker1.Enabled = true;
                weekDayPicker1.Enabled = true;
                monthLabel.Enabled = true;
                everyLabel.Enabled = true;
            } else {
                everyDayPicker1.Enabled = false;
                weekDayPicker1.Enabled = false;
                monthLabel.Enabled = false;
                everyLabel.Enabled = false;
            }
        }
    }
}
