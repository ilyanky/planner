﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Linq;

namespace Planner
{
    public partial class DayCell : UserControl
    {
        private int _day;
        private LinkedList<TaskCircle> _taskCircleList;


        //[Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        //public bool TaskCircle
        //{
        //    get { return this.taskCircle1.Visible; }
        //    set { this.taskCircle1.Visible = value; }
        //}


        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public override Color ForeColor
        {
            get { return dayLabel.ForeColor; }
            set { dayLabel.ForeColor = value; }
        }


        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public int Day
        {
            get { return _day; }
            set {
                if ( value < 1 || value > 31 )
                    return;

                _day = value;
                dayLabel.Text = _day.ToString();
            }
        }




        public DayCell()
        {
            _taskCircleList = new LinkedList<TaskCircle>();

            InitializeComponent();
        }




        public TaskInfo[] GetTasks()
        {
            TaskInfo[] result = new TaskInfo[_taskCircleList.Count];
            int count = 0;
            foreach ( var circle in _taskCircleList ) {
                result[count] = circle.TaskInfo;
                ++count;
            }

            return result;
        }


        public TaskInfo[] GetStatuses()
        {
            TaskInfo[] result = new TaskInfo[_taskCircleList.Count];
            int count = 0;
            foreach ( var circle in _taskCircleList ) {
                result[count] = circle.TaskStatus;
                ++count;
            }

            return result;
        }


        public bool ContainsTask(int taskID)
        {
            foreach ( var circle in _taskCircleList )
                if ( circle.TaskInfo.id == taskID )
                    return true;
            return false;
        }


        public void AddTaskCircle(TaskInfo taskInfo)
        {
            TaskCircle taskCircle = new TaskCircle(taskInfo);

            if ( _taskCircleList.Count == 0 ) {
                int x = this.Width - taskCircle.Width;
                int y = this.Height - taskCircle.Height;
                taskCircle.Location = new Point(x, y);
            } else {
                int x = _taskCircleList.First.Value.Location.X -
                    taskCircle.Width;
                int y = _taskCircleList.First.Value.Location.Y;
                taskCircle.Location = new Point(x, y);
            }
            taskCircle.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            taskCircle.Name = "taskCircle" + _taskCircleList.Count + 1;

            if ( taskInfo.Status == TaskStatus.Error )
                taskCircle.BackColor = Color.Red;
            else if ( taskInfo.Status == TaskStatus.Processing )
                taskCircle.BackColor = Color.Yellow;
            else if ( taskInfo.Status == TaskStatus.Waiting )
                taskCircle.BackColor = Color.DodgerBlue;
            else // if ( taskInfo.Status == TaskStatus.Complete )
                ;


            this.Controls.Add(taskCircle);
            taskCircle.BringToFront();

            _taskCircleList.AddFirst(taskCircle);
        }


        public void SetStatus(TaskInfo info)
        {
            //MessageBox.Show(info.ToString());


            foreach ( var circle in _taskCircleList ) {
                if ( circle.TaskInfo.id == info.id ) {
                    //foreach ( var sched in circle.TaskInfo.Schedules  ) {
                    //    var temp = info.Schedules.FirstOrDefault((x) => x.Name == sched.Name);
                    //    if ( temp == null )
                    //        continue;
                    //    sched.Status = temp.Status;

                    circle.TaskStatus = info;
                    if ( info.Status == TaskStatus.Error )
                        circle.BackColor = Color.Red;
                    else if ( info.Status == TaskStatus.Processing )
                        circle.BackColor = Color.Gold;
                    else if ( info.Status == TaskStatus.Waiting )
                        circle.BackColor = Color.DodgerBlue;
                    else if ( info.Status == TaskStatus.Complete )
                        circle.BackColor = Color.Green;
                    else if ( info.Status == TaskStatus.Inactive )
                        circle.BackColor = Color.Gray;
                    //}
                }
            }
        }


        public void ClearTasks()
        {
            foreach ( var circle in _taskCircleList )
                this.Controls.Remove(circle);

            _taskCircleList.Clear();
        }



        public void PaintBackground()
        {
            dayLabel.BorderStyle = BorderStyle.FixedSingle;
            this.Paint += paintBackground;
            this.Invalidate();
        }


        public void ClearBackgroud()
        {
            dayLabel.BorderStyle = BorderStyle.None;
            this.Paint -= paintBackground;
            this.Invalidate();
        }


        private void DayLabel_MouseEnter(object sender, EventArgs e)
        {
            BorderStyle = BorderStyle.FixedSingle;
        }


        private void DayLabel_MouseLeave(object sender, EventArgs e)
        {
            //if ( this.taskCircle1.ClientRectangle.
            //    Contains(this.taskCircle1.PointToClient(Control.MousePosition)) )
            //    return;

            BorderStyle = BorderStyle.None;
        }


        private void dayLabel_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }


        private void paintBackground(object sender, PaintEventArgs e)
        {
            using ( var b = new LinearGradientBrush(
                e.ClipRectangle, Color.Black, Color.Black, 90F, false) ) {
                ColorBlend cb = new ColorBlend();
                cb.Positions = new[] { 0, 0.5f, 1 };
                cb.Colors = new[] {
                    Color.FromArgb(80, Color.SteelBlue),
                    Color.FromArgb(80, Color.DodgerBlue),
                    Color.FromArgb(80, Color.SteelBlue)
                };
                b.InterpolationColors = cb;

                e.Graphics.FillRectangle(b, e.ClipRectangle);
            }
        }


        private void dayLabel_MouseCaptureChanged(object sender, EventArgs e)
        {
            //if ( this.taskCircle1.ClientRectangle.
            //    Contains(this.taskCircle1.PointToClient(Control.MousePosition)) )
            //    return;

            BorderStyle = BorderStyle.None;
        }
    }
}
