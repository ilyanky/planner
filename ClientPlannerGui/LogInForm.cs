﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using System.Linq;
using System.Text;

namespace Planner
{
    public partial class LogInForm : Form
    {
        private const String LAST_PARAMS_PATH = "./lastParams";

        public ClientPlanner Client { get; set; }

        private String Dns { get; set; }
        private String Port { get; set; }


        public LogInForm(ClientPlanner client)
        {
            InitializeComponent();

            Client = client;

            var temp = ParseLastParams();
            if ( temp.Count == 0 )
                return;

            String lastUserName = temp["lastUserName"];
            maxLoginTextBox.Text = lastUserName;
            minLoginTextBox.Text = lastUserName;

            Dns = temp["lastDns"];
            dnsTextBox.Text = Dns;

            Port = temp["lastPort"];
            portTextBox.Text = Port;
        }


        //public LogInForm(ClientPlanner client) : this()
        //{
        //    Client = client;
        //}


        private Dictionary<String, String> ParseLastParams()
        {
            var lastParams = new Dictionary<String, String>();
            if ( !File.Exists(LAST_PARAMS_PATH) )
                return new Dictionary<string, string>();

            var parameters = File.ReadAllLines(LAST_PARAMS_PATH);
            foreach ( var param in parameters ) {
                var temp = param.Split('=');
                var paramName = temp[0].Trim();
                var paramValue = temp[1].Trim();
                if ( paramName == "lastUserName" )
                    lastParams["lastUserName"] = paramValue;
                else if ( paramName == "lastDns" )
                    lastParams["lastDns"] = paramValue;
                else if ( paramName == "lastPort" )
                    lastParams["lastPort"] = paramValue;
                else
                    return new Dictionary<string, string>();
            }

            return lastParams;
        }




        private static void PaintBackground(Control control, Color first,
            Color second, Single angle, PaintEventArgs e)
        {
            Rectangle r = new Rectangle(control.Location, control.Size);
            using ( var brush =
                new LinearGradientBrush(r, first, second, angle) ) {
                e.Graphics.FillRectangle(brush, r);
            }
        }


        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            PaintBackground(topPanel,
                Color.DodgerBlue, Color.SteelBlue, 90F,
                e);

            PaintBackground(contentPanel,
                Color.AliceBlue, Color.GhostWhite, 90F,
                e);

            PaintBackground(bottomPanel,
                Color.AliceBlue, Color.GhostWhite, 90F,
                e);
        }


        private void LogInForm_Resize(object sender, EventArgs e)
        {
            //            topPanel.Invalidate();
            //            contentPanel.Invalidate();
            //            bottomPanel.Invalidate();

            this.Invalidate();
        }


        private void LogInForm_Load(object sender, EventArgs e)
        {
            int height = topPanel.Height + minLogInPanel.Height +
                         bottomPanel.Height;

            this.ClientSize = new Size(this.ClientSize.Width, height);
        }


        private void paramsButton_Click(object sender, EventArgs e)
        {
            if ( minLogInPanel.Visible ) {
                minLogInPanel.Visible = false;
                maxLogInPanel.Visible = true;
                int height = topPanel.Height + maxLogInPanel.Height +
                             bottomPanel.Height;
                this.ClientSize = new Size(this.ClientSize.Width, height);
                paramsButton.Text = "Параметры <<";
            } else {
                minLogInPanel.Visible = true;
                maxLogInPanel.Visible = false;
                int height = topPanel.Height + minLogInPanel.Height +
                             bottomPanel.Height;
                this.ClientSize = new Size(this.ClientSize.Width, height);
                paramsButton.Text = "Параметры >>";
            }
        }


        private void logInButton_Click(object sender, EventArgs e)
        {
            String login = String.Empty;
            String pswd = String.Empty;
            String dns = String.Empty;
            String port = String.Empty;
            if ( minLogInPanel.Visible ) {
                login = minLoginTextBox.Text;
                pswd = minPswdTextBox.Text;
                //    if ( Dns != null )
                //        dns = Dns;
                //    if ( Port != null )
                //        port = Port;
            } else {
                login = maxLoginTextBox.Text;
                pswd = maxPswdTextBox.Text;
            }
            dns = dnsTextBox.Text;
            port = portTextBox.Text;

            if ( login == String.Empty ) {
                MessageBox.Show("Пожалуйста введите логин.", "Ошибка авторизации!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if ( pswd == String.Empty ) {
                MessageBox.Show("Пожалуйста введите пароль.", "Ошибка авторизации!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if ( dns == String.Empty ) {
                MessageBox.Show("Система не смогла подгрузить последний использующийся DNS, " +
                    "пожалкуста введите новый.", "Ошибка авторизации!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if ( port == String.Empty ) {
                MessageBox.Show("Система не смогла подгрузить последний использующийся порт, " +
                    "пожалкуста введите новый.", "Ошибка авторизации!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int portValue = -1;
            if ( !int.TryParse(port, out portValue) ) {
                MessageBox.Show("Введенный порт имеет неверный формат, " +
                    "пожалкуста введите новый.", "Ошибка авторизации!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if ( portValue <= 0 ) {
                MessageBox.Show("Введенный порт имеет неверный формат, " +
                    "пожалкуйста введите новый.", "Ошибка авторизации!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }



            //if ( dns == Dns ) {
                var errorMsg = new StringBuilder();
                if ( !Client.Connected && !Client.Connect(dns, portValue, errorMsg) ) {
                    MessageBox.Show("Ошибка подключения. Скорее всего серверная сторна программы не запущена.",
                        "Ошибка подключения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                String error;
                if ( !Client.LogIn(login, pswd, out error) ) {
                    MessageBox.Show(error, "Ошибка авторизации",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            //} else {
            //    var errorMsg = new StringBuilder();
            //    if ( !Client.Connect(dns, portValue, errorMsg) ) {
            //        MessageBox.Show("Ошибка подключения. Скорее всего серверная сторна программы не запущена." +
            //            " Описание: \n" + errorMsg.ToString(), "Ошибка подключения!",
            //            MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        return;
            //    }

            //    String error;
            //    if ( !Client.LogIn(login, pswd, out error) ) {
            //        MessageBox.Show(error, "Ошибка авторизации",
            //            MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        return;
            //    }
            //}




            String[] lastParams = new String[3];
            lastParams[0] = "lastUserName = " + login;
            lastParams[1] = "lastDns = " + dns;
            lastParams[2] = "lastPort = " + port;
            File.WriteAllLines(LAST_PARAMS_PATH, lastParams);


            DialogResult = DialogResult.OK;
        }
    }
}
