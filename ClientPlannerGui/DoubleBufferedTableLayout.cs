﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Planner
{
    internal sealed class DoubleBufferedTableLayout : TableLayoutPanel
    {
        public DoubleBufferedTableLayout()
        {
            this.DoubleBuffered = true;
            SetStyle(ControlStyles.CacheText, true);
        }
    }
}
