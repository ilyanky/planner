﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Planner
{
    internal sealed class Program
    {
        private const String LAST_PARAMS_PATH = "./lastParams";


        private static Dictionary<String, String> ParseLastParams()
        {
            var lastParams = new Dictionary<String, String>();
            if ( !File.Exists(LAST_PARAMS_PATH) )
                return new Dictionary<string, string>();

            var parameters = File.ReadAllLines(LAST_PARAMS_PATH);
            foreach ( var param in parameters ) {
                var temp = param.Split('=');
                var paramName = temp[0].Trim();
                var paramValue = temp[1].Trim();

                if ( paramValue == String.Empty )
                    return new Dictionary<string, string>();

                if ( paramName == "lastUserName" )
                    lastParams["lastUserName"] = paramValue;
                else if ( paramName == "lastDns" )
                    lastParams["lastDns"] = paramValue;
                else if ( paramName == "lastPort" )
                    lastParams["lastPort"] = paramValue;
                else
                    return new Dictionary<string, string>();
            }

            return lastParams;
        }


        private static void RunLoginForm()
        {
            var client = new ClientPlanner();
            LogInForm logInForm = new LogInForm(client);
            DialogResult dialogResult = logInForm.ShowDialog();
            if ( dialogResult == DialogResult.Cancel )
                return;

            Application.Run(new Main(client));
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var temp = ParseLastParams();
            if ( temp.Count == 0 ) {
                RunLoginForm();
                return;
            }

            String lastUserName = temp["lastUserName"];
            String lastDns = temp["lastDns"];
            String port = temp["lastPort"];

            int portValue = -1;
            if ( !int.TryParse(port, out portValue) ) {
                RunLoginForm();
                return;
            }

            var client = new ClientPlanner();
            if ( !client.TryRestoreLastConnection(lastDns, portValue) ) {
                RunLoginForm();
                return;
            }

            Application.Run(new Main(client));

            //var tempEntries = new UserLogInfo[2];
            //tempEntries[0] = new UserLogInfo {
            //    UserName = "ilyanky",
            //    DateTime = new DateTime(2015, 9, 21),
            //    Description = "some descr",
            //    TaskName = "wow",
            //    TypeName = "bl"
            //};
            //tempEntries[1] = new UserLogInfo {
            //    UserName = "ilyanky",
            //    DateTime = new DateTime(2015, 9, 21),
            //    Description = "some descr",
            //    TaskName = "wow",
            //    TypeName = "bl"
            //};
            //var temp = new LogViewForm(LogViewForm.LogViewType.UserLog);
            //temp.AddEntries(tempEntries);
            //Application.Run(temp);
        }
    }
}
