﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Planner
{
    public partial class TaskCircle : UserControl
    {
        public TaskInfo TaskInfo { get; private set; }
        public TaskInfo TaskStatus { get; set; }


        public TaskCircle()
        {
            InitializeComponent();

            GraphicsPath path = new GraphicsPath();
//            path.AddEllipse(0, 0, this.Width, this.Height);
            path.AddEllipse(0, 0, 10, 10);
            this.Region = new Region(path);
        }


        public TaskCircle(TaskInfo taskInfo) : this()
        {
            TaskInfo = taskInfo;
        }


        private void TaskCircle_Click(object sender, EventArgs e)
        {
            MessageBox.Show(TaskInfo.ToString());
        }


        //        private void TaskCircle_SizeChanged(object sender, EventArgs e)
        //        {
        //            GraphicsPath path = new GraphicsPath();
        //            path.AddEllipse(0, 0, this.Width, this.Height);
        //            this.Region = new Region(path);
        //        }
    }
}
