﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Planner
{
    public partial class Main : Form
    {
        private ClientPlanner _client;

        public TaskInfo[] CurrentTasks { get; private set; }
        private TaskLogExecutionInfo[] _currentExecInfos;
        private TaskInfo _selectedTask;
        private ScheduleInfo _selectedSchedule;
        private TaskLogExecutionInfo _selectedExecInfo;


        private Panel _currentBottomPanel;
        private Panel _currentActionsPanel;

        private DateTimeOffset _serverTime;



        private ManualResetEvent _syncServerTimeEvent;
        private ManualResetEvent _syncUpdateTasks;
        private ManualResetEvent _syncUpdateDailyTasks;



        private PerformanceForm _perfForm;

        private Object _lockObj;

        public String[] TaskTypes { get; private set; }





        public Main()
        {
            InitializeComponent();

            ToggleGeneralContentPanel();
            ToggleGeneralActionsContentPanel();

            bottomVerticalSplitContainer.Panel1.Enabled = false;

            //dayTaskListView.Visible = true;

            _lockObj = new Object();

            _syncUpdateTasks = new ManualResetEvent(false);
            _syncUpdateDailyTasks = new ManualResetEvent(false);
        }


        public Main(ClientPlanner client) : this()
        {
            _client = client;

            InitializeServerTimeRequest();
            InitializeGetPerformanceRequest();
            InitializeGetTasksRequest();
        }






        // --------------- Таймеры и BackgroundWorker'ы -----------------
        private void InitializeServerTimeRequest()
        {
            _syncServerTimeEvent = new ManualResetEvent(true); // true

            serverTimeSynchronizer.DoWork += serverTimeSynchronizer_DoWork;
            serverTimeSynchronizer.RunWorkerCompleted +=
                serverTimeUpdater_RunWorkerCompleted;
            serverTimeSynchronizer.RunWorkerAsync();

            serverTimeSyncTimer.Tick += serverTimeSyncTimer_Tick;
            serverTimeSyncTimer.Interval = 300000; // каждые 5 минут 
            serverTimeSyncTimer.Start();

            serverTimeTimer.Interval = 1000;
            serverTimeTimer.Tick += serverTimeTimer_Tick;
            //serverTimeTimer.Start();
        }


        private void InitializeGetPerformanceRequest()
        {
            performance_BackgroundWorker.DoWork +=
                performance_BackgroundWorker_DoWork;
            performance_BackgroundWorker.RunWorkerCompleted +=
                performance_BackgroundWorker_RunWorkerCompleted;

            performanceTimer.Interval = 1000;
            performanceTimer.Tick += performanceTimer_Tick;
        }


        private void InitializeGetTasksRequest()
        {
            taskUpdater.DoWork += taskUpdater_DoWork;
            taskUpdater.RunWorkerCompleted +=
                taskUpdater_RunWorkerCompleted;

            taskUpdateTimer.Interval = 5000;
            taskUpdateTimer.Tick += taskUpdateTimer_Tick;
            taskUpdateTimer.Start();
        }


        private void taskUpdater_DoWork(object sender,
            DoWorkEventArgs e)
        {
            try {
                var from_to = (Tuple<DateTime, DateTime>)e.Argument;
                TaskInfo[] tasks;
                TaskInfo[] statuses;
                String error;

                bool res = _client.UpdateTasks(from_to.Item1, from_to.Item2,
                    onlyActiveTasksToolStripMenuItem.Checked, false,
                    out tasks, out statuses, out error);

                e.Result = new Tuple<bool, TaskInfo[], TaskInfo[], String>(
                    res, tasks, statuses, error);
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void taskUpdater_RunWorkerCompleted(
            object sender, RunWorkerCompletedEventArgs e)
        {
            var result = (Tuple<bool, TaskInfo[], TaskInfo[], String>)e.Result;

            if ( !result.Item1 ) {
                taskCalendar.ClearTasks();
                taskUpdateTimer.Stop();
                MessageBox.Show(result.Item4, "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else
                UpdateTasks(result.Item2, result.Item3);

        }


        private void taskUpdateTimer_Tick(object sender, EventArgs e)
        {
            //DateTime[] temp = new DateTime[2];
            //temp[0] = taskCalendar.FromDateTime;
            //temp[1] = taskCalendar.ToDateTime;

            var dates = new Tuple<DateTime, DateTime>(
                taskCalendar.FromDateTime,
                taskCalendar.ToDateTime
            );

            if ( !taskUpdater.IsBusy )
                taskUpdater.RunWorkerAsync(dates);
        }



        private void performance_BackgroundWorker_DoWork(object sender,
            DoWorkEventArgs e)
        {
            try {
                var taskID = (int)e.Argument;
                Tuple<int, float>[] cpuPerf;
                String error;
                bool res = _client.GetCpuPerformance(taskID, out cpuPerf, out error);
                e.Result = new Tuple<bool, String, Tuple<int, float>[]>(res, error, cpuPerf);
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }



        private void performance_BackgroundWorker_RunWorkerCompleted(
            object sender, RunWorkerCompletedEventArgs e)
        {
            var result = (Tuple<bool, String, Tuple<int, float>[]>)e.Result;

            SetPerformance(result.Item1, result.Item2, result.Item3);

            //if ( !result.Item1 ) {
            //    performanceTimer.Stop();
            //    cpuPerformanceTextBox.Text = result.Item2;
            //} else {
            //    if ( _selectedTask.PerfomanceList == null )
            //        _selectedTask.PerfomanceList = new List<float>();

            //    _selectedTask.PerfomanceList.Add(result.Item2);
            //    cpuPerformanceTextBox.Text = result.Item2.ToString();

            //    if ( _perfForm != null && _perfForm.Visible )
            //        _perfForm.AddPoint(result.Item2);
            //}
        }


        private void performanceTimer_Tick(object sender, EventArgs e)
        {
            if ( _selectedTask == null ) {
                performanceTimer.Stop();
                return;
            }

            if ( !performance_BackgroundWorker.IsBusy )
                performance_BackgroundWorker.RunWorkerAsync(_selectedTask.id);
        }


        private void serverTimeSynchronizer_DoWork(Object sender, DoWorkEventArgs e)
        {
            try {
                DateTimeOffset serverTime;
                String error;
                bool res = _client.GetServerTime(out serverTime, out error);
                e.Result = new Tuple<bool, String, DateTimeOffset>(res, error, serverTime);
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void serverTimeUpdater_RunWorkerCompleted(Object sender,
            RunWorkerCompletedEventArgs e)
        {
            var result = (Tuple<bool, String, DateTimeOffset>)e.Result;

            if ( result.Item1 ) {
                serverTimeTimer.Stop();
                _syncServerTimeEvent.WaitOne();
                _serverTime = result.Item3;
                serverTimeTimer.Start();
            } else {
                //serverTimeSyncTimer.Stop();
                MessageBox.Show("ServerUpdater failed");
            }
        }


        private void serverTimeSyncTimer_Tick(Object sender, EventArgs e)
        {
            if ( !serverTimeSynchronizer.IsBusy )
                serverTimeSynchronizer.RunWorkerAsync();
        }


        private void serverTimeTimer_Tick(Object sender, EventArgs e)
        {
            _syncServerTimeEvent.Reset();
            _serverTime = _serverTime.AddSeconds(1);
            serverTimeLabel.Text = _serverTime.TimeOfDay.
                ToString(@"hh\:mm\:ss");
            _syncServerTimeEvent.Set();
        }
        // --------------------------------------------------------------







        // --------------- Переключатели панелей ------------------------
        private void ToggleScheduleContentPanel()
        {
            if ( scheduleContentPanel.Visible ) {
                scheduleContentPanel.Visible = false;
                scheduleToolStripButton.Checked = false;
                ResetSelectedSchedule();
            } else {
                _currentBottomPanel = scheduleContentPanel;
                scheduleContentPanel.Visible = true;
                scheduleToolStripButton.Checked = true;
            }
        }


        private void ToggleGeneralContentPanel()
        {
            if ( generalContentPanel.Visible ) {
                generalContentPanel.Visible = false;
                generalToolStripButton.Checked = false;
            } else {
                _currentBottomPanel = generalContentPanel;
                generalContentPanel.Visible = true;
                generalToolStripButton.Checked = true;
            }
        }


        private void ToggleLogContentBottomPanelPanel()
        {
            if ( logContentBottomPanel.Visible ) {
                logContentBottomPanel.Visible = false;
                logToolStripButton.Checked = false;
            } else {
                SetTaskLog();
                _currentBottomPanel = logContentBottomPanel;
                logContentBottomPanel.Visible = true;
                logToolStripButton.Checked = true;
            }
        }


        private void TogglePerformanceContentPanel()
        {
            if ( performanceContentPanel.Visible ) {
                performanceContentPanel.Visible = false;
                performanceToolStripButton.Checked = false;
            } else {
                _currentBottomPanel = performanceContentPanel;
                SetTaskLogExecution();
                performanceContentPanel.Visible = true;
                performanceToolStripButton.Checked = true;
            }
        }


        private void ToggleCurrentPanel()
        {
            if ( _currentBottomPanel.Name == performanceContentPanel.Name )
                TogglePerformanceContentPanel();
            else if ( _currentBottomPanel.Name == generalContentPanel.Name )
                ToggleGeneralContentPanel();
            else if ( _currentBottomPanel.Name == scheduleContentPanel.Name )
                ToggleScheduleContentPanel();
            else if ( _currentBottomPanel.Name == logContentBottomPanel.Name )
                ToggleLogContentBottomPanelPanel();
            else
                ;
        }


        private void generalToolStripButton_Click(object sender, EventArgs e)
        {
            ToggleCurrentPanel();
            ToggleGeneralContentPanel();
        }


        private void performanceToolStripButton_Click(object sender, EventArgs e)
        {
            ToggleCurrentPanel();
            TogglePerformanceContentPanel();
            //GetPerformance();
        }


        private void scheduleToolStripButton_Click(object sender, EventArgs e)
        {
            UpdateSchedules(_selectedTask, _selectedSchedule);
            ToggleCurrentPanel();
            ToggleScheduleContentPanel();
        }


        private void logToolStripButton_Click(object sender, EventArgs e)
        {
            ToggleCurrentPanel();
            ToggleLogContentBottomPanelPanel();
        }


        private void taskComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if ( taskComboBox.SelectedIndex == 0 ) {
            //    dayTaskListView.Visible = true;
            //    monthTaskListView.Visible = false;
            //} else {
            //    dayTaskListView.Visible = false;
            //    monthTaskListView.Visible = true;
            //}

            dayTaskListView.Visible = false;
            monthTaskListView.Visible = true;

            if ( taskCalendar.SelectedCell == null )
                return;

            SetSelectedView(taskCalendar.SelectedDate,
                taskCalendar.SelectedCell.GetTasks(),
                taskCalendar.SelectedCell.GetStatuses());
        }


        private void ToggleTaskActionsContentPanel()
        {
            if ( taskActionsContentPanel.Visible )
                taskActionsContentPanel.Visible = false;
            else {
                _currentActionsPanel = taskActionsContentPanel;
                if ( _selectedTask.Active )
                    toggleActivityActionLabel.Text = "Деактивировать задачу";
                else
                    toggleActivityActionLabel.Text = "Активировать задачу";
                taskActionsContentPanel.Visible = true;
            }
        }


        private void ToggleGeneralActionsContentPanel()
        {
            if ( generalActionsContentPanel.Visible )
                generalActionsContentPanel.Visible = false;
            else {
                _currentActionsPanel = generalActionsContentPanel;
                generalActionsContentPanel.Visible = true;
            }
        }


        private void ToggleCurrentActionsPanel()
        {
            if ( _currentActionsPanel.Name == taskActionsContentPanel.Name )
                ToggleTaskActionsContentPanel();
            else if ( _currentActionsPanel.Name == generalActionsContentPanel.Name )
                ToggleGeneralActionsContentPanel();
            else if ( _currentActionsPanel.Name == scheduleActionsContentPanel.Name )
                ToggleScheduleActionsContentPanel();
            else
                ;
        }


        private void ToggleScheduleActionsContentPanel()
        {
            if ( scheduleActionsContentPanel.Visible )
                scheduleActionsContentPanel.Visible = false;
            else {
                _currentActionsPanel = scheduleActionsContentPanel;
                if ( _selectedSchedule.Active )
                    toggleScheduleActivityActionLabel.Text = "Деактивировать расписание";
                else
                    toggleScheduleActivityActionLabel.Text = "Активировать расписание";
                scheduleActionsContentPanel.Visible = true;
            }
        }
        // --------------------------------------------------------------










        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }









        // --------Обновление задач всего связанного с ними-------------
        private void UpdateSelectedContent(TaskInfo task)
        {
            if ( _currentBottomPanel.Name == generalContentPanel.Name ) {
                UpdateGeneralContent(task);
            } else if ( _currentBottomPanel.Name == scheduleContentPanel.Name ) {
                UpdateSchedules(task, _selectedSchedule);
            } else if ( _currentBottomPanel.Name == performanceContentPanel.Name ) {
                SetTaskLogExecution();
            } else {
                SetTaskLog();
            }
        }



        private void UpdateSelectedView(DateTime date, TaskInfo[] tasks,
            TaskInfo[] statuses, TaskInfo prevTask)
        {
            if ( monthTaskListView.Visible ) {
                UpdateMonthTasks(date, tasks, prevTask);
            } else if ( dayTaskListView.Visible ) {
                UpdateDaySchedules(date, tasks, statuses, prevTask);
            } else {

            }
        }



        private void SetSelectedView(DateTime date, TaskInfo[] tasks, TaskInfo[] statuses)
        {
            if ( monthTaskListView.Visible ) {
                SetMonthsTasks(date, tasks);
            } else if ( dayTaskListView.Visible ) {
                SetDaySchedules(date, tasks, statuses);
            } else {

            }
        }




        private void UpdateDaySchedules(DateTime date, TaskInfo[] tasks,
            TaskInfo[] statuses, TaskInfo prevTask)
        {
            lock ( _lockObj ) {
                dayTaskListView.Items.Clear();

                foreach ( var task in tasks )
                    foreach ( var sched in task.Schedules )
                        foreach ( var d in sched.Dates ) {
                            DateTime one = new DateTime(d.Year, d.Month, d.Day);
                            DateTime two =
                                new DateTime(date.Year, date.Month, date.Day);
                            if ( one != two )
                                continue;

                            ListViewItem item = new ListViewItem(task.Name);
                            item.Name = task.id.ToString();
                            item.SubItems.Add(sched.Name);
                            item.SubItems.Add(task.BusinessTaskName);
                            var temp = statuses.FirstOrDefault(
                                (x) => x.id == task.id &&
                                    ScheduleInfo.CompareDate(x.Dates[0], date) == 0
                            );
                            if ( temp == null )
                                item.SubItems.Add(TaskStatus.Waiting.ToString());
                            else
                                item.SubItems.Add(temp.Status.ToString());
                            item.SubItems.Add(d.ToString());
                            this.dayTaskListView.Items.Add(item);
                        }

                if ( prevTask != null ) {
                    var items = dayTaskListView.Items;
                    foreach ( ListViewItem item in items ) {
                        if ( item.Name == prevTask.id.ToString() ) {
                            item.Selected = true;
                            return;
                        }
                    }
                    ResetSelectedTask();
                    MessageBox.Show("Выбранная задача была удалена другим пользователем!",
                        "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }



        private void SetDaySchedules(DateTime date, TaskInfo[] tasks, TaskInfo[] statuses)
        {
            lock ( _lockObj ) {
                dayTaskListView.Items.Clear();
                ResetSelectedTask();

                foreach ( var task in tasks )
                    foreach ( var sched in task.Schedules )
                        foreach ( var d in sched.Dates ) {
                            DateTime one = new DateTime(d.Year, d.Month, d.Day);
                            DateTime two =
                                new DateTime(date.Year, date.Month, date.Day);

                            if ( one != two )
                                continue;

                            ListViewItem item = new ListViewItem(task.Name);
                            item.Name = task.id.ToString();
                            item.SubItems.Add(sched.Name);
                            item.SubItems.Add(task.BusinessTaskName);
                            var temp = statuses.FirstOrDefault(
                                (x) => x.id == task.id &&
                                    ScheduleInfo.CompareDate(x.Dates[0], date) == 0
                            );
                            if ( temp == null )
                                item.SubItems.Add(TaskStatus.Waiting.ToString());
                            else
                                item.SubItems.Add(temp.Status.ToString());
                            item.SubItems.Add(d.ToString());
                            this.dayTaskListView.Items.Add(item);
                        }
            }
        }




        private void UpdateMonthTasks(DateTime date, TaskInfo[] tasks,
            TaskInfo prevTask)
        {
            lock ( _lockObj ) {
                monthTaskListView.Items.Clear();

                foreach ( var task in tasks ) {
                    ListViewItem item = new ListViewItem(task.Name);
                    item.Name = task.id.ToString();
                    item.SubItems.Add(task.BusinessTaskName);
                    this.monthTaskListView.Items.Add(item);
                }

                if ( prevTask != null ) {
                    var items = monthTaskListView.Items;
                    foreach ( ListViewItem item in items ) {
                        if ( item.Name == prevTask.id.ToString() ) {
                            item.Selected = true;
                            return;
                        }
                    }
                    ResetSelectedTask();
                    MessageBox.Show("Выбранная задача была удалена другим пользователем!",
                        "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }



        private void SetMonthsTasks(DateTime date, TaskInfo[] tasks)
        {
            lock ( _lockObj ) {
                monthTaskListView.Items.Clear();
                ResetSelectedTask();

                foreach ( var task in tasks ) {
                    ListViewItem item = new ListViewItem(task.Name);
                    item.Name = task.id.ToString();
                    item.SubItems.Add(task.BusinessTaskName);
                    this.monthTaskListView.Items.Add(item);
                }
            }
        }



        private void UpdateTasks(DateTime fromDt, DateTime toDt)
        {
            lock ( _lockObj ) {
                try {
                    String error;
                    TaskInfo[] statuses;
                    TaskInfo[] taskList;
                    var res = _client.UpdateTasks(fromDt, toDt,
                        onlyActiveTasksToolStripMenuItem.Checked, false,
                        out taskList, out statuses, out error);

                    if ( !res ) {
                        MessageBox.Show(error, "Ошибка!",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if ( taskList == null )
                        return;

                    taskCalendar.ClearTasks();

                    CurrentTasks = taskList;
                    taskCalendar.SetTasks(taskList);
                    taskCalendar.SetStatuses(statuses);

                    if ( _selectedTask != null ) {
                        var result = CurrentTasks.FirstOrDefault((x) => {
                            if ( _selectedTask == null )
                                return false;
                            return x.id == _selectedTask.id;
                        });

                        SetSelectedTask(result);
                        if ( result == null ) {
                            MessageBox.Show("Выбранная задача была удалена другим пользователем!",
                                "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }

                    if ( taskCalendar.SelectedCell != null )
                        UpdateSelectedView(taskCalendar.SelectedDate,
                            taskCalendar.SelectedCell.GetTasks(), statuses, _selectedTask);

                    if ( _selectedSchedule != null ) {
                        SetSelectedSchedule(_selectedTask.Schedules.First(
                            (x) => x.id == _selectedSchedule.id
                        ));
                    }
                } catch ( Exception exception ) {
                    MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                        "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }

            }
        }


        private void UpdateTasks(TaskInfo[] tasks, TaskInfo[] statuses)
        {
            lock ( _lockObj ) {

                if ( tasks == null )
                    return;

                taskCalendar.ClearTasks();
                CurrentTasks = tasks;
                taskCalendar.SetTasks(tasks);
                taskCalendar.SetStatuses(statuses);


                if ( _selectedTask != null ) {
                    var result = CurrentTasks.FirstOrDefault((x) => {
                        if ( _selectedTask == null )
                            return false;
                        return x.id == _selectedTask.id;
                    });

                    SetSelectedTask(result);
                    if ( result == null ) {
                        MessageBox.Show("Выбранная задача была удалена другим пользователем!",
                            "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                if ( taskCalendar.SelectedCell != null )
                    UpdateSelectedView(taskCalendar.SelectedDate,
                        taskCalendar.SelectedCell.GetTasks(), statuses, _selectedTask);

                if ( _selectedSchedule != null ) {
                    SetSelectedSchedule(_selectedTask.Schedules.First(
                        (x) => x.id == _selectedSchedule.id
                    ));
                }
            }
        }



        private void GetTasks(DateTime fromDt, DateTime toDt)
        {
            lock ( _lockObj ) {
                try {
                    String error;
                    TaskInfo[] taskList;
                    TaskInfo[] statuses;
                    var res = _client.GetTasks(fromDt, toDt,
                        onlyActiveTasksToolStripMenuItem.Checked, false,
                        out taskList, out statuses, out error);

                    ResetSelectedTask();
                    if ( !res ) {
                        MessageBox.Show(error, "Ошибка!",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    } else {
                        taskCalendar.ClearTasks();
                        CurrentTasks = taskList;
                        taskCalendar.SetTasks(taskList);
                        taskCalendar.SetStatuses(statuses);
                    }
                } catch ( Exception exception ) {
                     MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                        "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
        }



        private float[] GetPerformance()
        {
            lock ( _lockObj ) {
                if ( _selectedExecInfo == null ) {
                    MessageBox.Show("Нужно выбрать запись из списка, чтобы узнать производительность.",
                        "Оповещение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return null;
                }

                var temp = _selectedExecInfo;
                if ( _selectedExecInfo.EndDateTime == DateTime.MinValue ) {
                    temp = new TaskLogExecutionInfo {
                        ScheduleID = _selectedExecInfo.ScheduleID,
                        StartDateTime = _selectedExecInfo.StartDateTime,
                        EndDateTime = DateTime.Now
                    };
                }

                try {
                    String error;
                    float[] perfLog;
                    bool res = _client.GetStoredCpuPerformance(temp, out perfLog, out error);
                    if ( !res )
                        MessageBox.Show(error, "Ошибка!" + error,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);

                    return perfLog;
                } catch ( Exception exception ) {
                    MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                        "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return null;
                }
            }
        }


        private void UpdateSchedules(TaskInfo task, ScheduleInfo prevSchedule)
        {
            if ( task == null )
                return;

            schedListView.Items.Clear();
            foreach ( var sched in task.Schedules ) {
                ListViewItem item = new ListViewItem(sched.Name);
                item.Name = sched.id.ToString();
                item.SubItems.Add(sched.Description);
                item.SubItems.Add(sched.TriggerName);
                item.SubItems.Add(sched.Active.ToString());
                item.SubItems.Add(sched.GetDetails());
                this.schedListView.Items.Add(item);
            }

            if ( prevSchedule != null ) {
                var items = schedListView.Items;
                foreach ( ListViewItem item in items ) {
                    if ( item.Name == prevSchedule.id.ToString() ) {
                        item.Selected = true;
                        return;
                    }
                }
                ResetSelectedSchedule();
                MessageBox.Show("Выбранное расписание было удалено другим пользователем!",
                    "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        private void RemoveSchedule()
        {
            try {
                String error;
                bool res = _client.DeleteSchedule(_selectedTask.id,
                    _selectedSchedule.id, out error);
                if ( !res )
                    MessageBox.Show(error, "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                else {
                    MessageBox.Show("Расписание успешно удалено", "Успех!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    UpdateTasks(taskCalendar.FromDateTime, taskCalendar.ToDateTime);
                }
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void DeleteTask()
        {
            //taskCalendar.ClearTasks();
            if ( _selectedTask == null ) {
                MessageBox.Show("Выберите задачу сначала", "Успех!",
                     MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try {
                String error;
                if ( !_client.RemoveTask(_selectedTask.id, out error) )
                    MessageBox.Show(error, "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                else {
                    ResetSelectedTask();
                    MessageBox.Show("Задача успешно удалена", "Успех!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    UpdateTasks(taskCalendar.FromDateTime, taskCalendar.ToDateTime);
                }
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void SetSelectedTask(TaskInfo task)
        {
            if ( task == null ) {
                ResetSelectedTask();
                return;
            }

            _selectedTask = task;

            UpdateSelectedContent(_selectedTask);
            bottomVerticalSplitContainer.Enabled = true;

            ToggleCurrentActionsPanel();
            ToggleTaskActionsContentPanel();

            //if ( _currentBottomPanel.Name == performanceContentPanel.Name )
            //    GetPerformance();
        }


        private void ResetSelectedTask()
        {
            if ( _selectedTask == null )
                return;

            _selectedTask = null;

            ToggleCurrentPanel();
            ToggleGeneralContentPanel();
            ClearGeneralContent();

            ToggleCurrentActionsPanel();
            ToggleGeneralActionsContentPanel();

            dayTaskListView.SelectedIndices.Clear();
            bottomVerticalSplitContainer.Panel1.Enabled = false;

            ResetSelectedExecInfo();
            ResetSelectedSchedule(); // аккуратно
        }


        private void UpdateGeneralContent(TaskInfo task)
        {
            if ( task == null ) {
                ClearGeneralContent();
                return;
            }

            taskDescriptionTextBox.Text = task.Description;
            taskNameTextBox.Text = task.Name;
            businessTaskNameTextBox.Text = task.BusinessTaskName;
            autorTextBox.Text = task.UserName;
        }


        private void ClearGeneralContent()
        {
            taskDescriptionTextBox.Clear();
            taskNameTextBox.Clear();
            businessTaskNameTextBox.Clear();
            autorTextBox.Clear();
        }


        private void AddTask()
        {
            AddTaskForm childForm = new AddTaskForm(_client, TaskTypes);
            childForm.Text = "Planner: Добавить задачу";
            childForm.ShowDialog();

            if ( childForm.DialogResult == DialogResult.Cancel )
                return;

            UpdateTasks(taskCalendar.FromDateTime, taskCalendar.ToDateTime);
        }


        private void ShowUserLog()
        {
            try {
                String error;
                UserLogInfo[] userLog;
                if ( !_client.GetUserLog(out userLog, out error) ) {
                    MessageBox.Show("Невозможно получить лог пользователя. Exception:\n" + error,
                        "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                LogViewForm childForm = new LogViewForm(_client, LogViewType.UserLog);
                childForm.Text = "Planner: Лог пользователя";
                childForm.AddEntries(userLog);
                childForm.Show();
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void ShowTaskLog()
        {
            try {
                String error;
                TaskLogInfo[] taskLog;
                if ( !_client.GetTaskLog(_selectedTask.id, out taskLog, out error) ) {
                    MessageBox.Show("Невозможно получить лог задачи. Exception:\n" + error,
                        "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                LogViewForm childForm = new LogViewForm(_client, _selectedTask.id);
                childForm.Text = "Planner: Лог задачи";
                childForm.AddEntries(taskLog);
                childForm.Show();
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void ShowServerLog()
        {
            try {
                String error;
                Tuple<DateTime, String>[] serverLog;
                if ( !_client.GetServerLog(out serverLog, out error) ) {
                    MessageBox.Show("Невозможно получить лог сервера. Exception:\n" + error,
                        "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                LogViewForm childForm = new LogViewForm(_client, LogViewType.ServerLog);
                childForm.Text = "Planner: Лог сервера";
                childForm.AddEntries(serverLog);
                childForm.Show();
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void AddSchedule()
        {
            NewScheduleForm childForm = new NewScheduleForm();
            childForm.Text = "Planner: Добавить расписание";
            childForm.ShowDialog();
            if ( childForm.DialogResult == DialogResult.Cancel )
                return;

            if ( _selectedTask == null ) {
                MessageBox.Show("Не выбрана задача!", "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ResetSelectedTask();
                return;
            }

            try {
                String error;
                bool res = _client.AddSchedule(_selectedTask.id,
                    childForm.ScheduleInfo, out error);
                if ( !res ) {
                    MessageBox.Show("Возникла ошибка при добавлении новго расписание к задаче." +
                        " Подробнее:\n!" + error, "Ошибка!", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }

            UpdateTasks(taskCalendar.FromDateTime, taskCalendar.ToDateTime);
        }


        private void ActivateTask(TaskInfo taskInfo)
        {
            if ( taskInfo == null ) {
                ResetSelectedTask();
                MessageBox.Show("Не выбрана задача!", "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try {
                String error;
                bool res = _client.ActivateTask(taskInfo.id, out error);
                if ( !res ) {
                    MessageBox.Show("Ошибка активации задания. Подробнее:\n" + error,
                        "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }

            //ResetSelectedTask();
            UpdateTasks(taskCalendar.FromDateTime,
                taskCalendar.ToDateTime);

            MessageBox.Show("Задача успешно активирована", "Успех!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        private void DeactivateTask(TaskInfo taskInfo)
        {
            if ( taskInfo == null ) {
                ResetSelectedTask();
                MessageBox.Show("Не выбрана задача!", "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try {
                String error;
                bool res = _client.DeactivateTask(taskInfo.id, out error);
                if ( !res ) {
                    MessageBox.Show("Ошибка деактивации задания. Подробнее:\n" + error,
                        "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }

            ResetSelectedTask();
            UpdateTasks(taskCalendar.FromDateTime,
                taskCalendar.ToDateTime);

            MessageBox.Show("Задача успешно деактивирована", "Успех!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        private void SetSelectedSchedule(ScheduleInfo schedule)
        {
            if ( schedule == null ) {
                ResetSelectedSchedule();
                return;
            }

            _selectedSchedule = schedule;

            ToggleCurrentActionsPanel();
            ToggleScheduleActionsContentPanel();
        }



        private void ResetSelectedSchedule()
        {
            if ( _selectedSchedule == null )
                return;

            _selectedSchedule = null;

            foreach ( ListViewItem item in schedListView.SelectedItems )
                item.Selected = false;

            SetSelectedTask(_selectedTask);
        }


        private void DeleteSchedule()
        {
            if ( _selectedSchedule == null ) {
                ResetSelectedSchedule();
                MessageBox.Show("Выберите расписание сначала", "Предупреждение!",
                     MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if ( _selectedTask.Schedules.Length <= 1 ) {
                MessageBox.Show("Нельзя деактивировать или удалить последнее расписание у задачи!",
                    "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try {
                String error;
                if ( !_client.DeleteSchedule(_selectedTask.id, _selectedSchedule.id, out error) )
                    MessageBox.Show("Возникла ошибка при удалении выбранного расписания. Подробнее: "
                        + error, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else {
                    ResetSelectedSchedule();
                    MessageBox.Show("Расписание успешно удалено!", "Успех!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    UpdateTasks(taskCalendar.FromDateTime, taskCalendar.ToDateTime);
                }
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void DeactivateSchedule()
        {
            if ( _selectedSchedule == null ) {
                ResetSelectedSchedule();
                MessageBox.Show("Выберите расписание сначала", "Предупреждение!",
                     MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if ( _selectedTask.Schedules.Length <= 1 ) {
                MessageBox.Show("Нельзя деактивировать или удалить последнее расписание у задачи",
                    "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try {
                String error;
                if ( !_client.DeactivateSchedule(_selectedTask.id, _selectedSchedule.id, out error) )
                    MessageBox.Show("Возникла ошибка при деактивации выбранного расписания. Подробнее: "
                        + error, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else {
                    ResetSelectedSchedule();
                    MessageBox.Show("Расписание успешно деактивировано!", "Успех!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    UpdateTasks(taskCalendar.FromDateTime, taskCalendar.ToDateTime);
                }
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void ActivateSchedule()
        {
            if ( _selectedSchedule == null ) {
                ResetSelectedSchedule();
                MessageBox.Show("Выберите расписание сначала", "Предупреждение!",
                     MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try {
                String error;
                if ( !_client.ActivateSchedule(_selectedTask.id, _selectedSchedule.id, out error) )
                    MessageBox.Show("Возникла ошибка при активации выбранного расписания. Подробнее: "
                        + error, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else {
                    ResetSelectedSchedule();
                    MessageBox.Show("Расписание успешно активировано!", "Успех!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    UpdateTasks(taskCalendar.FromDateTime, taskCalendar.ToDateTime);
                }
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void SetTaskLog()
        {
            taskLogListView.Items.Clear();

            try {
                String error;
                TaskLogInfo[] taskLog;
                if ( !_client.GetTaskLog(_selectedTask.id, out taskLog, out error) ) {
                    MessageBox.Show("Невозможно получить лог задачи. Exception:\n" + error,
                        "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


                foreach ( var entry in taskLog ) {
                    ListViewItem item = new ListViewItem(entry.TaskName);
                    item.SubItems.Add(entry.DateTime.ToString());
                    item.SubItems.Add(entry.TypeName);
                    item.SubItems.Add(entry.Description);
                    item.SubItems.Add(entry.ExitCode);
                    this.taskLogListView.Items.Add(item);
                }
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void SetTaskLogExecution()
        {
            lock ( _lockObj ) {
                performanceStatusTextBox.Clear();
                performanceListView.Items.Clear();

                String error;
                TaskLogExecutionInfo[] taskLogExec;

                try {
                    if ( !_client.GetTaskExecutionLog(_selectedTask.id,
                        taskCalendar.SelectedDate.Date, out taskLogExec, out error) ) {
                        MessageBox.Show("Невозможно получить лог задачи. Exception:\n" + error,
                            "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        performanceStatusTextBox.Text = "Ошибка!";
                        return;
                    }


                    if ( taskLogExec.Length == 0 ) {
                        performanceStatusTextBox.Text = "Сегодня еще не было ни одного запуска, " +
                            "а возможно и не будет. Смотрите расписание";
                        return;
                    }


                    _currentExecInfos = taskLogExec;
                    foreach ( var entry in taskLogExec ) {
                        if ( entry.EndDateTime == DateTime.MinValue )
                            performanceTimer.Start();

                        ListViewItem item = new ListViewItem(entry.ScheduleName);
                        item.Name = entry.ScheduleID.ToString();
                        item.SubItems.Add(entry.StartDateTime.ToString());

                        if ( entry.EndDateTime == DateTime.MinValue ) {
                            item.SubItems.Add("Выполняется");
                            item.SubItems.Add("");
                        } else {
                            item.SubItems.Add(entry.EndDateTime.ToString());
                            if ( entry.ExitCode != 0 )
                                item.SubItems.Add("Запуск был выполнен, но выполнение завершилось крахом. Код - " + entry.ExitCode);
                            else
                                item.SubItems.Add("Запуск был выполнен успешно");
                        }


                        this.performanceListView.Items.Add(item);
                    }
                } catch ( Exception exception ) {
                    MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                        "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }

                //MessageBox.Show(performanceListView.Items[0].SubItems[2].Text);
            }
        }



        private void SetPerformance(bool result, String error, Tuple<int, float>[] perfArr)
        {
            lock ( _lockObj ) {
                if ( result == false ) {
                    MessageBox.Show("Some error" + error);
                    performanceTimer.Stop();
                    return;
                }

                if ( performanceListView.Items.Count <= 0 )
                    return;

                foreach ( var item in perfArr ) {
                    if ( _perfForm != null && _perfForm.Visible )
                        _perfForm.AddPoint(item.Item2);
                    var temp = performanceListView.Items.
                        Find(item.Item1.ToString(), false)[0].SubItems[3];
                    temp.Text = "Производительность ЦП: " + item.Item2.ToString();
                }
            }

        }


        private void SetSelectedExecInfo(TaskLogExecutionInfo info)
        {
            if ( info == null ) {
                return;
            }

            _selectedExecInfo = info;

            performanceStatusTextBox.Text = info.ScheduleName;
        }


        private void ResetSelectedExecInfo()
        {
            if ( _selectedExecInfo == null ) {
                return;
            }

            _selectedExecInfo = null;

            performanceStatusTextBox.Clear();
        }
        // -------------------------------------------------------------










        // -----------Реакция на действия пользователя------------------
        private void cpuPerfButton_Click(object sender, EventArgs e)
        {
            var res = GetPerformance();
            if ( res == null ) {
                return;
            }

            _perfForm = new PerformanceForm();

            _perfForm.Text = "Planner: Использорвание ЦП";
            foreach ( var val in res )
                _perfForm.AddPoint(val);
            _perfForm.Show();
        }


        private void removeTaskLabel_Click(object sender, EventArgs e)
        {
            DeleteTask();
        }


        private void schedListView_ItemSelectionChanged(object sender,
            ListViewItemSelectionChangedEventArgs e)
        {
            if ( !e.IsSelected )
                return;

            var scheds = _selectedTask.Schedules;
            var selected = scheds.FirstOrDefault((x) => x.id.ToString() == e.Item.Name);
            if ( selected == null ) {
                ResetSelectedSchedule();
                return;
            }

            SetSelectedSchedule(selected);
        }


        private void deleteSchedButton_Click(object sender, EventArgs e)
        {
            RemoveSchedule();
        }


        private void refreshToolStripButton_Click(object sender, EventArgs e)
        {
            UpdateTasks(taskCalendar.FromDateTime, taskCalendar.ToDateTime);
        }


        private void taskListView_ItemSelectionChanged(object sender,
            ListViewItemSelectionChangedEventArgs e)
        {
            if ( !e.IsSelected )
                return;

            this.bottomVerticalSplitContainer.Enabled = true;
            bottomVerticalSplitContainer.Panel1.Enabled = true;

            var tasks = taskCalendar.SelectedCell.GetTasks();
            var selected = tasks.FirstOrDefault((x) => x.id.ToString() == e.Item.Name);
            if ( selected == null ) {
                ResetSelectedTask();
                return;
            }

            SetSelectedTask(selected);
        }



        private void monthTaskListView_ItemSelectionChanged(object sender,
            ListViewItemSelectionChangedEventArgs e)
        {
            if ( !e.IsSelected )
                return;

            this.bottomVerticalSplitContainer.Enabled = true;
            bottomVerticalSplitContainer.Panel1.Enabled = true;

            var tasks = taskCalendar.SelectedCell.GetTasks();
            var selected = tasks.FirstOrDefault((x) => x.id.ToString() == e.Item.Name);
            if ( selected == null ) {
                ResetSelectedTask();
                return;
            }

            SetSelectedTask(selected);
        }



        private void performanceListView_ItemSelectionChanged(object sender,
            ListViewItemSelectionChangedEventArgs e)
        {
            if ( !e.IsSelected )
                return;

            var temp = _currentExecInfos;
            var selected = temp.FirstOrDefault((x) => x.ScheduleID.ToString() == e.Item.Name);
            if ( selected == null ) {
                //ResetSelectedTask();
                return;
            }

            SetSelectedExecInfo(selected);
        }


        private void Main_Load(object sender, EventArgs e)
        {
            try {
                String error;
                String[] types;
                if ( !_client.GetTaskTypes(out types, out error) )
                    MessageBox.Show(error, "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                String version;
                if ( !_client.GetTaskVersion(out version, out error) )
                    MessageBox.Show(error, "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);


                TaskTypes = types;
                userNameLabel.Text = _client.UserName;
                dllLabel.Text = version;

                this.taskComboBox.SelectedIndex = 0;
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void taskCalendar_DateSpanChanged(object sender,
            TaskCalendar.DateSpanChangedEventArgs e)
        {
            GetTasks(e.FromDateTime, e.ToDateTime);
        }


        private void addTaskToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            AddTask();
        }



        private void taskCalendar_DaySelected(object sender,
            TaskCalendar.DaySelectedEventArgs e)
        {
            SetSelectedView(e.Date, e.Tasks, e.Statuses);
        }



        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            String error;
            try {
                _client.LogOut(out error);
                _client.Close();
            } catch ( Exception exc ) {
                return;
            }
        }


        private void addTaskActionLabel_Click(object sender, EventArgs e)
        {
            AddTask();
        }


        private void userLogActionLabel_Click(object sender, EventArgs e)
        {
            ShowUserLog();
        }


        private void serverLogActionLabel_Click(object sender, EventArgs e)
        {
            ShowServerLog();
        }


        private void taskLogActionLabel_Click(object sender, EventArgs e)
        {
            ShowTaskLog();
        }


        private void addScheduleActionLabel_Click(object sender, EventArgs e)
        {
            AddSchedule();
        }



        private void toggleActivityActionLabel_Click(object sender, EventArgs e)
        {
            if ( _selectedTask.Active )
                DeactivateTask(_selectedTask);
            else
                ActivateTask(_selectedTask);
        }


        private void deleteScheduleActionLabel_Click(object sender, EventArgs e)
        {
            DeleteSchedule();
        }


        private void schedListView_Leave(object sender, EventArgs e)
        {
            ResetSelectedSchedule();
        }


        private void toggleScheduleActivityActionLabel_Click(object sender, EventArgs e)
        {
            if ( _selectedSchedule.Active )
                DeactivateSchedule();
            else
                ActivateSchedule();
        }


        private void onlyActiveTasksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ( onlyActiveTasksToolStripMenuItem.Checked )
                onlyActiveTasksToolStripMenuItem.Checked = false;
            else
                onlyActiveTasksToolStripMenuItem.Checked = true;

            GetTasks(taskCalendar.FromDateTime, taskCalendar.ToDateTime);
            UpdateSelectedView(taskCalendar.SelectedDate, taskCalendar.SelectedCell.GetTasks(),
                taskCalendar.SelectedCell.GetStatuses(), _selectedTask);
        }
        // -------------------------------------------------------------
    }
}