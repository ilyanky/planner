﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Planner
{
    public partial class MonthPicker : UserControl
    {
        private const int MONTHS_COUNT = 12;

        public String Months { get; private set; }



        public MonthPicker()
        {
            InitializeComponent();

            HidePanel();
        }



        private void ShowPanel()
        {
            panel.Visible = true;
            this.Height += panel.Height;
        }


        private void HidePanel()
        {
            panel.Visible = false;
            this.Height = textBox.Height;
        }



        private void SetText()
        {
            var text = new StringBuilder();
            for ( int i = 1; i <= MONTHS_COUNT; ++i ) {
                String name = "checkBox" + i;
                var checkBox = (CheckBox)this.panel.Controls.Find(name, true)[0];
                if ( checkBox.Checked )
                    text.Append(", " + i);
            }

            if ( text.Length != 0 )
                text = text.Remove(0, 2);

            textBox.Text = text.ToString();
            Months = text.ToString();
        }




        private void MonthPicker_Leave(object sender, EventArgs e)
        {
            SetText();
            HidePanel();
        }

        private void monthAddButton_Click(object sender, EventArgs e)
        {
            SetText();

            if ( panel.Visible )
                HidePanel();
            else
                ShowPanel();
        }


        private void checkBox13_CheckedChanged(object sender, EventArgs e)
        {
            bool toggle;
            if ( checkBox13.Checked )
                toggle = true;
            else
                toggle = false;

            for ( int i = 1; i <= MONTHS_COUNT; ++i ) {
                String name = "checkBox" + i;
                var checkBox = (CheckBox)this.panel.Controls.Find(name, true)[0];
                checkBox.Checked = toggle;
            }
        }
    }
}
