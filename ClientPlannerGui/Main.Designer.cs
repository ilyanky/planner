﻿namespace Planner
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.onlyActiveTasksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.addTaskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.refreshToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.topVerticalSplitContainer = new System.Windows.Forms.SplitContainer();
            this.taskCalendar = new Planner.TaskCalendar();
            this.rightHorizontalSplitContainer = new System.Windows.Forms.SplitContainer();
            this.infoPanel = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dllLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.serverTimeLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.userNameLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.taskComboBox = new System.Windows.Forms.ComboBox();
            this.contentTasksPanel = new System.Windows.Forms.Panel();
            this.monthTaskListView = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dayTaskListView = new System.Windows.Forms.ListView();
            this.nameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.taskHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.statusHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timeHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bottomVerticalSplitContainer = new System.Windows.Forms.SplitContainer();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.generalToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.scheduleToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.performanceToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.logToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.bottomContentPanel = new System.Windows.Forms.Panel();
            this.performanceContentPanel = new System.Windows.Forms.Panel();
            this.performanceListView = new System.Windows.Forms.ListView();
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cpuPerfButton = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.performanceStatusTextBox = new System.Windows.Forms.TextBox();
            this.logContentBottomPanel = new System.Windows.Forms.Panel();
            this.taskLogListView = new System.Windows.Forms.ListView();
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.scheduleContentPanel = new System.Windows.Forms.Panel();
            this.schedListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.generalContentPanel = new System.Windows.Forms.Panel();
            this.autorTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.businessTaskNameTextBox = new System.Windows.Forms.TextBox();
            this.taskDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.taskNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.actionsPanel = new System.Windows.Forms.Panel();
            this.actionsContentPanel = new System.Windows.Forms.Panel();
            this.scheduleActionsContentPanel = new System.Windows.Forms.Panel();
            this.scheduleInfoActionLabel = new System.Windows.Forms.Label();
            this.toggleScheduleActivityActionLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.deleteScheduleActionLabel = new System.Windows.Forms.Label();
            this.taskActionsContentPanel = new System.Windows.Forms.Panel();
            this.infoActionLabel = new System.Windows.Forms.Label();
            this.taskLogActionLabel = new System.Windows.Forms.Label();
            this.toggleActivityActionLabel = new System.Windows.Forms.Label();
            this.addScheduleActionLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.deleteTaskActionLabel = new System.Windows.Forms.Label();
            this.generalActionsContentPanel = new System.Windows.Forms.Panel();
            this.serverLogActionLabel = new System.Windows.Forms.Label();
            this.userLogActionLabel = new System.Windows.Forms.Label();
            this.generalTasksSeparator = new System.Windows.Forms.Panel();
            this.addTaskActionLabel = new System.Windows.Forms.Label();
            this.actionsLabel = new System.Windows.Forms.Label();
            this.serverTimeSynchronizer = new System.ComponentModel.BackgroundWorker();
            this.serverTimeSyncTimer = new System.Windows.Forms.Timer(this.components);
            this.serverTimeTimer = new System.Windows.Forms.Timer(this.components);
            this.performance_BackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.performanceTimer = new System.Windows.Forms.Timer(this.components);
            this.taskUpdater = new System.ComponentModel.BackgroundWorker();
            this.taskUpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.topVerticalSplitContainer)).BeginInit();
            this.topVerticalSplitContainer.Panel1.SuspendLayout();
            this.topVerticalSplitContainer.Panel2.SuspendLayout();
            this.topVerticalSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rightHorizontalSplitContainer)).BeginInit();
            this.rightHorizontalSplitContainer.Panel1.SuspendLayout();
            this.rightHorizontalSplitContainer.Panel2.SuspendLayout();
            this.rightHorizontalSplitContainer.SuspendLayout();
            this.infoPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.contentTasksPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bottomVerticalSplitContainer)).BeginInit();
            this.bottomVerticalSplitContainer.Panel1.SuspendLayout();
            this.bottomVerticalSplitContainer.Panel2.SuspendLayout();
            this.bottomVerticalSplitContainer.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.bottomContentPanel.SuspendLayout();
            this.performanceContentPanel.SuspendLayout();
            this.logContentBottomPanel.SuspendLayout();
            this.scheduleContentPanel.SuspendLayout();
            this.generalContentPanel.SuspendLayout();
            this.actionsPanel.SuspendLayout();
            this.actionsContentPanel.SuspendLayout();
            this.scheduleActionsContentPanel.SuspendLayout();
            this.taskActionsContentPanel.SuspendLayout();
            this.generalActionsContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.viewMenu,
            this.toolsMenu,
            this.addTaskToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1810, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(48, 20);
            this.fileMenu.Text = "&Файл";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "В&ыход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.onlyActiveTasksToolStripMenuItem});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(39, 20);
            this.viewMenu.Text = "&Вид";
            // 
            // onlyActiveTasksToolStripMenuItem
            // 
            this.onlyActiveTasksToolStripMenuItem.Checked = true;
            this.onlyActiveTasksToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.onlyActiveTasksToolStripMenuItem.Name = "onlyActiveTasksToolStripMenuItem";
            this.onlyActiveTasksToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.onlyActiveTasksToolStripMenuItem.Text = "Только активные задачи";
            this.onlyActiveTasksToolStripMenuItem.Click += new System.EventHandler(this.onlyActiveTasksToolStripMenuItem_Click);
            // 
            // toolsMenu
            // 
            this.toolsMenu.Name = "toolsMenu";
            this.toolsMenu.Size = new System.Drawing.Size(48, 20);
            this.toolsMenu.Text = "&Tools";
            // 
            // addTaskToolStripMenuItem
            // 
            this.addTaskToolStripMenuItem.Name = "addTaskToolStripMenuItem";
            this.addTaskToolStripMenuItem.Size = new System.Drawing.Size(110, 20);
            this.addTaskToolStripMenuItem.Text = "Добавить задачу";
            this.addTaskToolStripMenuItem.Click += new System.EventHandler(this.addTaskToolStripMenuItem_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshToolStripButton,
            this.toolStripButton1});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(1810, 27);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "ToolStrip";
            // 
            // refreshToolStripButton
            // 
            this.refreshToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.refreshToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("refreshToolStripButton.Image")));
            this.refreshToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshToolStripButton.Name = "refreshToolStripButton";
            this.refreshToolStripButton.Size = new System.Drawing.Size(65, 24);
            this.refreshToolStripButton.Text = "Обновить";
            this.refreshToolStripButton.Click += new System.EventHandler(this.refreshToolStripButton_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::Planner.Properties.Resources.arrow_refresh_3_icon;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.ToolTipText = "Обновить";
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 723);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1810, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.BackColor = System.Drawing.Color.GhostWhite;
            this.mainSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 51);
            this.mainSplitContainer.Margin = new System.Windows.Forms.Padding(0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            this.mainSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.AutoScroll = true;
            this.mainSplitContainer.Panel1.Controls.Add(this.topVerticalSplitContainer);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.bottomVerticalSplitContainer);
            this.mainSplitContainer.Size = new System.Drawing.Size(1810, 672);
            this.mainSplitContainer.SplitterDistance = 489;
            this.mainSplitContainer.SplitterWidth = 3;
            this.mainSplitContainer.TabIndex = 7;
            // 
            // topVerticalSplitContainer
            // 
            this.topVerticalSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.topVerticalSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topVerticalSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.topVerticalSplitContainer.Margin = new System.Windows.Forms.Padding(0);
            this.topVerticalSplitContainer.Name = "topVerticalSplitContainer";
            // 
            // topVerticalSplitContainer.Panel1
            // 
            this.topVerticalSplitContainer.Panel1.AutoScroll = true;
            this.topVerticalSplitContainer.Panel1.AutoScrollMinSize = new System.Drawing.Size(1389, 517);
            this.topVerticalSplitContainer.Panel1.Controls.Add(this.taskCalendar);
            this.topVerticalSplitContainer.Panel1MinSize = 130;
            // 
            // topVerticalSplitContainer.Panel2
            // 
            this.topVerticalSplitContainer.Panel2.Controls.Add(this.rightHorizontalSplitContainer);
            this.topVerticalSplitContainer.Panel2MinSize = 130;
            this.topVerticalSplitContainer.Size = new System.Drawing.Size(1810, 489);
            this.topVerticalSplitContainer.SplitterDistance = 1408;
            this.topVerticalSplitContainer.SplitterWidth = 3;
            this.topVerticalSplitContainer.TabIndex = 0;
            // 
            // taskCalendar
            // 
            this.taskCalendar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taskCalendar.Location = new System.Drawing.Point(0, 0);
            this.taskCalendar.Margin = new System.Windows.Forms.Padding(0);
            this.taskCalendar.MinimumSize = new System.Drawing.Size(600, 420);
            this.taskCalendar.Name = "taskCalendar";
            this.taskCalendar.Size = new System.Drawing.Size(1389, 517);
            this.taskCalendar.TabIndex = 0;
            this.taskCalendar.DaySelected += new System.EventHandler<Planner.TaskCalendar.DaySelectedEventArgs>(this.taskCalendar_DaySelected);
            this.taskCalendar.DateSpanChanged += new System.EventHandler<Planner.TaskCalendar.DateSpanChangedEventArgs>(this.taskCalendar_DateSpanChanged);
            // 
            // rightHorizontalSplitContainer
            // 
            this.rightHorizontalSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rightHorizontalSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightHorizontalSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.rightHorizontalSplitContainer.Margin = new System.Windows.Forms.Padding(0);
            this.rightHorizontalSplitContainer.Name = "rightHorizontalSplitContainer";
            this.rightHorizontalSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // rightHorizontalSplitContainer.Panel1
            // 
            this.rightHorizontalSplitContainer.Panel1.Controls.Add(this.infoPanel);
            // 
            // rightHorizontalSplitContainer.Panel2
            // 
            this.rightHorizontalSplitContainer.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.rightHorizontalSplitContainer.Panel2.Controls.Add(this.taskComboBox);
            this.rightHorizontalSplitContainer.Panel2.Controls.Add(this.contentTasksPanel);
            this.rightHorizontalSplitContainer.Size = new System.Drawing.Size(399, 489);
            this.rightHorizontalSplitContainer.SplitterDistance = 184;
            this.rightHorizontalSplitContainer.SplitterWidth = 3;
            this.rightHorizontalSplitContainer.TabIndex = 0;
            // 
            // infoPanel
            // 
            this.infoPanel.AutoScroll = true;
            this.infoPanel.AutoScrollMinSize = new System.Drawing.Size(300, 100);
            this.infoPanel.BackColor = System.Drawing.Color.White;
            this.infoPanel.Controls.Add(this.groupBox1);
            this.infoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoPanel.Location = new System.Drawing.Point(0, 0);
            this.infoPanel.Margin = new System.Windows.Forms.Padding(0);
            this.infoPanel.Name = "infoPanel";
            this.infoPanel.Size = new System.Drawing.Size(397, 182);
            this.infoPanel.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dllLabel);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.serverTimeLabel);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.userNameLabel);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(397, 182);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Общая информация";
            // 
            // dllLabel
            // 
            this.dllLabel.AutoSize = true;
            this.dllLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.dllLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dllLabel.Location = new System.Drawing.Point(181, 111);
            this.dllLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dllLabel.Name = "dllLabel";
            this.dllLabel.Size = new System.Drawing.Size(25, 13);
            this.dllLabel.TabIndex = 9;
            this.dllLabel.Text = "123";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(6, 111);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(165, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Версия блибоитеки задач:";
            // 
            // serverTimeLabel
            // 
            this.serverTimeLabel.AutoSize = true;
            this.serverTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.serverTimeLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.serverTimeLabel.Location = new System.Drawing.Point(181, 88);
            this.serverTimeLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.serverTimeLabel.Name = "serverTimeLabel";
            this.serverTimeLabel.Size = new System.Drawing.Size(25, 13);
            this.serverTimeLabel.TabIndex = 7;
            this.serverTimeLabel.Text = "123";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label12.Location = new System.Drawing.Point(6, 88);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Время сервера:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(181, 65);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Нормально";
            // 
            // userNameLabel
            // 
            this.userNameLabel.AutoSize = true;
            this.userNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.userNameLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.userNameLabel.Location = new System.Drawing.Point(181, 42);
            this.userNameLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.userNameLabel.Name = "userNameLabel";
            this.userNameLabel.Size = new System.Drawing.Size(39, 13);
            this.userNameLabel.TabIndex = 4;
            this.userNameLabel.Text = "ilyanky";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(181, 19);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "192.168.20.150";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(6, 65);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Состояние подключения:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(6, 42);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Пользователь:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(6, 19);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Сервер:";
            // 
            // taskComboBox
            // 
            this.taskComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.taskComboBox.BackColor = System.Drawing.Color.White;
            this.taskComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.taskComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.taskComboBox.FormattingEnabled = true;
            this.taskComboBox.ItemHeight = 15;
            this.taskComboBox.Items.AddRange(new object[] {
            "Все задачи на месяц"});
            this.taskComboBox.Location = new System.Drawing.Point(0, 3);
            this.taskComboBox.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.taskComboBox.Name = "taskComboBox";
            this.taskComboBox.Size = new System.Drawing.Size(398, 23);
            this.taskComboBox.TabIndex = 2;
            this.taskComboBox.SelectedIndexChanged += new System.EventHandler(this.taskComboBox_SelectedIndexChanged);
            // 
            // contentTasksPanel
            // 
            this.contentTasksPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.contentTasksPanel.BackColor = System.Drawing.SystemColors.Control;
            this.contentTasksPanel.Controls.Add(this.monthTaskListView);
            this.contentTasksPanel.Controls.Add(this.dayTaskListView);
            this.contentTasksPanel.Location = new System.Drawing.Point(0, 29);
            this.contentTasksPanel.Margin = new System.Windows.Forms.Padding(0);
            this.contentTasksPanel.Name = "contentTasksPanel";
            this.contentTasksPanel.Size = new System.Drawing.Size(398, 274);
            this.contentTasksPanel.TabIndex = 3;
            // 
            // monthTaskListView
            // 
            this.monthTaskListView.BackColor = System.Drawing.SystemColors.Window;
            this.monthTaskListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.monthTaskListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7});
            this.monthTaskListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.monthTaskListView.FullRowSelect = true;
            this.monthTaskListView.HideSelection = false;
            this.monthTaskListView.Location = new System.Drawing.Point(0, 0);
            this.monthTaskListView.Margin = new System.Windows.Forms.Padding(0);
            this.monthTaskListView.Name = "monthTaskListView";
            this.monthTaskListView.Size = new System.Drawing.Size(398, 274);
            this.monthTaskListView.TabIndex = 1;
            this.monthTaskListView.UseCompatibleStateImageBehavior = false;
            this.monthTaskListView.View = System.Windows.Forms.View.Details;
            this.monthTaskListView.Visible = false;
            this.monthTaskListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.monthTaskListView_ItemSelectionChanged);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Имя задачи";
            this.columnHeader6.Width = 99;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Тип задачи";
            this.columnHeader7.Width = 86;
            // 
            // dayTaskListView
            // 
            this.dayTaskListView.BackColor = System.Drawing.SystemColors.Window;
            this.dayTaskListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dayTaskListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameHeader,
            this.columnHeader8,
            this.taskHeader,
            this.statusHeader,
            this.timeHeader});
            this.dayTaskListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayTaskListView.FullRowSelect = true;
            this.dayTaskListView.HideSelection = false;
            this.dayTaskListView.Location = new System.Drawing.Point(0, 0);
            this.dayTaskListView.Margin = new System.Windows.Forms.Padding(0);
            this.dayTaskListView.Name = "dayTaskListView";
            this.dayTaskListView.Size = new System.Drawing.Size(398, 274);
            this.dayTaskListView.TabIndex = 0;
            this.dayTaskListView.UseCompatibleStateImageBehavior = false;
            this.dayTaskListView.View = System.Windows.Forms.View.Details;
            this.dayTaskListView.Visible = false;
            this.dayTaskListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.taskListView_ItemSelectionChanged);
            // 
            // nameHeader
            // 
            this.nameHeader.Text = "Имя задачи";
            this.nameHeader.Width = 99;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Имя расписания";
            this.columnHeader8.Width = 79;
            // 
            // taskHeader
            // 
            this.taskHeader.Text = "Тип задачи";
            this.taskHeader.Width = 86;
            // 
            // statusHeader
            // 
            this.statusHeader.Text = "Статус";
            this.statusHeader.Width = 64;
            // 
            // timeHeader
            // 
            this.timeHeader.Text = "Время";
            this.timeHeader.Width = 109;
            // 
            // bottomVerticalSplitContainer
            // 
            this.bottomVerticalSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bottomVerticalSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomVerticalSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.bottomVerticalSplitContainer.Margin = new System.Windows.Forms.Padding(0);
            this.bottomVerticalSplitContainer.Name = "bottomVerticalSplitContainer";
            // 
            // bottomVerticalSplitContainer.Panel1
            // 
            this.bottomVerticalSplitContainer.Panel1.Controls.Add(this.toolStrip2);
            this.bottomVerticalSplitContainer.Panel1.Controls.Add(this.bottomContentPanel);
            // 
            // bottomVerticalSplitContainer.Panel2
            // 
            this.bottomVerticalSplitContainer.Panel2.Controls.Add(this.actionsPanel);
            this.bottomVerticalSplitContainer.Panel2MinSize = 227;
            this.bottomVerticalSplitContainer.Size = new System.Drawing.Size(1810, 180);
            this.bottomVerticalSplitContainer.SplitterDistance = 1523;
            this.bottomVerticalSplitContainer.SplitterWidth = 3;
            this.bottomVerticalSplitContainer.TabIndex = 0;
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generalToolStripButton,
            this.scheduleToolStripButton,
            this.toolStripSeparator9,
            this.performanceToolStripButton,
            this.logToolStripButton,
            this.toolStripSeparator10});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(1521, 29);
            this.toolStrip2.TabIndex = 0;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // generalToolStripButton
            // 
            this.generalToolStripButton.Checked = true;
            this.generalToolStripButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.generalToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.generalToolStripButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.generalToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.generalToolStripButton.Name = "generalToolStripButton";
            this.generalToolStripButton.Size = new System.Drawing.Size(53, 26);
            this.generalToolStripButton.Text = "Общие";
            this.generalToolStripButton.Click += new System.EventHandler(this.generalToolStripButton_Click);
            // 
            // scheduleToolStripButton
            // 
            this.scheduleToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.scheduleToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.scheduleToolStripButton.Name = "scheduleToolStripButton";
            this.scheduleToolStripButton.Size = new System.Drawing.Size(76, 26);
            this.scheduleToolStripButton.Text = "Расписание";
            this.scheduleToolStripButton.Click += new System.EventHandler(this.scheduleToolStripButton_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 29);
            // 
            // performanceToolStripButton
            // 
            this.performanceToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.performanceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.performanceToolStripButton.Name = "performanceToolStripButton";
            this.performanceToolStripButton.Size = new System.Drawing.Size(127, 26);
            this.performanceToolStripButton.Text = "Производительность";
            this.performanceToolStripButton.Click += new System.EventHandler(this.performanceToolStripButton_Click);
            // 
            // logToolStripButton
            // 
            this.logToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.logToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.logToolStripButton.Name = "logToolStripButton";
            this.logToolStripButton.Size = new System.Drawing.Size(55, 26);
            this.logToolStripButton.Text = "Журнал";
            this.logToolStripButton.Click += new System.EventHandler(this.logToolStripButton_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 29);
            // 
            // bottomContentPanel
            // 
            this.bottomContentPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bottomContentPanel.BackColor = System.Drawing.Color.White;
            this.bottomContentPanel.Controls.Add(this.performanceContentPanel);
            this.bottomContentPanel.Controls.Add(this.logContentBottomPanel);
            this.bottomContentPanel.Controls.Add(this.scheduleContentPanel);
            this.bottomContentPanel.Controls.Add(this.generalContentPanel);
            this.bottomContentPanel.Location = new System.Drawing.Point(0, 25);
            this.bottomContentPanel.Margin = new System.Windows.Forms.Padding(0);
            this.bottomContentPanel.Name = "bottomContentPanel";
            this.bottomContentPanel.Size = new System.Drawing.Size(1521, 155);
            this.bottomContentPanel.TabIndex = 5;
            // 
            // performanceContentPanel
            // 
            this.performanceContentPanel.Controls.Add(this.performanceListView);
            this.performanceContentPanel.Controls.Add(this.cpuPerfButton);
            this.performanceContentPanel.Controls.Add(this.label13);
            this.performanceContentPanel.Controls.Add(this.performanceStatusTextBox);
            this.performanceContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.performanceContentPanel.Location = new System.Drawing.Point(0, 0);
            this.performanceContentPanel.Name = "performanceContentPanel";
            this.performanceContentPanel.Size = new System.Drawing.Size(1521, 155);
            this.performanceContentPanel.TabIndex = 6;
            this.performanceContentPanel.Visible = false;
            // 
            // performanceListView
            // 
            this.performanceListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.performanceListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader12,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11});
            this.performanceListView.FullRowSelect = true;
            this.performanceListView.HideSelection = false;
            this.performanceListView.Location = new System.Drawing.Point(0, 49);
            this.performanceListView.Margin = new System.Windows.Forms.Padding(0);
            this.performanceListView.Name = "performanceListView";
            this.performanceListView.Size = new System.Drawing.Size(1521, 104);
            this.performanceListView.TabIndex = 4;
            this.performanceListView.UseCompatibleStateImageBehavior = false;
            this.performanceListView.View = System.Windows.Forms.View.Details;
            this.performanceListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.performanceListView_ItemSelectionChanged);
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Имя расписания";
            this.columnHeader12.Width = 120;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Задача запущена";
            this.columnHeader9.Width = 233;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Задача завершена";
            this.columnHeader10.Width = 514;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Состояние";
            this.columnHeader11.Width = 561;
            // 
            // cpuPerfButton
            // 
            this.cpuPerfButton.Location = new System.Drawing.Point(556, 14);
            this.cpuPerfButton.Name = "cpuPerfButton";
            this.cpuPerfButton.Size = new System.Drawing.Size(75, 23);
            this.cpuPerfButton.TabIndex = 3;
            this.cpuPerfButton.Text = "Подробнее";
            this.cpuPerfButton.UseVisualStyleBackColor = true;
            this.cpuPerfButton.Click += new System.EventHandler(this.cpuPerfButton_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 19);
            this.label13.Margin = new System.Windows.Forms.Padding(15, 15, 3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Статус:";
            // 
            // performanceStatusTextBox
            // 
            this.performanceStatusTextBox.Location = new System.Drawing.Point(65, 16);
            this.performanceStatusTextBox.Name = "performanceStatusTextBox";
            this.performanceStatusTextBox.ReadOnly = true;
            this.performanceStatusTextBox.Size = new System.Drawing.Size(485, 20);
            this.performanceStatusTextBox.TabIndex = 1;
            // 
            // logContentBottomPanel
            // 
            this.logContentBottomPanel.Controls.Add(this.taskLogListView);
            this.logContentBottomPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logContentBottomPanel.Location = new System.Drawing.Point(0, 0);
            this.logContentBottomPanel.Name = "logContentBottomPanel";
            this.logContentBottomPanel.Size = new System.Drawing.Size(1521, 155);
            this.logContentBottomPanel.TabIndex = 12;
            this.logContentBottomPanel.Visible = false;
            // 
            // taskLogListView
            // 
            this.taskLogListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.taskLogListView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.taskLogListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17});
            this.taskLogListView.Location = new System.Drawing.Point(0, 4);
            this.taskLogListView.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.taskLogListView.Name = "taskLogListView";
            this.taskLogListView.Size = new System.Drawing.Size(1521, 151);
            this.taskLogListView.TabIndex = 2;
            this.taskLogListView.UseCompatibleStateImageBehavior = false;
            this.taskLogListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Имя задачи";
            this.columnHeader13.Width = 202;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Время";
            this.columnHeader14.Width = 143;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Тип";
            this.columnHeader15.Width = 202;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Описание";
            this.columnHeader16.Width = 327;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Код завершения";
            this.columnHeader17.Width = 589;
            // 
            // scheduleContentPanel
            // 
            this.scheduleContentPanel.Controls.Add(this.schedListView);
            this.scheduleContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scheduleContentPanel.Location = new System.Drawing.Point(0, 0);
            this.scheduleContentPanel.Name = "scheduleContentPanel";
            this.scheduleContentPanel.Size = new System.Drawing.Size(1521, 155);
            this.scheduleContentPanel.TabIndex = 4;
            this.scheduleContentPanel.Visible = false;
            // 
            // schedListView
            // 
            this.schedListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.schedListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader5,
            this.columnHeader4});
            this.schedListView.FullRowSelect = true;
            this.schedListView.HideSelection = false;
            this.schedListView.Location = new System.Drawing.Point(0, 4);
            this.schedListView.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.schedListView.Name = "schedListView";
            this.schedListView.Size = new System.Drawing.Size(1521, 151);
            this.schedListView.TabIndex = 9;
            this.schedListView.UseCompatibleStateImageBehavior = false;
            this.schedListView.View = System.Windows.Forms.View.Details;
            this.schedListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.schedListView_ItemSelectionChanged);
            this.schedListView.Leave += new System.EventHandler(this.schedListView_Leave);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Рассписание";
            this.columnHeader1.Width = 127;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Описание";
            this.columnHeader2.Width = 220;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Триггер";
            this.columnHeader3.Width = 281;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Состояние";
            this.columnHeader5.Width = 115;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Подрбности";
            this.columnHeader4.Width = 347;
            // 
            // generalContentPanel
            // 
            this.generalContentPanel.BackColor = System.Drawing.Color.White;
            this.generalContentPanel.Controls.Add(this.autorTextBox);
            this.generalContentPanel.Controls.Add(this.label4);
            this.generalContentPanel.Controls.Add(this.label3);
            this.generalContentPanel.Controls.Add(this.businessTaskNameTextBox);
            this.generalContentPanel.Controls.Add(this.taskDescriptionTextBox);
            this.generalContentPanel.Controls.Add(this.taskNameTextBox);
            this.generalContentPanel.Controls.Add(this.label1);
            this.generalContentPanel.Controls.Add(this.label2);
            this.generalContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.generalContentPanel.Location = new System.Drawing.Point(0, 0);
            this.generalContentPanel.Name = "generalContentPanel";
            this.generalContentPanel.Size = new System.Drawing.Size(1521, 155);
            this.generalContentPanel.TabIndex = 6;
            this.generalContentPanel.Visible = false;
            // 
            // autorTextBox
            // 
            this.autorTextBox.Location = new System.Drawing.Point(59, 77);
            this.autorTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.autorTextBox.Name = "autorTextBox";
            this.autorTextBox.ReadOnly = true;
            this.autorTextBox.Size = new System.Drawing.Size(246, 20);
            this.autorTextBox.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(336, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Описание:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(11, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Автор:";
            // 
            // businessTaskNameTextBox
            // 
            this.businessTaskNameTextBox.Location = new System.Drawing.Point(59, 44);
            this.businessTaskNameTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.businessTaskNameTextBox.Name = "businessTaskNameTextBox";
            this.businessTaskNameTextBox.ReadOnly = true;
            this.businessTaskNameTextBox.Size = new System.Drawing.Size(246, 20);
            this.businessTaskNameTextBox.TabIndex = 6;
            // 
            // taskDescriptionTextBox
            // 
            this.taskDescriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.taskDescriptionTextBox.Location = new System.Drawing.Point(404, 11);
            this.taskDescriptionTextBox.Margin = new System.Windows.Forms.Padding(20);
            this.taskDescriptionTextBox.Multiline = true;
            this.taskDescriptionTextBox.Name = "taskDescriptionTextBox";
            this.taskDescriptionTextBox.ReadOnly = true;
            this.taskDescriptionTextBox.Size = new System.Drawing.Size(1097, 122);
            this.taskDescriptionTextBox.TabIndex = 5;
            // 
            // taskNameTextBox
            // 
            this.taskNameTextBox.Location = new System.Drawing.Point(59, 11);
            this.taskNameTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.taskNameTextBox.Name = "taskNameTextBox";
            this.taskNameTextBox.ReadOnly = true;
            this.taskNameTextBox.Size = new System.Drawing.Size(246, 20);
            this.taskNameTextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(10, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(10, 10, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Имя:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(10, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Задача:";
            // 
            // actionsPanel
            // 
            this.actionsPanel.BackColor = System.Drawing.Color.White;
            this.actionsPanel.Controls.Add(this.actionsContentPanel);
            this.actionsPanel.Controls.Add(this.actionsLabel);
            this.actionsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionsPanel.Location = new System.Drawing.Point(0, 0);
            this.actionsPanel.Margin = new System.Windows.Forms.Padding(0);
            this.actionsPanel.Name = "actionsPanel";
            this.actionsPanel.Size = new System.Drawing.Size(282, 178);
            this.actionsPanel.TabIndex = 0;
            // 
            // actionsContentPanel
            // 
            this.actionsContentPanel.Controls.Add(this.scheduleActionsContentPanel);
            this.actionsContentPanel.Controls.Add(this.taskActionsContentPanel);
            this.actionsContentPanel.Controls.Add(this.generalActionsContentPanel);
            this.actionsContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionsContentPanel.Location = new System.Drawing.Point(0, 29);
            this.actionsContentPanel.Name = "actionsContentPanel";
            this.actionsContentPanel.Size = new System.Drawing.Size(282, 149);
            this.actionsContentPanel.TabIndex = 1;
            // 
            // scheduleActionsContentPanel
            // 
            this.scheduleActionsContentPanel.Controls.Add(this.scheduleInfoActionLabel);
            this.scheduleActionsContentPanel.Controls.Add(this.toggleScheduleActivityActionLabel);
            this.scheduleActionsContentPanel.Controls.Add(this.panel2);
            this.scheduleActionsContentPanel.Controls.Add(this.deleteScheduleActionLabel);
            this.scheduleActionsContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scheduleActionsContentPanel.Location = new System.Drawing.Point(0, 0);
            this.scheduleActionsContentPanel.Name = "scheduleActionsContentPanel";
            this.scheduleActionsContentPanel.Size = new System.Drawing.Size(282, 149);
            this.scheduleActionsContentPanel.TabIndex = 8;
            this.scheduleActionsContentPanel.Visible = false;
            // 
            // scheduleInfoActionLabel
            // 
            this.scheduleInfoActionLabel.AutoSize = true;
            this.scheduleInfoActionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.scheduleInfoActionLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.scheduleInfoActionLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.scheduleInfoActionLabel.Location = new System.Drawing.Point(46, 92);
            this.scheduleInfoActionLabel.Margin = new System.Windows.Forms.Padding(15, 10, 3, 0);
            this.scheduleInfoActionLabel.Name = "scheduleInfoActionLabel";
            this.scheduleInfoActionLabel.Size = new System.Drawing.Size(82, 13);
            this.scheduleInfoActionLabel.TabIndex = 13;
            this.scheduleInfoActionLabel.Text = "Информация...";
            this.scheduleInfoActionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toggleScheduleActivityActionLabel
            // 
            this.toggleScheduleActivityActionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.toggleScheduleActivityActionLabel.Image = global::Planner.Properties.Resources.check_icon20x20;
            this.toggleScheduleActivityActionLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toggleScheduleActivityActionLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.toggleScheduleActivityActionLabel.Location = new System.Drawing.Point(13, 45);
            this.toggleScheduleActivityActionLabel.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.toggleScheduleActivityActionLabel.Name = "toggleScheduleActivityActionLabel";
            this.toggleScheduleActivityActionLabel.Size = new System.Drawing.Size(184, 23);
            this.toggleScheduleActivityActionLabel.TabIndex = 11;
            this.toggleScheduleActivityActionLabel.Text = "Активировать расписание...";
            this.toggleScheduleActivityActionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toggleScheduleActivityActionLabel.Click += new System.EventHandler(this.toggleScheduleActivityActionLabel_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Location = new System.Drawing.Point(10, 79);
            this.panel2.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(262, 1);
            this.panel2.TabIndex = 9;
            // 
            // deleteScheduleActionLabel
            // 
            this.deleteScheduleActionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteScheduleActionLabel.Image = global::Planner.Properties.Resources.Delete_Icon20x20;
            this.deleteScheduleActionLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteScheduleActionLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.deleteScheduleActionLabel.Location = new System.Drawing.Point(13, 15);
            this.deleteScheduleActionLabel.Margin = new System.Windows.Forms.Padding(15, 15, 3, 0);
            this.deleteScheduleActionLabel.Name = "deleteScheduleActionLabel";
            this.deleteScheduleActionLabel.Size = new System.Drawing.Size(156, 23);
            this.deleteScheduleActionLabel.TabIndex = 8;
            this.deleteScheduleActionLabel.Text = "Удалить расписание...";
            this.deleteScheduleActionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.deleteScheduleActionLabel.Click += new System.EventHandler(this.deleteScheduleActionLabel_Click);
            // 
            // taskActionsContentPanel
            // 
            this.taskActionsContentPanel.Controls.Add(this.infoActionLabel);
            this.taskActionsContentPanel.Controls.Add(this.taskLogActionLabel);
            this.taskActionsContentPanel.Controls.Add(this.toggleActivityActionLabel);
            this.taskActionsContentPanel.Controls.Add(this.addScheduleActionLabel);
            this.taskActionsContentPanel.Controls.Add(this.panel1);
            this.taskActionsContentPanel.Controls.Add(this.deleteTaskActionLabel);
            this.taskActionsContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taskActionsContentPanel.Location = new System.Drawing.Point(0, 0);
            this.taskActionsContentPanel.Name = "taskActionsContentPanel";
            this.taskActionsContentPanel.Size = new System.Drawing.Size(282, 149);
            this.taskActionsContentPanel.TabIndex = 5;
            this.taskActionsContentPanel.Visible = false;
            // 
            // infoActionLabel
            // 
            this.infoActionLabel.AutoSize = true;
            this.infoActionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.infoActionLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.infoActionLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.infoActionLabel.Location = new System.Drawing.Point(48, 125);
            this.infoActionLabel.Margin = new System.Windows.Forms.Padding(15, 10, 3, 0);
            this.infoActionLabel.Name = "infoActionLabel";
            this.infoActionLabel.Size = new System.Drawing.Size(82, 13);
            this.infoActionLabel.TabIndex = 7;
            this.infoActionLabel.Text = "Информация...";
            this.infoActionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // taskLogActionLabel
            // 
            this.taskLogActionLabel.AutoSize = true;
            this.taskLogActionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.taskLogActionLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.taskLogActionLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.taskLogActionLabel.Location = new System.Drawing.Point(48, 102);
            this.taskLogActionLabel.Margin = new System.Windows.Forms.Padding(15, 10, 3, 0);
            this.taskLogActionLabel.Name = "taskLogActionLabel";
            this.taskLogActionLabel.Size = new System.Drawing.Size(136, 13);
            this.taskLogActionLabel.TabIndex = 6;
            this.taskLogActionLabel.Text = "Посмотерть лог задачи...";
            this.taskLogActionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.taskLogActionLabel.Click += new System.EventHandler(this.taskLogActionLabel_Click);
            // 
            // toggleActivityActionLabel
            // 
            this.toggleActivityActionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.toggleActivityActionLabel.Image = global::Planner.Properties.Resources.check_icon20x20;
            this.toggleActivityActionLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toggleActivityActionLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.toggleActivityActionLabel.Location = new System.Drawing.Point(16, 66);
            this.toggleActivityActionLabel.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.toggleActivityActionLabel.Name = "toggleActivityActionLabel";
            this.toggleActivityActionLabel.Size = new System.Drawing.Size(156, 23);
            this.toggleActivityActionLabel.TabIndex = 5;
            this.toggleActivityActionLabel.Text = "Активировать задачу...";
            this.toggleActivityActionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toggleActivityActionLabel.Click += new System.EventHandler(this.toggleActivityActionLabel_Click);
            // 
            // addScheduleActionLabel
            // 
            this.addScheduleActionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.addScheduleActionLabel.Image = global::Planner.Properties.Resources.plus20x20;
            this.addScheduleActionLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addScheduleActionLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.addScheduleActionLabel.Location = new System.Drawing.Point(15, 10);
            this.addScheduleActionLabel.Margin = new System.Windows.Forms.Padding(15, 15, 3, 0);
            this.addScheduleActionLabel.Name = "addScheduleActionLabel";
            this.addScheduleActionLabel.Size = new System.Drawing.Size(161, 23);
            this.addScheduleActionLabel.TabIndex = 4;
            this.addScheduleActionLabel.Text = "Добавить расписание...";
            this.addScheduleActionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.addScheduleActionLabel.Click += new System.EventHandler(this.addScheduleActionLabel_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Location = new System.Drawing.Point(11, 92);
            this.panel1.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(262, 1);
            this.panel1.TabIndex = 3;
            // 
            // deleteTaskActionLabel
            // 
            this.deleteTaskActionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteTaskActionLabel.Image = global::Planner.Properties.Resources.Delete_Icon20x20;
            this.deleteTaskActionLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteTaskActionLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.deleteTaskActionLabel.Location = new System.Drawing.Point(15, 38);
            this.deleteTaskActionLabel.Margin = new System.Windows.Forms.Padding(15, 15, 3, 0);
            this.deleteTaskActionLabel.Name = "deleteTaskActionLabel";
            this.deleteTaskActionLabel.Size = new System.Drawing.Size(129, 23);
            this.deleteTaskActionLabel.TabIndex = 2;
            this.deleteTaskActionLabel.Text = "Удалить задачу...";
            this.deleteTaskActionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.deleteTaskActionLabel.Click += new System.EventHandler(this.removeTaskLabel_Click);
            // 
            // generalActionsContentPanel
            // 
            this.generalActionsContentPanel.Controls.Add(this.serverLogActionLabel);
            this.generalActionsContentPanel.Controls.Add(this.userLogActionLabel);
            this.generalActionsContentPanel.Controls.Add(this.generalTasksSeparator);
            this.generalActionsContentPanel.Controls.Add(this.addTaskActionLabel);
            this.generalActionsContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.generalActionsContentPanel.Location = new System.Drawing.Point(0, 0);
            this.generalActionsContentPanel.Name = "generalActionsContentPanel";
            this.generalActionsContentPanel.Size = new System.Drawing.Size(282, 149);
            this.generalActionsContentPanel.TabIndex = 3;
            this.generalActionsContentPanel.Visible = false;
            // 
            // serverLogActionLabel
            // 
            this.serverLogActionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverLogActionLabel.AutoSize = true;
            this.serverLogActionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.serverLogActionLabel.Location = new System.Drawing.Point(46, 79);
            this.serverLogActionLabel.Margin = new System.Windows.Forms.Padding(20, 15, 0, 0);
            this.serverLogActionLabel.Name = "serverLogActionLabel";
            this.serverLogActionLabel.Size = new System.Drawing.Size(137, 13);
            this.serverLogActionLabel.TabIndex = 3;
            this.serverLogActionLabel.Text = "Помотреть лог сервера...";
            this.serverLogActionLabel.Click += new System.EventHandler(this.serverLogActionLabel_Click);
            // 
            // userLogActionLabel
            // 
            this.userLogActionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userLogActionLabel.AutoSize = true;
            this.userLogActionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.userLogActionLabel.Location = new System.Drawing.Point(46, 51);
            this.userLogActionLabel.Margin = new System.Windows.Forms.Padding(20, 10, 0, 0);
            this.userLogActionLabel.Name = "userLogActionLabel";
            this.userLogActionLabel.Size = new System.Drawing.Size(166, 13);
            this.userLogActionLabel.TabIndex = 2;
            this.userLogActionLabel.Text = "Помотреть лог пользователя...";
            this.userLogActionLabel.Click += new System.EventHandler(this.userLogActionLabel_Click);
            // 
            // generalTasksSeparator
            // 
            this.generalTasksSeparator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.generalTasksSeparator.BackColor = System.Drawing.Color.Gainsboro;
            this.generalTasksSeparator.Location = new System.Drawing.Point(11, 40);
            this.generalTasksSeparator.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.generalTasksSeparator.Name = "generalTasksSeparator";
            this.generalTasksSeparator.Size = new System.Drawing.Size(262, 1);
            this.generalTasksSeparator.TabIndex = 1;
            // 
            // addTaskActionLabel
            // 
            this.addTaskActionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.addTaskActionLabel.Image = global::Planner.Properties.Resources.plus20x20;
            this.addTaskActionLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addTaskActionLabel.Location = new System.Drawing.Point(20, 10);
            this.addTaskActionLabel.Margin = new System.Windows.Forms.Padding(20, 10, 0, 0);
            this.addTaskActionLabel.Name = "addTaskActionLabel";
            this.addTaskActionLabel.Size = new System.Drawing.Size(156, 23);
            this.addTaskActionLabel.TabIndex = 0;
            this.addTaskActionLabel.Text = "Добавить задачу...";
            this.addTaskActionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.addTaskActionLabel.Click += new System.EventHandler(this.addTaskActionLabel_Click);
            // 
            // actionsLabel
            // 
            this.actionsLabel.BackColor = System.Drawing.SystemColors.Control;
            this.actionsLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.actionsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.actionsLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.actionsLabel.Location = new System.Drawing.Point(0, 0);
            this.actionsLabel.Margin = new System.Windows.Forms.Padding(0);
            this.actionsLabel.Name = "actionsLabel";
            this.actionsLabel.Size = new System.Drawing.Size(282, 29);
            this.actionsLabel.TabIndex = 0;
            this.actionsLabel.Text = "Действия";
            this.actionsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // serverTimeSyncTimer
            // 
            this.serverTimeSyncTimer.Interval = 1000;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1810, 745);
            this.Controls.Add(this.mainSplitContainer);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.DoubleBuffered = true;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "Main";
            this.Text = "Planner";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.topVerticalSplitContainer.Panel1.ResumeLayout(false);
            this.topVerticalSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.topVerticalSplitContainer)).EndInit();
            this.topVerticalSplitContainer.ResumeLayout(false);
            this.rightHorizontalSplitContainer.Panel1.ResumeLayout(false);
            this.rightHorizontalSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rightHorizontalSplitContainer)).EndInit();
            this.rightHorizontalSplitContainer.ResumeLayout(false);
            this.infoPanel.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.contentTasksPanel.ResumeLayout(false);
            this.bottomVerticalSplitContainer.Panel1.ResumeLayout(false);
            this.bottomVerticalSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bottomVerticalSplitContainer)).EndInit();
            this.bottomVerticalSplitContainer.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.bottomContentPanel.ResumeLayout(false);
            this.performanceContentPanel.ResumeLayout(false);
            this.performanceContentPanel.PerformLayout();
            this.logContentBottomPanel.ResumeLayout(false);
            this.scheduleContentPanel.ResumeLayout(false);
            this.generalContentPanel.ResumeLayout(false);
            this.generalContentPanel.PerformLayout();
            this.actionsPanel.ResumeLayout(false);
            this.actionsContentPanel.ResumeLayout(false);
            this.scheduleActionsContentPanel.ResumeLayout(false);
            this.scheduleActionsContentPanel.PerformLayout();
            this.taskActionsContentPanel.ResumeLayout(false);
            this.taskActionsContentPanel.PerformLayout();
            this.generalActionsContentPanel.ResumeLayout(false);
            this.generalActionsContentPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem toolsMenu;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.SplitContainer topVerticalSplitContainer;
        private TaskCalendar taskCalendar;
        private System.Windows.Forms.SplitContainer rightHorizontalSplitContainer;
        private System.Windows.Forms.Panel infoPanel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label userNameLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox taskComboBox;
        private System.Windows.Forms.Panel contentTasksPanel;
        private System.Windows.Forms.ListView dayTaskListView;
        private System.Windows.Forms.ColumnHeader taskHeader;
        private System.Windows.Forms.ColumnHeader statusHeader;
        private System.Windows.Forms.ColumnHeader timeHeader;
        private System.Windows.Forms.SplitContainer bottomVerticalSplitContainer;
        private System.Windows.Forms.Panel bottomContentPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox taskNameTextBox;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton generalToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton performanceToolStripButton;
        private System.Windows.Forms.ToolStripButton logToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton scheduleToolStripButton;
        private System.Windows.Forms.Panel actionsPanel;
        private System.Windows.Forms.Label deleteTaskActionLabel;
        private System.Windows.Forms.Label actionsLabel;
        private System.Windows.Forms.ToolStripMenuItem addTaskToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader nameHeader;
        private System.Windows.Forms.TextBox taskDescriptionTextBox;
        private System.Windows.Forms.Panel generalContentPanel;
        private System.Windows.Forms.Panel performanceContentPanel;
        private System.Windows.Forms.TextBox performanceStatusTextBox;
        private System.Windows.Forms.ToolStripButton refreshToolStripButton;
        private System.Windows.Forms.Label serverTimeLabel;
        private System.Windows.Forms.Label label12;
        private System.ComponentModel.BackgroundWorker serverTimeSynchronizer;
        private System.Windows.Forms.Timer serverTimeSyncTimer;
        private System.Windows.Forms.Timer serverTimeTimer;
        private System.Windows.Forms.Button cpuPerfButton;
        private System.Windows.Forms.Label label13;
        private System.ComponentModel.BackgroundWorker performance_BackgroundWorker;
        private System.Windows.Forms.Timer performanceTimer;
        private System.ComponentModel.BackgroundWorker taskUpdater;
        private System.Windows.Forms.Timer taskUpdateTimer;
        private System.Windows.Forms.Panel scheduleContentPanel;
        private System.Windows.Forms.ListView schedListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Panel actionsContentPanel;
        private System.Windows.Forms.Panel taskActionsContentPanel;
        private System.Windows.Forms.TextBox autorTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox businessTaskNameTextBox;
        private System.Windows.Forms.ListView monthTaskListView;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Panel logContentBottomPanel;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Panel generalActionsContentPanel;
        private System.Windows.Forms.Label addTaskActionLabel;
        private System.Windows.Forms.Panel generalTasksSeparator;
        private System.Windows.Forms.Label serverLogActionLabel;
        private System.Windows.Forms.Label userLogActionLabel;
        private System.Windows.Forms.Label addScheduleActionLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label toggleActivityActionLabel;
        private System.Windows.Forms.Label taskLogActionLabel;
        private System.Windows.Forms.Label infoActionLabel;
        private System.Windows.Forms.ListView taskLogListView;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.Panel scheduleActionsContentPanel;
        private System.Windows.Forms.Label scheduleInfoActionLabel;
        private System.Windows.Forms.Label toggleScheduleActivityActionLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label deleteScheduleActionLabel;
        private System.Windows.Forms.ToolStripMenuItem onlyActiveTasksToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Label dllLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListView performanceListView;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
    }
}



