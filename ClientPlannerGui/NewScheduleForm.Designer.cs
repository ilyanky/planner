﻿namespace Planner
{
    partial class NewScheduleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && (components != null) ) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nameLabel = new System.Windows.Forms.Label();
            this.rightPanel = new System.Windows.Forms.Panel();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.monthlyTriggeringPanel = new System.Windows.Forms.Panel();
            this.monthlyEndDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label29 = new System.Windows.Forms.Label();
            this.monthLabel = new System.Windows.Forms.Label();
            this.everyLabel = new System.Windows.Forms.Label();
            this.daysLabel = new System.Windows.Forms.Label();
            this.inDaysRadioButton = new System.Windows.Forms.RadioButton();
            this.inNumbersRadioButton = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.monthlyTimePicker = new System.Windows.Forms.DateTimePicker();
            this.monthlyDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dailyTriggeringPanel = new System.Windows.Forms.Panel();
            this.dailyEndDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.dailyTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.everyDayTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dailyDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.onceTriggeringPanel = new System.Windows.Forms.Panel();
            this.onceTimePicker = new System.Windows.Forms.DateTimePicker();
            this.onceDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nameDescriptionPanel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.addSchedFinishPanel = new System.Windows.Forms.Panel();
            this.finalMoreTextBox = new System.Windows.Forms.TextBox();
            this.finalTriggerTextBox = new System.Windows.Forms.TextBox();
            this.finalDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.finalNameTextBox = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.weeklyTriggeringPanel = new System.Windows.Forms.Panel();
            this.weeklyEndDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.weeklyTimePicker = new System.Windows.Forms.DateTimePicker();
            this.allCheckBox = new System.Windows.Forms.CheckBox();
            this.thursdayCheckBox = new System.Windows.Forms.CheckBox();
            this.sundayCheckBox = new System.Windows.Forms.CheckBox();
            this.saturdayCheckBox = new System.Windows.Forms.CheckBox();
            this.fridayCheckBox = new System.Windows.Forms.CheckBox();
            this.wednesdayCheckBox = new System.Windows.Forms.CheckBox();
            this.tuesdayCheckBox = new System.Windows.Forms.CheckBox();
            this.mondayCheckBox = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.everyWeekTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.weeklyDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.triggersPanel = new System.Windows.Forms.Panel();
            this.monthlyRadioButton = new System.Windows.Forms.RadioButton();
            this.weeklyRadioButton = new System.Windows.Forms.RadioButton();
            this.dailyRadioButton = new System.Windows.Forms.RadioButton();
            this.onceRadioButton = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonsPanel = new System.Windows.Forms.Panel();
            this.prevButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.leftPanel = new System.Windows.Forms.Panel();
            this.finishLabel = new System.Windows.Forms.Label();
            this.triggerLabel = new System.Windows.Forms.Label();
            this.chooseTriggerLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chooseNameLabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.monthPicker1 = new Planner.MonthPicker();
            this.dayOfMonthPicker1 = new Planner.DayOfMonthPicker();
            this.weekDayPicker1 = new Planner.WeekDayPicker();
            this.everyDayPicker1 = new Planner.EveryDayPicker();
            this.topPanel.SuspendLayout();
            this.rightPanel.SuspendLayout();
            this.contentPanel.SuspendLayout();
            this.monthlyTriggeringPanel.SuspendLayout();
            this.dailyTriggeringPanel.SuspendLayout();
            this.onceTriggeringPanel.SuspendLayout();
            this.nameDescriptionPanel.SuspendLayout();
            this.addSchedFinishPanel.SuspendLayout();
            this.weeklyTriggeringPanel.SuspendLayout();
            this.triggersPanel.SuspendLayout();
            this.buttonsPanel.SuspendLayout();
            this.leftPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.Color.Transparent;
            this.topPanel.Controls.Add(this.panel1);
            this.topPanel.Controls.Add(this.nameLabel);
            this.topPanel.Controls.Add(this.pictureBox1);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Margin = new System.Windows.Forms.Padding(0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(699, 100);
            this.topPanel.TabIndex = 15;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(476, 530);
            this.panel1.TabIndex = 0;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameLabel.ForeColor = System.Drawing.Color.Black;
            this.nameLabel.Location = new System.Drawing.Point(138, 27);
            this.nameLabel.Margin = new System.Windows.Forms.Padding(0);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(385, 39);
            this.nameLabel.TabIndex = 1;
            this.nameLabel.Text = "Добавить расписание";
            // 
            // rightPanel
            // 
            this.rightPanel.BackColor = System.Drawing.Color.White;
            this.rightPanel.Controls.Add(this.contentPanel);
            this.rightPanel.Controls.Add(this.buttonsPanel);
            this.rightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.rightPanel.Location = new System.Drawing.Point(165, 100);
            this.rightPanel.Margin = new System.Windows.Forms.Padding(0);
            this.rightPanel.Name = "rightPanel";
            this.rightPanel.Size = new System.Drawing.Size(534, 390);
            this.rightPanel.TabIndex = 16;
            // 
            // contentPanel
            // 
            this.contentPanel.Controls.Add(this.monthlyTriggeringPanel);
            this.contentPanel.Controls.Add(this.dailyTriggeringPanel);
            this.contentPanel.Controls.Add(this.onceTriggeringPanel);
            this.contentPanel.Controls.Add(this.nameDescriptionPanel);
            this.contentPanel.Controls.Add(this.addSchedFinishPanel);
            this.contentPanel.Controls.Add(this.weeklyTriggeringPanel);
            this.contentPanel.Controls.Add(this.triggersPanel);
            this.contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentPanel.Location = new System.Drawing.Point(0, 0);
            this.contentPanel.Margin = new System.Windows.Forms.Padding(2);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(534, 348);
            this.contentPanel.TabIndex = 12;
            // 
            // monthlyTriggeringPanel
            // 
            this.monthlyTriggeringPanel.Controls.Add(this.monthPicker1);
            this.monthlyTriggeringPanel.Controls.Add(this.dayOfMonthPicker1);
            this.monthlyTriggeringPanel.Controls.Add(this.weekDayPicker1);
            this.monthlyTriggeringPanel.Controls.Add(this.everyDayPicker1);
            this.monthlyTriggeringPanel.Controls.Add(this.label18);
            this.monthlyTriggeringPanel.Controls.Add(this.monthlyEndDatePicker);
            this.monthlyTriggeringPanel.Controls.Add(this.label29);
            this.monthlyTriggeringPanel.Controls.Add(this.monthLabel);
            this.monthlyTriggeringPanel.Controls.Add(this.everyLabel);
            this.monthlyTriggeringPanel.Controls.Add(this.daysLabel);
            this.monthlyTriggeringPanel.Controls.Add(this.inDaysRadioButton);
            this.monthlyTriggeringPanel.Controls.Add(this.inNumbersRadioButton);
            this.monthlyTriggeringPanel.Controls.Add(this.label15);
            this.monthlyTriggeringPanel.Controls.Add(this.label14);
            this.monthlyTriggeringPanel.Controls.Add(this.monthlyTimePicker);
            this.monthlyTriggeringPanel.Controls.Add(this.monthlyDatePicker);
            this.monthlyTriggeringPanel.Controls.Add(this.label16);
            this.monthlyTriggeringPanel.Controls.Add(this.label17);
            this.monthlyTriggeringPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.monthlyTriggeringPanel.Location = new System.Drawing.Point(0, 0);
            this.monthlyTriggeringPanel.Margin = new System.Windows.Forms.Padding(2);
            this.monthlyTriggeringPanel.Name = "monthlyTriggeringPanel";
            this.monthlyTriggeringPanel.Size = new System.Drawing.Size(534, 348);
            this.monthlyTriggeringPanel.TabIndex = 29;
            this.monthlyTriggeringPanel.Visible = false;
            // 
            // monthlyEndDatePicker
            // 
            this.monthlyEndDatePicker.Location = new System.Drawing.Point(95, 254);
            this.monthlyEndDatePicker.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.monthlyEndDatePicker.Name = "monthlyEndDatePicker";
            this.monthlyEndDatePicker.Size = new System.Drawing.Size(143, 20);
            this.monthlyEndDatePicker.TabIndex = 46;
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(25, 257);
            this.label29.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 13);
            this.label29.TabIndex = 45;
            this.label29.Text = "Закончить";
            // 
            // monthLabel
            // 
            this.monthLabel.AutoSize = true;
            this.monthLabel.Enabled = false;
            this.monthLabel.Location = new System.Drawing.Point(474, 222);
            this.monthLabel.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.monthLabel.Name = "monthLabel";
            this.monthLabel.Size = new System.Drawing.Size(45, 13);
            this.monthLabel.TabIndex = 44;
            this.monthLabel.Text = "месяца";
            // 
            // everyLabel
            // 
            this.everyLabel.AutoSize = true;
            this.everyLabel.Enabled = false;
            this.everyLabel.Location = new System.Drawing.Point(59, 222);
            this.everyLabel.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.everyLabel.Name = "everyLabel";
            this.everyLabel.Size = new System.Drawing.Size(57, 13);
            this.everyLabel.TabIndex = 41;
            this.everyLabel.Text = "В каждый";
            // 
            // daysLabel
            // 
            this.daysLabel.AutoSize = true;
            this.daysLabel.Enabled = false;
            this.daysLabel.Location = new System.Drawing.Point(59, 176);
            this.daysLabel.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.daysLabel.Name = "daysLabel";
            this.daysLabel.Size = new System.Drawing.Size(31, 13);
            this.daysLabel.TabIndex = 40;
            this.daysLabel.Text = "Дни:";
            // 
            // inDaysRadioButton
            // 
            this.inDaysRadioButton.AutoSize = true;
            this.inDaysRadioButton.Location = new System.Drawing.Point(38, 197);
            this.inDaysRadioButton.Margin = new System.Windows.Forms.Padding(2, 10, 2, 2);
            this.inDaysRadioButton.Name = "inDaysRadioButton";
            this.inDaysRadioButton.Size = new System.Drawing.Size(110, 17);
            this.inDaysRadioButton.TabIndex = 38;
            this.inDaysRadioButton.TabStop = true;
            this.inDaysRadioButton.Text = "По дням недели:";
            this.inDaysRadioButton.UseVisualStyleBackColor = true;
            this.inDaysRadioButton.CheckedChanged += new System.EventHandler(this.inDaysRadioButton_CheckedChanged);
            // 
            // inNumbersRadioButton
            // 
            this.inNumbersRadioButton.AutoSize = true;
            this.inNumbersRadioButton.Location = new System.Drawing.Point(39, 151);
            this.inNumbersRadioButton.Margin = new System.Windows.Forms.Padding(2, 10, 2, 2);
            this.inNumbersRadioButton.Name = "inNumbersRadioButton";
            this.inNumbersRadioButton.Size = new System.Drawing.Size(163, 17);
            this.inNumbersRadioButton.TabIndex = 37;
            this.inNumbersRadioButton.TabStop = true;
            this.inNumbersRadioButton.Text = "Конкретные числа месяца:";
            this.inNumbersRadioButton.UseVisualStyleBackColor = true;
            this.inNumbersRadioButton.CheckedChanged += new System.EventHandler(this.inNumbersRadioButton_CheckedChanged);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 126);
            this.label15.Margin = new System.Windows.Forms.Padding(25, 5, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(293, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Выберите дни, по которым вы хотите запускать задачи:";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(25, 75);
            this.label14.Margin = new System.Windows.Forms.Padding(25, 5, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(296, 13);
            this.label14.TabIndex = 34;
            this.label14.Text = "Выберите месяцы, в которые будут запускаться задачи:";
            // 
            // monthlyTimePicker
            // 
            this.monthlyTimePicker.CustomFormat = "";
            this.monthlyTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.monthlyTimePicker.Location = new System.Drawing.Point(236, 48);
            this.monthlyTimePicker.Name = "monthlyTimePicker";
            this.monthlyTimePicker.ShowUpDown = true;
            this.monthlyTimePicker.Size = new System.Drawing.Size(70, 20);
            this.monthlyTimePicker.TabIndex = 32;
            // 
            // monthlyDatePicker
            // 
            this.monthlyDatePicker.Location = new System.Drawing.Point(87, 48);
            this.monthlyDatePicker.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.monthlyDatePicker.Name = "monthlyDatePicker";
            this.monthlyDatePicker.Size = new System.Drawing.Size(143, 20);
            this.monthlyDatePicker.TabIndex = 31;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(36, 51);
            this.label16.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "Начать";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(25, 28);
            this.label17.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(173, 13);
            this.label17.TabIndex = 29;
            this.label17.Text = "Выберите время и дату запуска:";
            // 
            // dailyTriggeringPanel
            // 
            this.dailyTriggeringPanel.Controls.Add(this.dailyEndDatePicker);
            this.dailyTriggeringPanel.Controls.Add(this.label27);
            this.dailyTriggeringPanel.Controls.Add(this.label26);
            this.dailyTriggeringPanel.Controls.Add(this.dailyTimePicker);
            this.dailyTriggeringPanel.Controls.Add(this.label9);
            this.dailyTriggeringPanel.Controls.Add(this.everyDayTextBox);
            this.dailyTriggeringPanel.Controls.Add(this.label5);
            this.dailyTriggeringPanel.Controls.Add(this.dailyDatePicker);
            this.dailyTriggeringPanel.Controls.Add(this.label3);
            this.dailyTriggeringPanel.Controls.Add(this.label4);
            this.dailyTriggeringPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dailyTriggeringPanel.Location = new System.Drawing.Point(0, 0);
            this.dailyTriggeringPanel.Margin = new System.Windows.Forms.Padding(0);
            this.dailyTriggeringPanel.Name = "dailyTriggeringPanel";
            this.dailyTriggeringPanel.Size = new System.Drawing.Size(534, 348);
            this.dailyTriggeringPanel.TabIndex = 12;
            this.dailyTriggeringPanel.Visible = false;
            // 
            // dailyEndDatePicker
            // 
            this.dailyEndDatePicker.Location = new System.Drawing.Point(103, 125);
            this.dailyEndDatePicker.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.dailyEndDatePicker.Name = "dailyEndDatePicker";
            this.dailyEndDatePicker.Size = new System.Drawing.Size(143, 20);
            this.dailyEndDatePicker.TabIndex = 38;
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(36, 128);
            this.label27.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(60, 13);
            this.label27.TabIndex = 37;
            this.label27.Text = "Закончить";
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(240, 58);
            this.label26.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 13);
            this.label26.TabIndex = 36;
            this.label26.Text = "запускать в ";
            // 
            // dailyTimePicker
            // 
            this.dailyTimePicker.CustomFormat = "";
            this.dailyTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dailyTimePicker.Location = new System.Drawing.Point(311, 56);
            this.dailyTimePicker.Name = "dailyTimePicker";
            this.dailyTimePicker.ShowUpDown = true;
            this.dailyTimePicker.Size = new System.Drawing.Size(70, 20);
            this.dailyTimePicker.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(194, 93);
            this.label9.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "дн.";
            // 
            // everyDayTextBox
            // 
            this.everyDayTextBox.Location = new System.Drawing.Point(158, 90);
            this.everyDayTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.everyDayTextBox.Name = "everyDayTextBox";
            this.everyDayTextBox.Size = new System.Drawing.Size(31, 20);
            this.everyDayTextBox.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 93);
            this.label5.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Повторять каждые";
            // 
            // dailyDatePicker
            // 
            this.dailyDatePicker.Location = new System.Drawing.Point(93, 55);
            this.dailyDatePicker.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.dailyDatePicker.Name = "dailyDatePicker";
            this.dailyDatePicker.Size = new System.Drawing.Size(143, 20);
            this.dailyDatePicker.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 58);
            this.label3.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Начать с";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 28);
            this.label4.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(173, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Выберите время и дату запуска:";
            // 
            // onceTriggeringPanel
            // 
            this.onceTriggeringPanel.Controls.Add(this.onceTimePicker);
            this.onceTriggeringPanel.Controls.Add(this.onceDatePicker);
            this.onceTriggeringPanel.Controls.Add(this.label8);
            this.onceTriggeringPanel.Controls.Add(this.label6);
            this.onceTriggeringPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.onceTriggeringPanel.Location = new System.Drawing.Point(0, 0);
            this.onceTriggeringPanel.Name = "onceTriggeringPanel";
            this.onceTriggeringPanel.Size = new System.Drawing.Size(534, 348);
            this.onceTriggeringPanel.TabIndex = 12;
            this.onceTriggeringPanel.Visible = false;
            // 
            // onceTimePicker
            // 
            this.onceTimePicker.CustomFormat = "";
            this.onceTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.onceTimePicker.Location = new System.Drawing.Point(245, 52);
            this.onceTimePicker.Name = "onceTimePicker";
            this.onceTimePicker.ShowUpDown = true;
            this.onceTimePicker.Size = new System.Drawing.Size(70, 20);
            this.onceTimePicker.TabIndex = 5;
            // 
            // onceDatePicker
            // 
            this.onceDatePicker.Location = new System.Drawing.Point(96, 52);
            this.onceDatePicker.Name = "onceDatePicker";
            this.onceDatePicker.Size = new System.Drawing.Size(143, 20);
            this.onceDatePicker.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 55);
            this.label8.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Запустить";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 25);
            this.label6.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Выберите время и дату запуска:";
            // 
            // nameDescriptionPanel
            // 
            this.nameDescriptionPanel.Controls.Add(this.label2);
            this.nameDescriptionPanel.Controls.Add(this.label1);
            this.nameDescriptionPanel.Controls.Add(this.nameTextBox);
            this.nameDescriptionPanel.Controls.Add(this.descriptionTextBox);
            this.nameDescriptionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nameDescriptionPanel.Location = new System.Drawing.Point(0, 0);
            this.nameDescriptionPanel.Name = "nameDescriptionPanel";
            this.nameDescriptionPanel.Size = new System.Drawing.Size(534, 348);
            this.nameDescriptionPanel.TabIndex = 0;
            this.nameDescriptionPanel.Visible = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Описание:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Имя расписания:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameTextBox.Location = new System.Drawing.Point(130, 22);
            this.nameTextBox.Margin = new System.Windows.Forms.Padding(5, 3, 15, 3);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(380, 20);
            this.nameTextBox.TabIndex = 0;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.descriptionTextBox.Location = new System.Drawing.Point(130, 60);
            this.descriptionTextBox.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(380, 194);
            this.descriptionTextBox.TabIndex = 3;
            // 
            // addSchedFinishPanel
            // 
            this.addSchedFinishPanel.Controls.Add(this.finalMoreTextBox);
            this.addSchedFinishPanel.Controls.Add(this.finalTriggerTextBox);
            this.addSchedFinishPanel.Controls.Add(this.finalDescriptionTextBox);
            this.addSchedFinishPanel.Controls.Add(this.finalNameTextBox);
            this.addSchedFinishPanel.Controls.Add(this.label25);
            this.addSchedFinishPanel.Controls.Add(this.label24);
            this.addSchedFinishPanel.Controls.Add(this.label23);
            this.addSchedFinishPanel.Controls.Add(this.label22);
            this.addSchedFinishPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addSchedFinishPanel.Location = new System.Drawing.Point(0, 0);
            this.addSchedFinishPanel.Name = "addSchedFinishPanel";
            this.addSchedFinishPanel.Size = new System.Drawing.Size(534, 348);
            this.addSchedFinishPanel.TabIndex = 32;
            this.addSchedFinishPanel.Visible = false;
            // 
            // finalMoreTextBox
            // 
            this.finalMoreTextBox.Location = new System.Drawing.Point(105, 267);
            this.finalMoreTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 25);
            this.finalMoreTextBox.Multiline = true;
            this.finalMoreTextBox.Name = "finalMoreTextBox";
            this.finalMoreTextBox.ReadOnly = true;
            this.finalMoreTextBox.Size = new System.Drawing.Size(395, 54);
            this.finalMoreTextBox.TabIndex = 7;
            // 
            // finalTriggerTextBox
            // 
            this.finalTriggerTextBox.Location = new System.Drawing.Point(105, 219);
            this.finalTriggerTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 25);
            this.finalTriggerTextBox.Name = "finalTriggerTextBox";
            this.finalTriggerTextBox.ReadOnly = true;
            this.finalTriggerTextBox.Size = new System.Drawing.Size(395, 20);
            this.finalTriggerTextBox.TabIndex = 6;
            // 
            // finalDescriptionTextBox
            // 
            this.finalDescriptionTextBox.Location = new System.Drawing.Point(105, 60);
            this.finalDescriptionTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 25, 25);
            this.finalDescriptionTextBox.Multiline = true;
            this.finalDescriptionTextBox.Name = "finalDescriptionTextBox";
            this.finalDescriptionTextBox.ReadOnly = true;
            this.finalDescriptionTextBox.Size = new System.Drawing.Size(395, 131);
            this.finalDescriptionTextBox.TabIndex = 5;
            // 
            // finalNameTextBox
            // 
            this.finalNameTextBox.Location = new System.Drawing.Point(105, 22);
            this.finalNameTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.finalNameTextBox.Name = "finalNameTextBox";
            this.finalNameTextBox.ReadOnly = true;
            this.finalNameTextBox.Size = new System.Drawing.Size(395, 20);
            this.finalNameTextBox.TabIndex = 4;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(24, 270);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(66, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "Подробнее:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(24, 222);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(51, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Триггер:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(25, 63);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Описание:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(25, 25);
            this.label22.Margin = new System.Windows.Forms.Padding(25, 25, 5, 25);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Имя:";
            // 
            // weeklyTriggeringPanel
            // 
            this.weeklyTriggeringPanel.Controls.Add(this.weeklyEndDatePicker);
            this.weeklyTriggeringPanel.Controls.Add(this.label28);
            this.weeklyTriggeringPanel.Controls.Add(this.label21);
            this.weeklyTriggeringPanel.Controls.Add(this.weeklyTimePicker);
            this.weeklyTriggeringPanel.Controls.Add(this.allCheckBox);
            this.weeklyTriggeringPanel.Controls.Add(this.thursdayCheckBox);
            this.weeklyTriggeringPanel.Controls.Add(this.sundayCheckBox);
            this.weeklyTriggeringPanel.Controls.Add(this.saturdayCheckBox);
            this.weeklyTriggeringPanel.Controls.Add(this.fridayCheckBox);
            this.weeklyTriggeringPanel.Controls.Add(this.wednesdayCheckBox);
            this.weeklyTriggeringPanel.Controls.Add(this.tuesdayCheckBox);
            this.weeklyTriggeringPanel.Controls.Add(this.mondayCheckBox);
            this.weeklyTriggeringPanel.Controls.Add(this.label10);
            this.weeklyTriggeringPanel.Controls.Add(this.everyWeekTextBox);
            this.weeklyTriggeringPanel.Controls.Add(this.label11);
            this.weeklyTriggeringPanel.Controls.Add(this.weeklyDatePicker);
            this.weeklyTriggeringPanel.Controls.Add(this.label12);
            this.weeklyTriggeringPanel.Controls.Add(this.label13);
            this.weeklyTriggeringPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.weeklyTriggeringPanel.Location = new System.Drawing.Point(0, 0);
            this.weeklyTriggeringPanel.Margin = new System.Windows.Forms.Padding(2);
            this.weeklyTriggeringPanel.Name = "weeklyTriggeringPanel";
            this.weeklyTriggeringPanel.Size = new System.Drawing.Size(534, 348);
            this.weeklyTriggeringPanel.TabIndex = 13;
            this.weeklyTriggeringPanel.Visible = false;
            // 
            // weeklyEndDatePicker
            // 
            this.weeklyEndDatePicker.Location = new System.Drawing.Point(103, 159);
            this.weeklyEndDatePicker.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.weeklyEndDatePicker.Name = "weeklyEndDatePicker";
            this.weeklyEndDatePicker.Size = new System.Drawing.Size(143, 20);
            this.weeklyEndDatePicker.TabIndex = 40;
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(36, 162);
            this.label28.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(60, 13);
            this.label28.TabIndex = 39;
            this.label28.Text = "Закончить";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(240, 57);
            this.label21.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(71, 13);
            this.label21.TabIndex = 32;
            this.label21.Text = "запускать в ";
            // 
            // weeklyTimePicker
            // 
            this.weeklyTimePicker.CustomFormat = "";
            this.weeklyTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.weeklyTimePicker.Location = new System.Drawing.Point(311, 55);
            this.weeklyTimePicker.Name = "weeklyTimePicker";
            this.weeklyTimePicker.ShowUpDown = true;
            this.weeklyTimePicker.Size = new System.Drawing.Size(70, 20);
            this.weeklyTimePicker.TabIndex = 31;
            // 
            // allCheckBox
            // 
            this.allCheckBox.AutoSize = true;
            this.allCheckBox.Location = new System.Drawing.Point(64, 109);
            this.allCheckBox.Margin = new System.Windows.Forms.Padding(2, 2, 8, 2);
            this.allCheckBox.Name = "allCheckBox";
            this.allCheckBox.Size = new System.Drawing.Size(103, 17);
            this.allCheckBox.TabIndex = 28;
            this.allCheckBox.Text = "<Выбрать все>";
            this.allCheckBox.UseVisualStyleBackColor = true;
            // 
            // thursdayCheckBox
            // 
            this.thursdayCheckBox.AutoSize = true;
            this.thursdayCheckBox.Location = new System.Drawing.Point(244, 109);
            this.thursdayCheckBox.Margin = new System.Windows.Forms.Padding(2, 2, 8, 2);
            this.thursdayCheckBox.Name = "thursdayCheckBox";
            this.thursdayCheckBox.Size = new System.Drawing.Size(68, 17);
            this.thursdayCheckBox.TabIndex = 27;
            this.thursdayCheckBox.Text = "Четверг";
            this.thursdayCheckBox.UseVisualStyleBackColor = true;
            // 
            // sundayCheckBox
            // 
            this.sundayCheckBox.AutoSize = true;
            this.sundayCheckBox.Location = new System.Drawing.Point(316, 131);
            this.sundayCheckBox.Margin = new System.Windows.Forms.Padding(2, 2, 8, 2);
            this.sundayCheckBox.Name = "sundayCheckBox";
            this.sundayCheckBox.Size = new System.Drawing.Size(93, 17);
            this.sundayCheckBox.TabIndex = 26;
            this.sundayCheckBox.Text = "Воскресенье";
            this.sundayCheckBox.UseVisualStyleBackColor = true;
            // 
            // saturdayCheckBox
            // 
            this.saturdayCheckBox.AutoSize = true;
            this.saturdayCheckBox.Location = new System.Drawing.Point(316, 109);
            this.saturdayCheckBox.Margin = new System.Windows.Forms.Padding(2, 2, 8, 2);
            this.saturdayCheckBox.Name = "saturdayCheckBox";
            this.saturdayCheckBox.Size = new System.Drawing.Size(67, 17);
            this.saturdayCheckBox.TabIndex = 25;
            this.saturdayCheckBox.Text = "Суббота";
            this.saturdayCheckBox.UseVisualStyleBackColor = true;
            // 
            // fridayCheckBox
            // 
            this.fridayCheckBox.AutoSize = true;
            this.fridayCheckBox.Location = new System.Drawing.Point(244, 131);
            this.fridayCheckBox.Margin = new System.Windows.Forms.Padding(2, 2, 8, 2);
            this.fridayCheckBox.Name = "fridayCheckBox";
            this.fridayCheckBox.Size = new System.Drawing.Size(69, 17);
            this.fridayCheckBox.TabIndex = 24;
            this.fridayCheckBox.Text = "Пятница";
            this.fridayCheckBox.UseVisualStyleBackColor = true;
            // 
            // wednesdayCheckBox
            // 
            this.wednesdayCheckBox.AutoSize = true;
            this.wednesdayCheckBox.Location = new System.Drawing.Point(171, 131);
            this.wednesdayCheckBox.Margin = new System.Windows.Forms.Padding(2, 2, 8, 2);
            this.wednesdayCheckBox.Name = "wednesdayCheckBox";
            this.wednesdayCheckBox.Size = new System.Drawing.Size(57, 17);
            this.wednesdayCheckBox.TabIndex = 23;
            this.wednesdayCheckBox.Text = "Среда";
            this.wednesdayCheckBox.UseVisualStyleBackColor = true;
            // 
            // tuesdayCheckBox
            // 
            this.tuesdayCheckBox.AutoSize = true;
            this.tuesdayCheckBox.Location = new System.Drawing.Point(171, 109);
            this.tuesdayCheckBox.Margin = new System.Windows.Forms.Padding(2, 2, 8, 2);
            this.tuesdayCheckBox.Name = "tuesdayCheckBox";
            this.tuesdayCheckBox.Size = new System.Drawing.Size(68, 17);
            this.tuesdayCheckBox.TabIndex = 22;
            this.tuesdayCheckBox.Text = "Вторник";
            this.tuesdayCheckBox.UseVisualStyleBackColor = true;
            // 
            // mondayCheckBox
            // 
            this.mondayCheckBox.AutoSize = true;
            this.mondayCheckBox.Location = new System.Drawing.Point(64, 131);
            this.mondayCheckBox.Margin = new System.Windows.Forms.Padding(2, 2, 8, 2);
            this.mondayCheckBox.Name = "mondayCheckBox";
            this.mondayCheckBox.Size = new System.Drawing.Size(94, 17);
            this.mondayCheckBox.TabIndex = 21;
            this.mondayCheckBox.Text = "Понедельник";
            this.mondayCheckBox.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(194, 85);
            this.label10.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(155, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "неделю, по следующим дням";
            // 
            // everyWeekTextBox
            // 
            this.everyWeekTextBox.Location = new System.Drawing.Point(155, 83);
            this.everyWeekTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.everyWeekTextBox.Name = "everyWeekTextBox";
            this.everyWeekTextBox.Size = new System.Drawing.Size(31, 20);
            this.everyWeekTextBox.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(49, 85);
            this.label11.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Повторять каждую";
            // 
            // weeklyDatePicker
            // 
            this.weeklyDatePicker.Location = new System.Drawing.Point(91, 54);
            this.weeklyDatePicker.Name = "weeklyDatePicker";
            this.weeklyDatePicker.Size = new System.Drawing.Size(143, 20);
            this.weeklyDatePicker.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(36, 57);
            this.label12.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Начать с";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(25, 28);
            this.label13.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(173, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Выберите время и дату запуска:";
            // 
            // triggersPanel
            // 
            this.triggersPanel.Controls.Add(this.monthlyRadioButton);
            this.triggersPanel.Controls.Add(this.weeklyRadioButton);
            this.triggersPanel.Controls.Add(this.dailyRadioButton);
            this.triggersPanel.Controls.Add(this.onceRadioButton);
            this.triggersPanel.Controls.Add(this.label7);
            this.triggersPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.triggersPanel.Location = new System.Drawing.Point(0, 0);
            this.triggersPanel.Name = "triggersPanel";
            this.triggersPanel.Size = new System.Drawing.Size(534, 348);
            this.triggersPanel.TabIndex = 7;
            this.triggersPanel.Visible = false;
            // 
            // monthlyRadioButton
            // 
            this.monthlyRadioButton.AutoSize = true;
            this.monthlyRadioButton.Location = new System.Drawing.Point(38, 119);
            this.monthlyRadioButton.Name = "monthlyRadioButton";
            this.monthlyRadioButton.Size = new System.Drawing.Size(89, 17);
            this.monthlyRadioButton.TabIndex = 11;
            this.monthlyRadioButton.Text = "Ежемесячно";
            this.monthlyRadioButton.UseVisualStyleBackColor = true;
            // 
            // weeklyRadioButton
            // 
            this.weeklyRadioButton.AutoSize = true;
            this.weeklyRadioButton.Location = new System.Drawing.Point(38, 96);
            this.weeklyRadioButton.Name = "weeklyRadioButton";
            this.weeklyRadioButton.Size = new System.Drawing.Size(94, 17);
            this.weeklyRadioButton.TabIndex = 10;
            this.weeklyRadioButton.Text = "Еженедельно";
            this.weeklyRadioButton.UseVisualStyleBackColor = true;
            // 
            // dailyRadioButton
            // 
            this.dailyRadioButton.AutoSize = true;
            this.dailyRadioButton.Location = new System.Drawing.Point(38, 73);
            this.dailyRadioButton.Name = "dailyRadioButton";
            this.dailyRadioButton.Size = new System.Drawing.Size(82, 17);
            this.dailyRadioButton.TabIndex = 9;
            this.dailyRadioButton.Text = "Ежедневно";
            this.dailyRadioButton.UseVisualStyleBackColor = true;
            // 
            // onceRadioButton
            // 
            this.onceRadioButton.AutoSize = true;
            this.onceRadioButton.Checked = true;
            this.onceRadioButton.Location = new System.Drawing.Point(38, 48);
            this.onceRadioButton.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.onceRadioButton.Name = "onceRadioButton";
            this.onceRadioButton.Size = new System.Drawing.Size(86, 17);
            this.onceRadioButton.TabIndex = 8;
            this.onceRadioButton.TabStop = true;
            this.onceRadioButton.Text = "Однократно";
            this.onceRadioButton.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 25);
            this.label7.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(188, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Когда вы хотите запускать задачу?";
            // 
            // buttonsPanel
            // 
            this.buttonsPanel.Controls.Add(this.prevButton);
            this.buttonsPanel.Controls.Add(this.nextButton);
            this.buttonsPanel.Controls.Add(this.cancelButton);
            this.buttonsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonsPanel.Location = new System.Drawing.Point(0, 348);
            this.buttonsPanel.Margin = new System.Windows.Forms.Padding(2);
            this.buttonsPanel.Name = "buttonsPanel";
            this.buttonsPanel.Size = new System.Drawing.Size(534, 42);
            this.buttonsPanel.TabIndex = 12;
            // 
            // prevButton
            // 
            this.prevButton.Location = new System.Drawing.Point(282, 10);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(75, 23);
            this.prevButton.TabIndex = 4;
            this.prevButton.Text = "< Назад";
            this.prevButton.UseVisualStyleBackColor = true;
            this.prevButton.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(363, 10);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 5;
            this.nextButton.Text = "Далее >";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(444, 10);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // leftPanel
            // 
            this.leftPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.leftPanel.Controls.Add(this.finishLabel);
            this.leftPanel.Controls.Add(this.triggerLabel);
            this.leftPanel.Controls.Add(this.chooseTriggerLabel);
            this.leftPanel.Controls.Add(this.panel2);
            this.leftPanel.Controls.Add(this.chooseNameLabel);
            this.leftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.leftPanel.Location = new System.Drawing.Point(0, 100);
            this.leftPanel.Margin = new System.Windows.Forms.Padding(0);
            this.leftPanel.Name = "leftPanel";
            this.leftPanel.Size = new System.Drawing.Size(200, 390);
            this.leftPanel.TabIndex = 17;
            // 
            // finishLabel
            // 
            this.finishLabel.AutoSize = true;
            this.finishLabel.BackColor = System.Drawing.Color.Transparent;
            this.finishLabel.Location = new System.Drawing.Point(12, 82);
            this.finishLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.finishLabel.Name = "finishLabel";
            this.finishLabel.Size = new System.Drawing.Size(64, 13);
            this.finishLabel.TabIndex = 9;
            this.finishLabel.Text = "Зарешение";
            // 
            // triggerLabel
            // 
            this.triggerLabel.AutoSize = true;
            this.triggerLabel.BackColor = System.Drawing.Color.Transparent;
            this.triggerLabel.Location = new System.Drawing.Point(20, 59);
            this.triggerLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.triggerLabel.Name = "triggerLabel";
            this.triggerLabel.Size = new System.Drawing.Size(48, 13);
            this.triggerLabel.TabIndex = 7;
            this.triggerLabel.Text = "Триггер";
            // 
            // chooseTriggerLabel
            // 
            this.chooseTriggerLabel.AutoSize = true;
            this.chooseTriggerLabel.BackColor = System.Drawing.Color.Transparent;
            this.chooseTriggerLabel.Location = new System.Drawing.Point(12, 36);
            this.chooseTriggerLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.chooseTriggerLabel.Name = "chooseTriggerLabel";
            this.chooseTriggerLabel.Size = new System.Drawing.Size(99, 13);
            this.chooseTriggerLabel.TabIndex = 6;
            this.chooseTriggerLabel.Text = "Выберите триггер";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(192, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(2, 399);
            this.panel2.TabIndex = 5;
            this.panel2.Visible = false;
            // 
            // chooseNameLabel
            // 
            this.chooseNameLabel.AutoSize = true;
            this.chooseNameLabel.BackColor = System.Drawing.Color.Transparent;
            this.chooseNameLabel.Location = new System.Drawing.Point(12, 13);
            this.chooseNameLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.chooseNameLabel.Name = "chooseNameLabel";
            this.chooseNameLabel.Size = new System.Drawing.Size(80, 13);
            this.chooseNameLabel.TabIndex = 4;
            this.chooseNameLabel.Text = "Выберите имя";
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(36, 101);
            this.label18.Margin = new System.Windows.Forms.Padding(25, 15, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(51, 13);
            this.label18.TabIndex = 61;
            this.label18.Text = "Месяцы:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Planner.Properties.Resources.paperPlane;
            this.pictureBox1.Location = new System.Drawing.Point(9, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(120, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // monthPicker1
            // 
            this.monthPicker1.Location = new System.Drawing.Point(96, 98);
            this.monthPicker1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.monthPicker1.Name = "monthPicker1";
            this.monthPicker1.Size = new System.Drawing.Size(266, 20);
            this.monthPicker1.TabIndex = 60;
            // 
            // dayOfMonthPicker1
            // 
            this.dayOfMonthPicker1.Location = new System.Drawing.Point(96, 173);
            this.dayOfMonthPicker1.MaximumSize = new System.Drawing.Size(0, 169);
            this.dayOfMonthPicker1.MinimumSize = new System.Drawing.Size(373, 0);
            this.dayOfMonthPicker1.Name = "dayOfMonthPicker1";
            this.dayOfMonthPicker1.Size = new System.Drawing.Size(373, 20);
            this.dayOfMonthPicker1.TabIndex = 59;
            // 
            // weekDayPicker1
            // 
            this.weekDayPicker1.Location = new System.Drawing.Point(255, 219);
            this.weekDayPicker1.Name = "weekDayPicker1";
            this.weekDayPicker1.Size = new System.Drawing.Size(214, 20);
            this.weekDayPicker1.TabIndex = 63;
            // 
            // everyDayPicker1
            // 
            this.everyDayPicker1.Location = new System.Drawing.Point(122, 219);
            this.everyDayPicker1.Name = "everyDayPicker1";
            this.everyDayPicker1.Size = new System.Drawing.Size(127, 20);
            this.everyDayPicker1.TabIndex = 62;
            // 
            // NewScheduleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(699, 490);
            this.Controls.Add(this.rightPanel);
            this.Controls.Add(this.leftPanel);
            this.Controls.Add(this.topPanel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewScheduleForm";
            this.Text = "Planner: Добавить расписание";
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.rightPanel.ResumeLayout(false);
            this.contentPanel.ResumeLayout(false);
            this.monthlyTriggeringPanel.ResumeLayout(false);
            this.monthlyTriggeringPanel.PerformLayout();
            this.dailyTriggeringPanel.ResumeLayout(false);
            this.dailyTriggeringPanel.PerformLayout();
            this.onceTriggeringPanel.ResumeLayout(false);
            this.onceTriggeringPanel.PerformLayout();
            this.nameDescriptionPanel.ResumeLayout(false);
            this.nameDescriptionPanel.PerformLayout();
            this.addSchedFinishPanel.ResumeLayout(false);
            this.addSchedFinishPanel.PerformLayout();
            this.weeklyTriggeringPanel.ResumeLayout(false);
            this.weeklyTriggeringPanel.PerformLayout();
            this.triggersPanel.ResumeLayout(false);
            this.triggersPanel.PerformLayout();
            this.buttonsPanel.ResumeLayout(false);
            this.leftPanel.ResumeLayout(false);
            this.leftPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel rightPanel;
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.Panel dailyTriggeringPanel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox everyDayTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel weeklyTriggeringPanel;
        private System.Windows.Forms.CheckBox allCheckBox;
        private System.Windows.Forms.CheckBox thursdayCheckBox;
        private System.Windows.Forms.CheckBox sundayCheckBox;
        private System.Windows.Forms.CheckBox saturdayCheckBox;
        private System.Windows.Forms.CheckBox fridayCheckBox;
        private System.Windows.Forms.CheckBox wednesdayCheckBox;
        private System.Windows.Forms.CheckBox tuesdayCheckBox;
        private System.Windows.Forms.CheckBox mondayCheckBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox everyWeekTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker weeklyDatePicker;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel triggersPanel;
        private System.Windows.Forms.RadioButton monthlyRadioButton;
        private System.Windows.Forms.RadioButton weeklyRadioButton;
        private System.Windows.Forms.RadioButton dailyRadioButton;
        private System.Windows.Forms.RadioButton onceRadioButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel addSchedFinishPanel;
        private System.Windows.Forms.TextBox finalMoreTextBox;
        private System.Windows.Forms.TextBox finalTriggerTextBox;
        private System.Windows.Forms.TextBox finalDescriptionTextBox;
        private System.Windows.Forms.TextBox finalNameTextBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel monthlyTriggeringPanel;
        private System.Windows.Forms.Label monthLabel;
        private System.Windows.Forms.Label everyLabel;
        private System.Windows.Forms.Label daysLabel;
        private System.Windows.Forms.RadioButton inDaysRadioButton;
        private System.Windows.Forms.RadioButton inNumbersRadioButton;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker monthlyTimePicker;
        private System.Windows.Forms.DateTimePicker monthlyDatePicker;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel onceTriggeringPanel;
        private System.Windows.Forms.DateTimePicker onceTimePicker;
        private System.Windows.Forms.DateTimePicker onceDatePicker;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel nameDescriptionPanel;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Panel buttonsPanel;
        private System.Windows.Forms.Button prevButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Panel leftPanel;
        private System.Windows.Forms.Label finishLabel;
        private System.Windows.Forms.Label triggerLabel;
        private System.Windows.Forms.Label chooseTriggerLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label chooseNameLabel;
        private System.Windows.Forms.DateTimePicker weeklyTimePicker;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DateTimePicker dailyTimePicker;
        private System.Windows.Forms.DateTimePicker dailyDatePicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker weeklyEndDatePicker;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DateTimePicker dailyEndDatePicker;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DateTimePicker monthlyEndDatePicker;
        private System.Windows.Forms.Label label29;
        private DayOfMonthPicker dayOfMonthPicker1;
        private MonthPicker monthPicker1;
        private System.Windows.Forms.Label label18;
        private EveryDayPicker everyDayPicker1;
        private WeekDayPicker weekDayPicker1;
    }
}