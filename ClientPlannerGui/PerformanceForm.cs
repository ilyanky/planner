﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Planner
{
    public partial class PerformanceForm : Form
    {
        private double _count = 0;

        public PerformanceForm()
        {
            InitializeComponent();
        }


        public void AddPoint(double x, double y)
        {
            //chart1.Series[0].Points.AddXY(_count++, _client.CpuPerformance / 4);

            //chart1.Series[0].Points.Clear();

            //cpuPerformanceTextBox.Text += (" " + (int)(_client.CpuPerformance / 4) + " ");
            //chart1.Series[0].Points.AddXY(1, _client.CpuPerformance / 4);
        }


        public void AddPoint(double y)
        {
            chart1.Series[0].Points.AddXY(_count++, y);
        }


        private void PerformanceForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _count = 0;
            chart1.Series[0].Points.Clear();
        }
    }
}
