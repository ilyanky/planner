﻿namespace Planner
{
    partial class AddTaskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && (components != null) ) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nameLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.leftPanel = new System.Windows.Forms.Panel();
            this.finishLabel = new System.Windows.Forms.Label();
            this.chooseTaskTypeLabel = new System.Windows.Forms.Label();
            this.addTriggerLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chooseNameLabel = new System.Windows.Forms.Label();
            this.rightPanel = new System.Windows.Forms.Panel();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.createTriggerPanel = new System.Windows.Forms.Panel();
            this.createScheduleButton = new System.Windows.Forms.Button();
            this.schedListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.deleteSchedButton = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.addTaskFinishPanel = new System.Windows.Forms.Panel();
            this.finalTaskTypeTextBox = new System.Windows.Forms.TextBox();
            this.finalTriggerTextBox = new System.Windows.Forms.TextBox();
            this.finalDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.finalNameTextBox = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.nameDescriptionPanel = new System.Windows.Forms.Panel();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.taskNameTextBox = new System.Windows.Forms.TextBox();
            this.taskTypesPanel = new System.Windows.Forms.Panel();
            this.taskTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.buttonsPanel = new System.Windows.Forms.Panel();
            this.dividerPanel = new System.Windows.Forms.Panel();
            this.prevButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.leftPanel.SuspendLayout();
            this.rightPanel.SuspendLayout();
            this.contentPanel.SuspendLayout();
            this.createTriggerPanel.SuspendLayout();
            this.addTaskFinishPanel.SuspendLayout();
            this.nameDescriptionPanel.SuspendLayout();
            this.taskTypesPanel.SuspendLayout();
            this.buttonsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.Color.Transparent;
            this.topPanel.Controls.Add(this.panel1);
            this.topPanel.Controls.Add(this.nameLabel);
            this.topPanel.Controls.Add(this.pictureBox1);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Margin = new System.Windows.Forms.Padding(0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(699, 100);
            this.topPanel.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(476, 530);
            this.panel1.TabIndex = 0;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameLabel.ForeColor = System.Drawing.Color.Black;
            this.nameLabel.Location = new System.Drawing.Point(138, 27);
            this.nameLabel.Margin = new System.Windows.Forms.Padding(0);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(305, 39);
            this.nameLabel.TabIndex = 1;
            this.nameLabel.Text = "Добавить задачу";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Planner.Properties.Resources.paperPlane;
            this.pictureBox1.Location = new System.Drawing.Point(9, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(120, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // leftPanel
            // 
            this.leftPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.leftPanel.Controls.Add(this.finishLabel);
            this.leftPanel.Controls.Add(this.chooseTaskTypeLabel);
            this.leftPanel.Controls.Add(this.addTriggerLabel);
            this.leftPanel.Controls.Add(this.panel2);
            this.leftPanel.Controls.Add(this.chooseNameLabel);
            this.leftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.leftPanel.Location = new System.Drawing.Point(0, 100);
            this.leftPanel.Margin = new System.Windows.Forms.Padding(0);
            this.leftPanel.Name = "leftPanel";
            this.leftPanel.Size = new System.Drawing.Size(200, 390);
            this.leftPanel.TabIndex = 2;
            // 
            // finishLabel
            // 
            this.finishLabel.AutoSize = true;
            this.finishLabel.BackColor = System.Drawing.Color.Transparent;
            this.finishLabel.Location = new System.Drawing.Point(12, 82);
            this.finishLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.finishLabel.Name = "finishLabel";
            this.finishLabel.Size = new System.Drawing.Size(43, 13);
            this.finishLabel.TabIndex = 9;
            this.finishLabel.Text = "Запуск";
            // 
            // chooseTaskTypeLabel
            // 
            this.chooseTaskTypeLabel.AutoSize = true;
            this.chooseTaskTypeLabel.BackColor = System.Drawing.Color.Transparent;
            this.chooseTaskTypeLabel.Location = new System.Drawing.Point(12, 59);
            this.chooseTaskTypeLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.chooseTaskTypeLabel.Name = "chooseTaskTypeLabel";
            this.chooseTaskTypeLabel.Size = new System.Drawing.Size(115, 13);
            this.chooseTaskTypeLabel.TabIndex = 8;
            this.chooseTaskTypeLabel.Text = "Выберите тип задачи";
            // 
            // addTriggerLabel
            // 
            this.addTriggerLabel.AutoSize = true;
            this.addTriggerLabel.BackColor = System.Drawing.Color.Transparent;
            this.addTriggerLabel.Location = new System.Drawing.Point(12, 36);
            this.addTriggerLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.addTriggerLabel.Name = "addTriggerLabel";
            this.addTriggerLabel.Size = new System.Drawing.Size(99, 13);
            this.addTriggerLabel.TabIndex = 6;
            this.addTriggerLabel.Text = "Добавьте триггер";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(192, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(2, 399);
            this.panel2.TabIndex = 5;
            this.panel2.Visible = false;
            // 
            // chooseNameLabel
            // 
            this.chooseNameLabel.AutoSize = true;
            this.chooseNameLabel.BackColor = System.Drawing.Color.Transparent;
            this.chooseNameLabel.Location = new System.Drawing.Point(12, 13);
            this.chooseNameLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.chooseNameLabel.Name = "chooseNameLabel";
            this.chooseNameLabel.Size = new System.Drawing.Size(80, 13);
            this.chooseNameLabel.TabIndex = 4;
            this.chooseNameLabel.Text = "Выберите имя";
            // 
            // rightPanel
            // 
            this.rightPanel.BackColor = System.Drawing.Color.White;
            this.rightPanel.Controls.Add(this.contentPanel);
            this.rightPanel.Controls.Add(this.buttonsPanel);
            this.rightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.rightPanel.Location = new System.Drawing.Point(165, 100);
            this.rightPanel.Margin = new System.Windows.Forms.Padding(0);
            this.rightPanel.Name = "rightPanel";
            this.rightPanel.Size = new System.Drawing.Size(534, 390);
            this.rightPanel.TabIndex = 3;
            // 
            // contentPanel
            // 
            this.contentPanel.Controls.Add(this.createTriggerPanel);
            this.contentPanel.Controls.Add(this.addTaskFinishPanel);
            this.contentPanel.Controls.Add(this.nameDescriptionPanel);
            this.contentPanel.Controls.Add(this.taskTypesPanel);
            this.contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentPanel.Location = new System.Drawing.Point(0, 0);
            this.contentPanel.Margin = new System.Windows.Forms.Padding(2);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(534, 348);
            this.contentPanel.TabIndex = 12;
            // 
            // createTriggerPanel
            // 
            this.createTriggerPanel.Controls.Add(this.createScheduleButton);
            this.createTriggerPanel.Controls.Add(this.schedListView);
            this.createTriggerPanel.Controls.Add(this.deleteSchedButton);
            this.createTriggerPanel.Controls.Add(this.label27);
            this.createTriggerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createTriggerPanel.Location = new System.Drawing.Point(0, 0);
            this.createTriggerPanel.Name = "createTriggerPanel";
            this.createTriggerPanel.Size = new System.Drawing.Size(534, 348);
            this.createTriggerPanel.TabIndex = 14;
            this.createTriggerPanel.Visible = false;
            // 
            // createScheduleButton
            // 
            this.createScheduleButton.Location = new System.Drawing.Point(17, 315);
            this.createScheduleButton.Name = "createScheduleButton";
            this.createScheduleButton.Size = new System.Drawing.Size(75, 23);
            this.createScheduleButton.TabIndex = 7;
            this.createScheduleButton.Text = "Создать...";
            this.createScheduleButton.UseVisualStyleBackColor = true;
            this.createScheduleButton.Click += new System.EventHandler(this.createScheduleButton_Click);
            // 
            // schedListView
            // 
            this.schedListView.CheckBoxes = true;
            this.schedListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader5,
            this.columnHeader4});
            this.schedListView.FullRowSelect = true;
            this.schedListView.HideSelection = false;
            this.schedListView.Location = new System.Drawing.Point(39, 52);
            this.schedListView.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.schedListView.Name = "schedListView";
            this.schedListView.Size = new System.Drawing.Size(470, 253);
            this.schedListView.TabIndex = 8;
            this.schedListView.UseCompatibleStateImageBehavior = false;
            this.schedListView.View = System.Windows.Forms.View.Details;
            this.schedListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.schedListView_ItemChecked);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Рассписание";
            this.columnHeader1.Width = 101;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Описание";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Триггер";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Состояние";
            this.columnHeader5.Width = 118;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Подрбности";
            this.columnHeader4.Width = 95;
            // 
            // deleteSchedButton
            // 
            this.deleteSchedButton.Location = new System.Drawing.Point(98, 315);
            this.deleteSchedButton.Name = "deleteSchedButton";
            this.deleteSchedButton.Size = new System.Drawing.Size(75, 23);
            this.deleteSchedButton.TabIndex = 9;
            this.deleteSchedButton.Text = "Удалить";
            this.deleteSchedButton.UseVisualStyleBackColor = true;
            this.deleteSchedButton.Click += new System.EventHandler(this.deleteSchedButton_Click);
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(25, 25);
            this.label27.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(173, 13);
            this.label27.TabIndex = 7;
            this.label27.Text = "Выберите время и дату запуска:";
            // 
            // addTaskFinishPanel
            // 
            this.addTaskFinishPanel.Controls.Add(this.finalTaskTypeTextBox);
            this.addTaskFinishPanel.Controls.Add(this.finalTriggerTextBox);
            this.addTaskFinishPanel.Controls.Add(this.finalDescriptionTextBox);
            this.addTaskFinishPanel.Controls.Add(this.finalNameTextBox);
            this.addTaskFinishPanel.Controls.Add(this.label25);
            this.addTaskFinishPanel.Controls.Add(this.label24);
            this.addTaskFinishPanel.Controls.Add(this.label23);
            this.addTaskFinishPanel.Controls.Add(this.label22);
            this.addTaskFinishPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addTaskFinishPanel.Location = new System.Drawing.Point(0, 0);
            this.addTaskFinishPanel.Name = "addTaskFinishPanel";
            this.addTaskFinishPanel.Size = new System.Drawing.Size(534, 348);
            this.addTaskFinishPanel.TabIndex = 32;
            this.addTaskFinishPanel.Visible = false;
            // 
            // finalTaskTypeTextBox
            // 
            this.finalTaskTypeTextBox.Location = new System.Drawing.Point(105, 267);
            this.finalTaskTypeTextBox.Name = "finalTaskTypeTextBox";
            this.finalTaskTypeTextBox.ReadOnly = true;
            this.finalTaskTypeTextBox.Size = new System.Drawing.Size(395, 20);
            this.finalTaskTypeTextBox.TabIndex = 7;
            // 
            // finalTriggerTextBox
            // 
            this.finalTriggerTextBox.Location = new System.Drawing.Point(105, 160);
            this.finalTriggerTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 25);
            this.finalTriggerTextBox.Multiline = true;
            this.finalTriggerTextBox.Name = "finalTriggerTextBox";
            this.finalTriggerTextBox.ReadOnly = true;
            this.finalTriggerTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.finalTriggerTextBox.Size = new System.Drawing.Size(395, 79);
            this.finalTriggerTextBox.TabIndex = 6;
            // 
            // finalDescriptionTextBox
            // 
            this.finalDescriptionTextBox.Location = new System.Drawing.Point(105, 60);
            this.finalDescriptionTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 25, 25);
            this.finalDescriptionTextBox.Multiline = true;
            this.finalDescriptionTextBox.Name = "finalDescriptionTextBox";
            this.finalDescriptionTextBox.ReadOnly = true;
            this.finalDescriptionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.finalDescriptionTextBox.Size = new System.Drawing.Size(395, 72);
            this.finalDescriptionTextBox.TabIndex = 5;
            this.finalDescriptionTextBox.Text = " ";
            // 
            // finalNameTextBox
            // 
            this.finalNameTextBox.Location = new System.Drawing.Point(105, 22);
            this.finalNameTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.finalNameTextBox.Name = "finalNameTextBox";
            this.finalNameTextBox.ReadOnly = true;
            this.finalNameTextBox.Size = new System.Drawing.Size(395, 20);
            this.finalNameTextBox.TabIndex = 4;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(24, 270);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "Тип задачи:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(25, 163);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(51, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Триггер:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(25, 63);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Описание:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(25, 25);
            this.label22.Margin = new System.Windows.Forms.Padding(25, 25, 5, 25);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Имя:";
            // 
            // nameDescriptionPanel
            // 
            this.nameDescriptionPanel.Controls.Add(this.descriptionTextBox);
            this.nameDescriptionPanel.Controls.Add(this.label2);
            this.nameDescriptionPanel.Controls.Add(this.label1);
            this.nameDescriptionPanel.Controls.Add(this.taskNameTextBox);
            this.nameDescriptionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nameDescriptionPanel.Location = new System.Drawing.Point(0, 0);
            this.nameDescriptionPanel.Name = "nameDescriptionPanel";
            this.nameDescriptionPanel.Size = new System.Drawing.Size(534, 348);
            this.nameDescriptionPanel.TabIndex = 0;
            this.nameDescriptionPanel.Visible = false;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.descriptionTextBox.Location = new System.Drawing.Point(105, 63);
            this.descriptionTextBox.Margin = new System.Windows.Forms.Padding(5, 3, 15, 3);
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(405, 194);
            this.descriptionTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Описание:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Имя задачи:";
            // 
            // taskNameTextBox
            // 
            this.taskNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.taskNameTextBox.Location = new System.Drawing.Point(105, 22);
            this.taskNameTextBox.Margin = new System.Windows.Forms.Padding(5, 3, 15, 3);
            this.taskNameTextBox.Name = "taskNameTextBox";
            this.taskNameTextBox.Size = new System.Drawing.Size(405, 20);
            this.taskNameTextBox.TabIndex = 0;
            // 
            // taskTypesPanel
            // 
            this.taskTypesPanel.Controls.Add(this.taskTypeComboBox);
            this.taskTypesPanel.Controls.Add(this.label21);
            this.taskTypesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taskTypesPanel.Location = new System.Drawing.Point(0, 0);
            this.taskTypesPanel.Name = "taskTypesPanel";
            this.taskTypesPanel.Size = new System.Drawing.Size(534, 348);
            this.taskTypesPanel.TabIndex = 45;
            this.taskTypesPanel.Visible = false;
            // 
            // taskTypeComboBox
            // 
            this.taskTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.taskTypeComboBox.FormattingEnabled = true;
            this.taskTypeComboBox.Location = new System.Drawing.Point(47, 54);
            this.taskTypeComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.taskTypeComboBox.Name = "taskTypeComboBox";
            this.taskTypeComboBox.Size = new System.Drawing.Size(463, 21);
            this.taskTypeComboBox.TabIndex = 31;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(25, 25);
            this.label21.Margin = new System.Windows.Forms.Padding(25, 25, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(118, 13);
            this.label21.TabIndex = 30;
            this.label21.Text = "Выберите тип задачи:";
            // 
            // buttonsPanel
            // 
            this.buttonsPanel.Controls.Add(this.dividerPanel);
            this.buttonsPanel.Controls.Add(this.prevButton);
            this.buttonsPanel.Controls.Add(this.nextButton);
            this.buttonsPanel.Controls.Add(this.cancelButton);
            this.buttonsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonsPanel.Location = new System.Drawing.Point(0, 348);
            this.buttonsPanel.Margin = new System.Windows.Forms.Padding(2);
            this.buttonsPanel.Name = "buttonsPanel";
            this.buttonsPanel.Size = new System.Drawing.Size(534, 42);
            this.buttonsPanel.TabIndex = 12;
            // 
            // dividerPanel
            // 
            this.dividerPanel.BackColor = System.Drawing.Color.Gainsboro;
            this.dividerPanel.Location = new System.Drawing.Point(3, 0);
            this.dividerPanel.Name = "dividerPanel";
            this.dividerPanel.Size = new System.Drawing.Size(528, 1);
            this.dividerPanel.TabIndex = 7;
            // 
            // prevButton
            // 
            this.prevButton.Location = new System.Drawing.Point(282, 10);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(75, 23);
            this.prevButton.TabIndex = 4;
            this.prevButton.Text = "< Назад";
            this.prevButton.UseVisualStyleBackColor = true;
            this.prevButton.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(363, 10);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 5;
            this.nextButton.Text = "Далее >";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(444, 10);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // AddTaskForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(699, 490);
            this.Controls.Add(this.rightPanel);
            this.Controls.Add(this.leftPanel);
            this.Controls.Add(this.topPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddTaskForm";
            this.Text = "Planner: Дабвить задачу";
            this.Resize += new System.EventHandler(this.AddTaskForm_Resize);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.leftPanel.ResumeLayout(false);
            this.leftPanel.PerformLayout();
            this.rightPanel.ResumeLayout(false);
            this.contentPanel.ResumeLayout(false);
            this.createTriggerPanel.ResumeLayout(false);
            this.createTriggerPanel.PerformLayout();
            this.addTaskFinishPanel.ResumeLayout(false);
            this.addTaskFinishPanel.PerformLayout();
            this.nameDescriptionPanel.ResumeLayout(false);
            this.nameDescriptionPanel.PerformLayout();
            this.taskTypesPanel.ResumeLayout(false);
            this.taskTypesPanel.PerformLayout();
            this.buttonsPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel leftPanel;
        private System.Windows.Forms.Panel rightPanel;
        private System.Windows.Forms.Panel nameDescriptionPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox taskNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.Label chooseNameLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label addTriggerLabel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button prevButton;
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.Panel buttonsPanel;
        private System.Windows.Forms.Label chooseTaskTypeLabel;
        private System.Windows.Forms.Panel taskTypesPanel;
        private System.Windows.Forms.ComboBox taskTypeComboBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label finishLabel;
        private System.Windows.Forms.Panel addTaskFinishPanel;
        private System.Windows.Forms.TextBox finalTaskTypeTextBox;
        private System.Windows.Forms.TextBox finalTriggerTextBox;
        private System.Windows.Forms.TextBox finalDescriptionTextBox;
        private System.Windows.Forms.TextBox finalNameTextBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel createTriggerPanel;
        private System.Windows.Forms.Button createScheduleButton;
        private System.Windows.Forms.ListView schedListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button deleteSchedButton;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel dividerPanel;
    }
}