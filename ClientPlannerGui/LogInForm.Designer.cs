﻿namespace Planner
{
    partial class LogInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nameLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.minLogInPanel = new System.Windows.Forms.Panel();
            this.minPswdTextBox = new System.Windows.Forms.TextBox();
            this.minLoginTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.logInButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.separatorPanel = new System.Windows.Forms.Panel();
            this.paramsButton = new System.Windows.Forms.Button();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.maxLogInPanel = new System.Windows.Forms.Panel();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.maxLoginTextBox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.dnsTextBox = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.maxPswdTextBox = new System.Windows.Forms.TextBox();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.minLogInPanel.SuspendLayout();
            this.contentPanel.SuspendLayout();
            this.maxLogInPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.Color.Transparent;
            this.topPanel.Controls.Add(this.panel1);
            this.topPanel.Controls.Add(this.nameLabel);
            this.topPanel.Controls.Add(this.pictureBox1);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Margin = new System.Windows.Forms.Padding(0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(476, 100);
            this.topPanel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(476, 530);
            this.panel1.TabIndex = 0;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameLabel.ForeColor = System.Drawing.Color.Black;
            this.nameLabel.Location = new System.Drawing.Point(140, 11);
            this.nameLabel.Margin = new System.Windows.Forms.Padding(0);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(269, 76);
            this.nameLabel.TabIndex = 1;
            this.nameLabel.Text = "Planner";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Planner.Properties.Resources.paperPlane;
            this.pictureBox1.Location = new System.Drawing.Point(9, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(120, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // minLogInPanel
            // 
            this.minLogInPanel.BackColor = System.Drawing.Color.Transparent;
            this.minLogInPanel.Controls.Add(this.minPswdTextBox);
            this.minLogInPanel.Controls.Add(this.minLoginTextBox);
            this.minLogInPanel.Controls.Add(this.label2);
            this.minLogInPanel.Controls.Add(this.label1);
            this.minLogInPanel.Location = new System.Drawing.Point(0, 0);
            this.minLogInPanel.Margin = new System.Windows.Forms.Padding(0);
            this.minLogInPanel.Name = "minLogInPanel";
            this.minLogInPanel.Size = new System.Drawing.Size(476, 106);
            this.minLogInPanel.TabIndex = 1;
            // 
            // minPswdTextBox
            // 
            this.minPswdTextBox.Location = new System.Drawing.Point(142, 65);
            this.minPswdTextBox.Name = "minPswdTextBox";
            this.minPswdTextBox.Size = new System.Drawing.Size(176, 20);
            this.minPswdTextBox.TabIndex = 3;
            this.minPswdTextBox.UseSystemPasswordChar = true;
            // 
            // minLoginTextBox
            // 
            this.minLoginTextBox.Location = new System.Drawing.Point(142, 23);
            this.minLoginTextBox.Name = "minLoginTextBox";
            this.minLoginTextBox.Size = new System.Drawing.Size(176, 20);
            this.minLoginTextBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(46, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Логин:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(46, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Пароль:";
            // 
            // logInButton
            // 
            this.logInButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logInButton.Location = new System.Drawing.Point(151, 22);
            this.logInButton.Margin = new System.Windows.Forms.Padding(0, 15, 15, 15);
            this.logInButton.Name = "logInButton";
            this.logInButton.Size = new System.Drawing.Size(82, 24);
            this.logInButton.TabIndex = 4;
            this.logInButton.Text = "Вход";
            this.logInButton.UseVisualStyleBackColor = true;
            this.logInButton.Click += new System.EventHandler(this.logInButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(248, 22);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(0, 15, 15, 15);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(82, 24);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // separatorPanel
            // 
            this.separatorPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.separatorPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.separatorPanel.Location = new System.Drawing.Point(15, 3);
            this.separatorPanel.Name = "separatorPanel";
            this.separatorPanel.Size = new System.Drawing.Size(449, 1);
            this.separatorPanel.TabIndex = 6;
            // 
            // paramsButton
            // 
            this.paramsButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.paramsButton.Location = new System.Drawing.Point(345, 22);
            this.paramsButton.Margin = new System.Windows.Forms.Padding(0, 15, 15, 15);
            this.paramsButton.Name = "paramsButton";
            this.paramsButton.Size = new System.Drawing.Size(107, 24);
            this.paramsButton.TabIndex = 7;
            this.paramsButton.Text = "Параметры >>";
            this.paramsButton.UseVisualStyleBackColor = true;
            this.paramsButton.Click += new System.EventHandler(this.paramsButton_Click);
            // 
            // contentPanel
            // 
            this.contentPanel.BackColor = System.Drawing.Color.Transparent;
            this.contentPanel.Controls.Add(this.minLogInPanel);
            this.contentPanel.Controls.Add(this.maxLogInPanel);
            this.contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentPanel.Location = new System.Drawing.Point(0, 100);
            this.contentPanel.Margin = new System.Windows.Forms.Padding(0);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(476, 530);
            this.contentPanel.TabIndex = 2;
            // 
            // maxLogInPanel
            // 
            this.maxLogInPanel.Controls.Add(this.portTextBox);
            this.maxLogInPanel.Controls.Add(this.label6);
            this.maxLogInPanel.Controls.Add(this.maxLoginTextBox);
            this.maxLogInPanel.Controls.Add(this.button2);
            this.maxLogInPanel.Controls.Add(this.dnsTextBox);
            this.maxLogInPanel.Controls.Add(this.panel3);
            this.maxLogInPanel.Controls.Add(this.button3);
            this.maxLogInPanel.Controls.Add(this.label4);
            this.maxLogInPanel.Controls.Add(this.button4);
            this.maxLogInPanel.Controls.Add(this.label5);
            this.maxLogInPanel.Controls.Add(this.label3);
            this.maxLogInPanel.Controls.Add(this.maxPswdTextBox);
            this.maxLogInPanel.Location = new System.Drawing.Point(0, 0);
            this.maxLogInPanel.Margin = new System.Windows.Forms.Padding(0);
            this.maxLogInPanel.Name = "maxLogInPanel";
            this.maxLogInPanel.Size = new System.Drawing.Size(476, 199);
            this.maxLogInPanel.TabIndex = 18;
            this.maxLogInPanel.Visible = false;
            // 
            // portTextBox
            // 
            this.portTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.portTextBox.Location = new System.Drawing.Point(142, 149);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(176, 20);
            this.portTextBox.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(46, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 17);
            this.label6.TabIndex = 18;
            this.label6.Text = "Порт:";
            // 
            // maxLoginTextBox
            // 
            this.maxLoginTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.maxLoginTextBox.Location = new System.Drawing.Point(142, 21);
            this.maxLoginTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 20);
            this.maxLoginTextBox.Name = "maxLoginTextBox";
            this.maxLoginTextBox.Size = new System.Drawing.Size(176, 20);
            this.maxLoginTextBox.TabIndex = 10;
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button2.Location = new System.Drawing.Point(345, 326);
            this.button2.Margin = new System.Windows.Forms.Padding(0, 5, 15, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(107, 24);
            this.button2.TabIndex = 15;
            this.button2.Text = "Параметры >>";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // dnsTextBox
            // 
            this.dnsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dnsTextBox.Location = new System.Drawing.Point(142, 107);
            this.dnsTextBox.Name = "dnsTextBox";
            this.dnsTextBox.Size = new System.Drawing.Size(176, 20);
            this.dnsTextBox.TabIndex = 17;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.Location = new System.Drawing.Point(12, 483);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(455, 1);
            this.panel3.TabIndex = 14;
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button3.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button3.Location = new System.Drawing.Point(248, 326);
            this.button3.Margin = new System.Windows.Forms.Padding(0, 5, 15, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(82, 24);
            this.button3.TabIndex = 13;
            this.button3.Text = "Отмена";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(46, 65);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 0, 3, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Пароль:";
            // 
            // button4
            // 
            this.button4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button4.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button4.Location = new System.Drawing.Point(151, 326);
            this.button4.Margin = new System.Windows.Forms.Padding(0, 5, 15, 5);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(82, 24);
            this.button4.TabIndex = 12;
            this.button4.Text = "Вход";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(46, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "Dns:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(46, 22);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Логин:";
            // 
            // maxPswdTextBox
            // 
            this.maxPswdTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.maxPswdTextBox.Location = new System.Drawing.Point(142, 64);
            this.maxPswdTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 20);
            this.maxPswdTextBox.Name = "maxPswdTextBox";
            this.maxPswdTextBox.Size = new System.Drawing.Size(176, 20);
            this.maxPswdTextBox.TabIndex = 11;
            this.maxPswdTextBox.UseSystemPasswordChar = true;
            // 
            // bottomPanel
            // 
            this.bottomPanel.BackColor = System.Drawing.Color.Transparent;
            this.bottomPanel.Controls.Add(this.paramsButton);
            this.bottomPanel.Controls.Add(this.separatorPanel);
            this.bottomPanel.Controls.Add(this.logInButton);
            this.bottomPanel.Controls.Add(this.cancelButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 569);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(476, 61);
            this.bottomPanel.TabIndex = 3;
            // 
            // LogInForm
            // 
            this.AcceptButton = this.logInButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(476, 630);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.contentPanel);
            this.Controls.Add(this.topPanel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LogInForm";
            this.Text = "Planner: Авторизация";
            this.Load += new System.EventHandler(this.LogInForm_Load);
            this.Resize += new System.EventHandler(this.LogInForm_Resize);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.minLogInPanel.ResumeLayout(false);
            this.minLogInPanel.PerformLayout();
            this.contentPanel.ResumeLayout(false);
            this.maxLogInPanel.ResumeLayout(false);
            this.maxLogInPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel minLogInPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button logInButton;
        private System.Windows.Forms.TextBox minPswdTextBox;
        private System.Windows.Forms.TextBox minLoginTextBox;
        private System.Windows.Forms.Panel separatorPanel;
        private System.Windows.Forms.Button paramsButton;
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox maxPswdTextBox;
        private System.Windows.Forms.TextBox maxLoginTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dnsTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel maxLogInPanel;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Label label6;
    }
}