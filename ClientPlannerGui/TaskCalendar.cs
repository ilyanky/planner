﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Planner
{
    enum RuDayOfWeek
    {
        Monday = 0,
        Tuesday,
        Wednesday,
        Thursday,
        Friday, 
        Saturday,
        Sunday 
    }

    internal sealed partial class TaskCalendar : UserControl
    {
        enum MonthCheck
        {
            PrevMonth,
            CurrentMonth,
            NextMonth
        }

    // ----------------EventArgs Classes------------------------------------
        public class DaySelectedEventArgs : EventArgs
        {
            public DateTime Date { get; private set; }
            public TaskInfo[] Tasks { get; private set; }
            public TaskInfo[] Statuses { get; private set; }

            public DaySelectedEventArgs(DateTime date, TaskInfo[] tasks,
                TaskInfo[] statuses)
            {
                Date = date;
                Tasks = tasks;
                Statuses = statuses;
            }
        }


        public class DateSpanChangedEventArgs : EventArgs
        {
            public DateTime FromDateTime { get; private set; }
            public DateTime ToDateTime { get; private set; }

            public DateSpanChangedEventArgs(DateTime fromDt,
                DateTime toDt)
            {
                FromDateTime = fromDt;
                ToDateTime = toDt;
            }
        }
    // ----------------EventArgs Classes------------------------------------



    // ----------------Events-----------------------------------------------
        public event EventHandler<DaySelectedEventArgs> DaySelected;
        private void OnDaySelected(DaySelectedEventArgs e)
        {
            var temp = Volatile.Read(ref DaySelected);
            if ( temp != null )
                temp(this, e);
        }

        public event EventHandler<DateSpanChangedEventArgs> DateSpanChanged;
        private void OnDateSpanChanged(DateSpanChangedEventArgs e)
        {
            var temp = Volatile.Read(ref DateSpanChanged);
            if ( temp != null )
                temp(this, e);
        }
    // ----------------Events-----------------------------------------------



        private readonly GregorianCalendar _calendar = 
            new GregorianCalendar();
        private DateTime _currentDate = DateTime.Now;
        public DayCell SelectedCell { get; private set; }
        public DateTime SelectedDate { get; private set; }
        public DateTime FromDateTime { get; private set; }
        public DateTime ToDateTime { get; private set; }


        private Object _lockObj = new Object();


        public TaskCalendar()
        {
            InitializeComponent();

            //InitializeCalendar(DateTime.Now);
        }





        public DateTime GetCurrentDate()
        {
            return _currentDate;
        }


        public void SetTasks(IList<TaskInfo> taskList)
        {
            lock ( _lockObj ) {
                // нужно здесь поправить, чтобы работало не только для дня из текущего месяца
                foreach ( var taskInfo in taskList ) {
                    var dates = taskInfo.Dates;
                    foreach ( var date in dates ) {
                        if ( date.Year != _currentDate.Year ||
                            date.Month != _currentDate.Month )
                            continue;

                        DayCell temp = GetCell(date);
                        if ( !temp.ContainsTask(taskInfo.id) )
                            temp.AddTaskCircle(taskInfo);
                    }
                }
            }
        }


        public void SetStatuses(IList<TaskInfo> taskList)
        {
            lock ( _lockObj ) {
                foreach ( var taskInfo in taskList )
                    GetCell(taskInfo.Dates[0]).SetStatus(taskInfo);
            }
        }


        public void ClearTasks()
        {
            int columns = calendarTableLauout.ColumnCount;
            int rows = calendarTableLauout.RowCount;

            for ( int i = 1; i < rows; ++i ) {
                for ( int j = 0; j < columns; ++j ) {
                    var cell = (DayCell)
                        calendarTableLauout.GetControlFromPosition(j, i);
                    cell.ClearTasks();
                }
            }
        }


        private MonthCheck CheckMonth(DayCell dayCell)
        {
            int daysInMonth = _calendar.GetDaysInMonth(_currentDate.Year,
                _currentDate.Month);
            if ( dayCell.Day > daysInMonth )
                return MonthCheck.PrevMonth;

            DayCell cell = GetCell(new DateTime(_currentDate.Year,
                _currentDate.Month, dayCell.Day));
            if ( !dayCell.Equals(cell) ) {
                // максимальное число, которое может быть в рамках таблицы для следующего месяца
                if ( dayCell.Day < 15 )
                    return MonthCheck.NextMonth;
                // минимальное число, которое может быть в рамках таблицы для предыдущего месяца
                // (февраль, вискосный год)
                else /*if ( dayCell.Day > 22 )*/
                    return MonthCheck.PrevMonth;
            } else
                return MonthCheck.CurrentMonth;
        }


        private void ClearSelectedCell()
        {
            if ( SelectedCell == null )
                return;

            SelectedCell.ClearBackgroud();
            SelectedCell = null;
        }


        private void NextMonth()
        {
            _currentDate = _currentDate.AddMonths(1);
            InitializeCalendar(_currentDate);
            ClearSelectedCell();
        }


        private void PrevMonth()
        {
            _currentDate = _currentDate.AddMonths(-1);
            InitializeCalendar(_currentDate);
            ClearSelectedCell();
        }


        private DayCell GetCell(DateTime date)
        {
            var cellPos = GetCellPos(date);
            return (DayCell)calendarTableLauout.
                GetControlFromPosition(cellPos.Column, cellPos.Row);
        }


        private TableLayoutPanelCellPosition GetCellPos(DateTime date)
        {
            // нужно здесь поправить, чтобы работало не только для дня из текущего месяца

            if ( date.Year != _currentDate.Year )
                ; // кинуть ошибку как-то
            if ( date.Month != _currentDate.Month )
                ; // кинуть ошибку как-то

            int day = date.Day;
            int indent = (int)GetFirstDayInMonth(date);
            int cellPos = day + indent - 1;
            int i = cellPos / 7;
            int j = cellPos - 7 * i;

            // i + 1 из-за того, что таблица сверху содержит названия дней
            return new TableLayoutPanelCellPosition(j, i + 1);
        }


        private RuDayOfWeek ToRussianDayOfWeek(DayOfWeek day)
        {
            if ( day == DayOfWeek.Sunday )
                return RuDayOfWeek.Sunday;

            int d = (int)day;
            return (RuDayOfWeek)(--d);
        }


        private RuDayOfWeek GetFirstDayInMonth(DateTime date)
        {
            return ToRussianDayOfWeek(
                _calendar.GetDayOfWeek(
                    new DateTime(date.Year, date.Month, 1)));
        }


        private void ChangeDateSpan(DateTime dateTime, int daysInMonth)
        {
            DayCell first = (DayCell)this.
                calendarTableLauout.GetControlFromPosition(0, 1);
            DateTime fromDt;
            if ( first.Day == 1 )
                fromDt = new DateTime(dateTime.Year,
                    dateTime.Month, first.Day);
            else {
                var temp = new DateTime(dateTime.Year, dateTime.Month, 1).AddMonths(-1);
                fromDt = new DateTime(temp.Year, temp.Month, first.Day);
            }


            int column = calendarTableLauout.ColumnCount - 1;
            int row = calendarTableLauout.RowCount - 1;
            DayCell last = (DayCell)this.
                calendarTableLauout.GetControlFromPosition(column, row);
            DateTime toDt;
            if ( last.Day == daysInMonth )
                toDt = new DateTime(dateTime.Year,
                    dateTime.Month, last.Day);
            else {
                var temp = new DateTime(dateTime.Year, dateTime.Month, 1).AddMonths(1);
                toDt = new DateTime(temp.Year, temp.Month, last.Day);
            }

            FromDateTime = fromDt;
            ToDateTime = toDt.AddDays(1);
            OnDateSpanChanged(new DateSpanChangedEventArgs(FromDateTime,
                ToDateTime));
        }


        private void InitializeCalendar(DateTime dateTime)
        {
            RuDayOfWeek day = GetFirstDayInMonth(dateTime);

            int daysInMonth = _calendar.GetDaysInMonth(dateTime.Year,
                dateTime.Month);

            int daysInPrevMonth = _calendar.GetDaysInMonth(dateTime.Year,
                dateTime.AddMonths(-1).Month);

            int rows = calendarTableLauout.RowCount;
            int columns = calendarTableLauout.ColumnCount;

            int count = 1;
            int count1 = 1;
            for ( int i = 1; i < rows; ++i ) {
                for ( int j = 0; j < columns; ++j ) {
                    DayCell dayCell = (DayCell)this.
                        calendarTableLauout.GetControlFromPosition(j, i);
                    if ( i == 1 && j < (int)day ) {
                        dayCell.Day = daysInPrevMonth - (int)day + 1 + j;
                        dayCell.ForeColor = Color.LightGray;
                    } else if ( count > daysInMonth ) {
                        dayCell.Day = count1++;
                        dayCell.ForeColor = Color.LightGray;
                    } else {
                        dayCell.Day = count++;
                        if ( !dayCell.ForeColor.Equals(Color.DimGray) )
                            dayCell.ForeColor = Color.DimGray;
                    }
                }
            }

            monthLabel.Text =
                CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateTime.Month) + 
                ", " + dateTime.Year;

            DateTime yearMonth = DateTime.Today;
            if ( dateTime.Month == yearMonth.Month )
                if ( dateTime.Year == yearMonth.Year )
                    GetCell(dateTime).ForeColor = Color.DodgerBlue;

            ChangeDateSpan(dateTime, daysInMonth);
        }


        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using ( var brush = new LinearGradientBrush(monthPanel.ClientRectangle,
                Color.DodgerBlue, Color.SteelBlue, 90F) )
            {
                e.Graphics.FillRectangle(brush, monthPanel.ClientRectangle);
            }
        }


        private void panel1_Resize(object sender, EventArgs e)
        {
            this.Invalidate();
        }


        private void nextMonthButton_Click(object sender, EventArgs e)
        {
            NextMonth();
        }


        private void prevMonthButton_Click(object sender, EventArgs e)
        {
            PrevMonth();
        }


        private void monthLabel_Click(object sender, EventArgs e)
        {
            monthCalendar1.Visible = true;
        }


        private void monthCalendar1_MouseLeave(object sender, EventArgs e)
        {
            monthCalendar1.Visible = false;
        }


        private void dayCell41_Click_1(object sender, EventArgs e)
        {
            DayCell dayCell = (DayCell)sender;
            MonthCheck check = CheckMonth(dayCell);
            if ( check != MonthCheck.CurrentMonth ) {
                if ( check == MonthCheck.NextMonth ) {
                    int day = dayCell.Day;
                    NextMonth();
                    dayCell = GetCell(new DateTime(_currentDate.Year,
                        _currentDate.Month, day));
                } else /*if ( check == MonthCheck.PrevMonth )*/ {
                    int day = dayCell.Day;
                    PrevMonth();
                    dayCell = GetCell(new DateTime(_currentDate.Year,
                        _currentDate.Month, day));
                }
            }


            if ( dayCell.Equals(SelectedCell) )
                return;

            if ( SelectedCell != null )
                SelectedCell.ClearBackgroud();
            SelectedCell = dayCell;
            SelectedCell.PaintBackground();


            DateTime date = new DateTime(_currentDate.Year,
                _currentDate.Month, dayCell.Day);
            SelectedDate = date;
            OnDaySelected(new DaySelectedEventArgs(date,
                dayCell.GetTasks(), dayCell.GetStatuses()));
        }


        private void TaskCalendar_Load(object sender, EventArgs e)
        {
            InitializeCalendar(DateTime.Today);
        }
    }
}
