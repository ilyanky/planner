﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Planner
{
    public partial class DayOfMonthPicker : UserControl
    {
        private const int CHECK_BOX_COUNT = 31;


        public String Days { get; private set; }


        public DayOfMonthPicker()
        {
            InitializeComponent();

            HidePanel();
        }




        private void ShowPanel()
        {
            panel.Visible = true;
            this.Height += panel.Height;
        }


        private void HidePanel()
        {
            panel.Visible = false;
            this.Height = textBox.Height;
        }


        private void SetText()
        {
            var text = new StringBuilder();
            for ( int i = 1; i <= CHECK_BOX_COUNT; ++i ) {
                String name = "checkBox" + i;
                var checkBox = (CheckBox)this.panel.Controls.Find(name, true)[0];
                if ( checkBox.Checked )
                    text.Append(", " + i);
            }
            if ( checkBox32.Checked )
                text.Append(", -1");

            if ( text.Length != 0 )
                text = text.Remove(0, 2);

            textBox.Text = text.ToString();
            Days = text.ToString();
        }


        private void daysAddButton_Click(object sender, EventArgs e)
        {
            SetText();

            if ( panel.Visible )
                HidePanel();
            else
                ShowPanel();
        }


        private void DayOfMonthPicker_Leave(object sender, EventArgs e)
        {
            SetText();
            HidePanel();
        }
    }
}
