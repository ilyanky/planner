﻿namespace Planner
{
    sealed partial class TaskCalendar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.monthPanel = new System.Windows.Forms.Panel();
            this.nextMonthButton = new System.Windows.Forms.Button();
            this.prevMonthButton = new System.Windows.Forms.Button();
            this.monthLabel = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.calendarTableLauout = new Planner.DoubleBufferedTableLayout();
            this.sunLabel = new System.Windows.Forms.Label();
            this.satLabel = new System.Windows.Forms.Label();
            this.friLabel = new System.Windows.Forms.Label();
            this.thuLabel = new System.Windows.Forms.Label();
            this.wedLabel = new System.Windows.Forms.Label();
            this.tueLabel = new System.Windows.Forms.Label();
            this.dayCell39 = new Planner.DayCell();
            this.dayCell37 = new Planner.DayCell();
            this.dayCell36 = new Planner.DayCell();
            this.dayCell35 = new Planner.DayCell();
            this.dayCell38 = new Planner.DayCell();
            this.dayCell41 = new Planner.DayCell();
            this.dayCell40 = new Planner.DayCell();
            this.dayCell34 = new Planner.DayCell();
            this.dayCell33 = new Planner.DayCell();
            this.dayCell32 = new Planner.DayCell();
            this.dayCell31 = new Planner.DayCell();
            this.dayCell30 = new Planner.DayCell();
            this.dayCell29 = new Planner.DayCell();
            this.dayCell28 = new Planner.DayCell();
            this.dayCell27 = new Planner.DayCell();
            this.dayCell26 = new Planner.DayCell();
            this.dayCell25 = new Planner.DayCell();
            this.dayCell24 = new Planner.DayCell();
            this.dayCell23 = new Planner.DayCell();
            this.dayCell22 = new Planner.DayCell();
            this.dayCell21 = new Planner.DayCell();
            this.dayCell20 = new Planner.DayCell();
            this.dayCell19 = new Planner.DayCell();
            this.dayCell18 = new Planner.DayCell();
            this.dayCell17 = new Planner.DayCell();
            this.dayCell16 = new Planner.DayCell();
            this.dayCell15 = new Planner.DayCell();
            this.dayCell14 = new Planner.DayCell();
            this.dayCell13 = new Planner.DayCell();
            this.dayCell12 = new Planner.DayCell();
            this.dayCell11 = new Planner.DayCell();
            this.dayCell10 = new Planner.DayCell();
            this.dayCell9 = new Planner.DayCell();
            this.dayCell8 = new Planner.DayCell();
            this.dayCell7 = new Planner.DayCell();
            this.dayCell6 = new Planner.DayCell();
            this.dayCell5 = new Planner.DayCell();
            this.dayCell4 = new Planner.DayCell();
            this.dayCell3 = new Planner.DayCell();
            this.dayCell2 = new Planner.DayCell();
            this.dayCell1 = new Planner.DayCell();
            this.dayCell0 = new Planner.DayCell();
            this.monLabel = new System.Windows.Forms.Label();
            this.monthPanel.SuspendLayout();
            this.calendarTableLauout.SuspendLayout();
            this.SuspendLayout();
            // 
            // monthPanel
            // 
            this.monthPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.monthPanel.BackColor = System.Drawing.Color.Transparent;
            this.monthPanel.Controls.Add(this.nextMonthButton);
            this.monthPanel.Controls.Add(this.prevMonthButton);
            this.monthPanel.Controls.Add(this.monthLabel);
            this.monthPanel.Location = new System.Drawing.Point(0, 0);
            this.monthPanel.Margin = new System.Windows.Forms.Padding(0);
            this.monthPanel.Name = "monthPanel";
            this.monthPanel.Size = new System.Drawing.Size(1636, 79);
            this.monthPanel.TabIndex = 1;
            this.monthPanel.Resize += new System.EventHandler(this.panel1_Resize);
            // 
            // nextMonthButton
            // 
            this.nextMonthButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nextMonthButton.FlatAppearance.BorderSize = 0;
            this.nextMonthButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.nextMonthButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nextMonthButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nextMonthButton.ForeColor = System.Drawing.Color.White;
            this.nextMonthButton.Location = new System.Drawing.Point(1600, 11);
            this.nextMonthButton.Margin = new System.Windows.Forms.Padding(4);
            this.nextMonthButton.Name = "nextMonthButton";
            this.nextMonthButton.Size = new System.Drawing.Size(32, 54);
            this.nextMonthButton.TabIndex = 2;
            this.nextMonthButton.Text = ">";
            this.nextMonthButton.UseVisualStyleBackColor = false;
            this.nextMonthButton.Click += new System.EventHandler(this.nextMonthButton_Click);
            // 
            // prevMonthButton
            // 
            this.prevMonthButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.prevMonthButton.FlatAppearance.BorderSize = 0;
            this.prevMonthButton.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.prevMonthButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.prevMonthButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.prevMonthButton.ForeColor = System.Drawing.Color.White;
            this.prevMonthButton.Location = new System.Drawing.Point(4, 14);
            this.prevMonthButton.Margin = new System.Windows.Forms.Padding(4);
            this.prevMonthButton.Name = "prevMonthButton";
            this.prevMonthButton.Size = new System.Drawing.Size(32, 49);
            this.prevMonthButton.TabIndex = 1;
            this.prevMonthButton.Text = "<";
            this.prevMonthButton.UseVisualStyleBackColor = true;
            this.prevMonthButton.Click += new System.EventHandler(this.prevMonthButton_Click);
            // 
            // monthLabel
            // 
            this.monthLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.monthLabel.AutoSize = true;
            this.monthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.monthLabel.ForeColor = System.Drawing.Color.White;
            this.monthLabel.Location = new System.Drawing.Point(724, 28);
            this.monthLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.monthLabel.Name = "monthLabel";
            this.monthLabel.Size = new System.Drawing.Size(129, 24);
            this.monthLabel.TabIndex = 0;
            this.monthLabel.Text = "Месяц, 2012";
            this.monthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.monthLabel.Click += new System.EventHandler(this.monthLabel_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(893, 48);
            this.monthCalendar1.Margin = new System.Windows.Forms.Padding(12, 11, 12, 11);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 3;
            this.monthCalendar1.Visible = false;
            this.monthCalendar1.MouseLeave += new System.EventHandler(this.monthCalendar1_MouseLeave);
            // 
            // calendarTableLauout
            // 
            this.calendarTableLauout.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.calendarTableLauout.BackColor = System.Drawing.Color.White;
            this.calendarTableLauout.ColumnCount = 7;
            this.calendarTableLauout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.calendarTableLauout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarTableLauout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarTableLauout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarTableLauout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarTableLauout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarTableLauout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarTableLauout.Controls.Add(this.sunLabel, 6, 0);
            this.calendarTableLauout.Controls.Add(this.satLabel, 5, 0);
            this.calendarTableLauout.Controls.Add(this.friLabel, 4, 0);
            this.calendarTableLauout.Controls.Add(this.thuLabel, 3, 0);
            this.calendarTableLauout.Controls.Add(this.wedLabel, 2, 0);
            this.calendarTableLauout.Controls.Add(this.tueLabel, 1, 0);
            this.calendarTableLauout.Controls.Add(this.dayCell39, 0, 6);
            this.calendarTableLauout.Controls.Add(this.dayCell37, 0, 6);
            this.calendarTableLauout.Controls.Add(this.dayCell36, 0, 6);
            this.calendarTableLauout.Controls.Add(this.dayCell35, 0, 6);
            this.calendarTableLauout.Controls.Add(this.dayCell38, 0, 6);
            this.calendarTableLauout.Controls.Add(this.dayCell41, 0, 6);
            this.calendarTableLauout.Controls.Add(this.dayCell40, 0, 6);
            this.calendarTableLauout.Controls.Add(this.dayCell34, 6, 5);
            this.calendarTableLauout.Controls.Add(this.dayCell33, 5, 5);
            this.calendarTableLauout.Controls.Add(this.dayCell32, 4, 5);
            this.calendarTableLauout.Controls.Add(this.dayCell31, 3, 5);
            this.calendarTableLauout.Controls.Add(this.dayCell30, 2, 5);
            this.calendarTableLauout.Controls.Add(this.dayCell29, 1, 5);
            this.calendarTableLauout.Controls.Add(this.dayCell28, 0, 5);
            this.calendarTableLauout.Controls.Add(this.dayCell27, 6, 4);
            this.calendarTableLauout.Controls.Add(this.dayCell26, 5, 4);
            this.calendarTableLauout.Controls.Add(this.dayCell25, 4, 4);
            this.calendarTableLauout.Controls.Add(this.dayCell24, 3, 4);
            this.calendarTableLauout.Controls.Add(this.dayCell23, 2, 4);
            this.calendarTableLauout.Controls.Add(this.dayCell22, 1, 4);
            this.calendarTableLauout.Controls.Add(this.dayCell21, 0, 4);
            this.calendarTableLauout.Controls.Add(this.dayCell20, 6, 3);
            this.calendarTableLauout.Controls.Add(this.dayCell19, 5, 3);
            this.calendarTableLauout.Controls.Add(this.dayCell18, 4, 3);
            this.calendarTableLauout.Controls.Add(this.dayCell17, 3, 3);
            this.calendarTableLauout.Controls.Add(this.dayCell16, 2, 3);
            this.calendarTableLauout.Controls.Add(this.dayCell15, 1, 3);
            this.calendarTableLauout.Controls.Add(this.dayCell14, 0, 3);
            this.calendarTableLauout.Controls.Add(this.dayCell13, 6, 2);
            this.calendarTableLauout.Controls.Add(this.dayCell12, 5, 2);
            this.calendarTableLauout.Controls.Add(this.dayCell11, 4, 2);
            this.calendarTableLauout.Controls.Add(this.dayCell10, 3, 2);
            this.calendarTableLauout.Controls.Add(this.dayCell9, 2, 2);
            this.calendarTableLauout.Controls.Add(this.dayCell8, 1, 2);
            this.calendarTableLauout.Controls.Add(this.dayCell7, 0, 2);
            this.calendarTableLauout.Controls.Add(this.dayCell6, 6, 1);
            this.calendarTableLauout.Controls.Add(this.dayCell5, 5, 1);
            this.calendarTableLauout.Controls.Add(this.dayCell4, 4, 1);
            this.calendarTableLauout.Controls.Add(this.dayCell3, 3, 1);
            this.calendarTableLauout.Controls.Add(this.dayCell2, 2, 1);
            this.calendarTableLauout.Controls.Add(this.dayCell1, 1, 1);
            this.calendarTableLauout.Controls.Add(this.dayCell0, 0, 1);
            this.calendarTableLauout.Controls.Add(this.monLabel, 0, 0);
            this.calendarTableLauout.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.calendarTableLauout.Location = new System.Drawing.Point(0, 79);
            this.calendarTableLauout.Margin = new System.Windows.Forms.Padding(0);
            this.calendarTableLauout.Name = "calendarTableLauout";
            this.calendarTableLauout.RowCount = 7;
            this.calendarTableLauout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.calendarTableLauout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.calendarTableLauout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.calendarTableLauout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.calendarTableLauout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.calendarTableLauout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.calendarTableLauout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.calendarTableLauout.Size = new System.Drawing.Size(1636, 724);
            this.calendarTableLauout.TabIndex = 0;
            // 
            // sunLabel
            // 
            this.sunLabel.BackColor = System.Drawing.Color.White;
            this.sunLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sunLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sunLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.sunLabel.Location = new System.Drawing.Point(1398, 0);
            this.sunLabel.Margin = new System.Windows.Forms.Padding(0);
            this.sunLabel.Name = "sunLabel";
            this.sunLabel.Size = new System.Drawing.Size(238, 103);
            this.sunLabel.TabIndex = 62;
            this.sunLabel.Text = "Воскресенье";
            this.sunLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // satLabel
            // 
            this.satLabel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.satLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.satLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.satLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.satLabel.Location = new System.Drawing.Point(1165, 0);
            this.satLabel.Margin = new System.Windows.Forms.Padding(0);
            this.satLabel.Name = "satLabel";
            this.satLabel.Size = new System.Drawing.Size(233, 103);
            this.satLabel.TabIndex = 61;
            this.satLabel.Text = "Суббота";
            this.satLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // friLabel
            // 
            this.friLabel.BackColor = System.Drawing.Color.White;
            this.friLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.friLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.friLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.friLabel.Location = new System.Drawing.Point(932, 0);
            this.friLabel.Margin = new System.Windows.Forms.Padding(0);
            this.friLabel.Name = "friLabel";
            this.friLabel.Size = new System.Drawing.Size(233, 103);
            this.friLabel.TabIndex = 60;
            this.friLabel.Text = "Пятница";
            this.friLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // thuLabel
            // 
            this.thuLabel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.thuLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thuLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.thuLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.thuLabel.Location = new System.Drawing.Point(699, 0);
            this.thuLabel.Margin = new System.Windows.Forms.Padding(0);
            this.thuLabel.Name = "thuLabel";
            this.thuLabel.Size = new System.Drawing.Size(233, 103);
            this.thuLabel.TabIndex = 59;
            this.thuLabel.Text = "Четверг";
            this.thuLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // wedLabel
            // 
            this.wedLabel.BackColor = System.Drawing.Color.White;
            this.wedLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wedLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.wedLabel.Location = new System.Drawing.Point(466, 0);
            this.wedLabel.Margin = new System.Windows.Forms.Padding(0);
            this.wedLabel.Name = "wedLabel";
            this.wedLabel.Size = new System.Drawing.Size(233, 103);
            this.wedLabel.TabIndex = 58;
            this.wedLabel.Text = "Среда";
            this.wedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tueLabel
            // 
            this.tueLabel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tueLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.tueLabel.Location = new System.Drawing.Point(233, 0);
            this.tueLabel.Margin = new System.Windows.Forms.Padding(0);
            this.tueLabel.Name = "tueLabel";
            this.tueLabel.Size = new System.Drawing.Size(233, 103);
            this.tueLabel.TabIndex = 57;
            this.tueLabel.Text = "Вторник";
            this.tueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dayCell39
            // 
            this.dayCell39.BackColor = System.Drawing.Color.White;
            this.dayCell39.Day = 0;
            this.dayCell39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell39.Location = new System.Drawing.Point(932, 618);
            this.dayCell39.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell39.Name = "dayCell39";
            this.dayCell39.Size = new System.Drawing.Size(233, 106);
            this.dayCell39.TabIndex = 55;
            this.dayCell39.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell37
            // 
            this.dayCell37.BackColor = System.Drawing.Color.White;
            this.dayCell37.Day = 0;
            this.dayCell37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell37.Location = new System.Drawing.Point(466, 618);
            this.dayCell37.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell37.Name = "dayCell37";
            this.dayCell37.Size = new System.Drawing.Size(233, 106);
            this.dayCell37.TabIndex = 54;
            this.dayCell37.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell36
            // 
            this.dayCell36.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell36.Day = 0;
            this.dayCell36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell36.Location = new System.Drawing.Point(233, 618);
            this.dayCell36.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell36.Name = "dayCell36";
            this.dayCell36.Size = new System.Drawing.Size(233, 106);
            this.dayCell36.TabIndex = 53;
            this.dayCell36.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell35
            // 
            this.dayCell35.BackColor = System.Drawing.Color.White;
            this.dayCell35.Day = 0;
            this.dayCell35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell35.Location = new System.Drawing.Point(0, 618);
            this.dayCell35.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell35.Name = "dayCell35";
            this.dayCell35.Size = new System.Drawing.Size(233, 106);
            this.dayCell35.TabIndex = 52;
            this.dayCell35.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell38
            // 
            this.dayCell38.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell38.Day = 0;
            this.dayCell38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell38.Location = new System.Drawing.Point(699, 618);
            this.dayCell38.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell38.Name = "dayCell38";
            this.dayCell38.Size = new System.Drawing.Size(233, 106);
            this.dayCell38.TabIndex = 51;
            this.dayCell38.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell41
            // 
            this.dayCell41.BackColor = System.Drawing.Color.White;
            this.dayCell41.Day = 0;
            this.dayCell41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell41.Location = new System.Drawing.Point(1398, 618);
            this.dayCell41.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell41.Name = "dayCell41";
            this.dayCell41.Size = new System.Drawing.Size(238, 106);
            this.dayCell41.TabIndex = 50;
            this.dayCell41.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell40
            // 
            this.dayCell40.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell40.Day = 0;
            this.dayCell40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell40.Location = new System.Drawing.Point(1165, 618);
            this.dayCell40.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell40.Name = "dayCell40";
            this.dayCell40.Size = new System.Drawing.Size(233, 106);
            this.dayCell40.TabIndex = 49;
            this.dayCell40.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell34
            // 
            this.dayCell34.BackColor = System.Drawing.Color.White;
            this.dayCell34.Day = 0;
            this.dayCell34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell34.Location = new System.Drawing.Point(1398, 515);
            this.dayCell34.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell34.Name = "dayCell34";
            this.dayCell34.Size = new System.Drawing.Size(238, 103);
            this.dayCell34.TabIndex = 48;
            this.dayCell34.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell33
            // 
            this.dayCell33.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell33.Day = 0;
            this.dayCell33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell33.Location = new System.Drawing.Point(1165, 515);
            this.dayCell33.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell33.Name = "dayCell33";
            this.dayCell33.Size = new System.Drawing.Size(233, 103);
            this.dayCell33.TabIndex = 47;
            this.dayCell33.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell32
            // 
            this.dayCell32.BackColor = System.Drawing.Color.White;
            this.dayCell32.Day = 0;
            this.dayCell32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell32.Location = new System.Drawing.Point(932, 515);
            this.dayCell32.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell32.Name = "dayCell32";
            this.dayCell32.Size = new System.Drawing.Size(233, 103);
            this.dayCell32.TabIndex = 46;
            this.dayCell32.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell31
            // 
            this.dayCell31.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell31.Day = 0;
            this.dayCell31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell31.Location = new System.Drawing.Point(699, 515);
            this.dayCell31.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell31.Name = "dayCell31";
            this.dayCell31.Size = new System.Drawing.Size(233, 103);
            this.dayCell31.TabIndex = 45;
            this.dayCell31.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell30
            // 
            this.dayCell30.BackColor = System.Drawing.Color.White;
            this.dayCell30.Day = 0;
            this.dayCell30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell30.Location = new System.Drawing.Point(466, 515);
            this.dayCell30.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell30.Name = "dayCell30";
            this.dayCell30.Size = new System.Drawing.Size(233, 103);
            this.dayCell30.TabIndex = 44;
            this.dayCell30.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell29
            // 
            this.dayCell29.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell29.Day = 0;
            this.dayCell29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell29.Location = new System.Drawing.Point(233, 515);
            this.dayCell29.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell29.Name = "dayCell29";
            this.dayCell29.Size = new System.Drawing.Size(233, 103);
            this.dayCell29.TabIndex = 43;
            this.dayCell29.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell28
            // 
            this.dayCell28.BackColor = System.Drawing.Color.White;
            this.dayCell28.Day = 0;
            this.dayCell28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell28.Location = new System.Drawing.Point(0, 515);
            this.dayCell28.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell28.Name = "dayCell28";
            this.dayCell28.Size = new System.Drawing.Size(233, 103);
            this.dayCell28.TabIndex = 42;
            this.dayCell28.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell27
            // 
            this.dayCell27.BackColor = System.Drawing.Color.White;
            this.dayCell27.Day = 0;
            this.dayCell27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell27.Location = new System.Drawing.Point(1398, 412);
            this.dayCell27.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell27.Name = "dayCell27";
            this.dayCell27.Size = new System.Drawing.Size(238, 103);
            this.dayCell27.TabIndex = 41;
            this.dayCell27.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell26
            // 
            this.dayCell26.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell26.Day = 0;
            this.dayCell26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell26.Location = new System.Drawing.Point(1165, 412);
            this.dayCell26.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell26.Name = "dayCell26";
            this.dayCell26.Size = new System.Drawing.Size(233, 103);
            this.dayCell26.TabIndex = 40;
            this.dayCell26.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell25
            // 
            this.dayCell25.BackColor = System.Drawing.Color.White;
            this.dayCell25.Day = 0;
            this.dayCell25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell25.Location = new System.Drawing.Point(932, 412);
            this.dayCell25.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell25.Name = "dayCell25";
            this.dayCell25.Size = new System.Drawing.Size(233, 103);
            this.dayCell25.TabIndex = 39;
            this.dayCell25.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell24
            // 
            this.dayCell24.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell24.Day = 0;
            this.dayCell24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell24.Location = new System.Drawing.Point(699, 412);
            this.dayCell24.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell24.Name = "dayCell24";
            this.dayCell24.Size = new System.Drawing.Size(233, 103);
            this.dayCell24.TabIndex = 38;
            this.dayCell24.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell23
            // 
            this.dayCell23.BackColor = System.Drawing.Color.White;
            this.dayCell23.Day = 0;
            this.dayCell23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell23.Location = new System.Drawing.Point(466, 412);
            this.dayCell23.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell23.Name = "dayCell23";
            this.dayCell23.Size = new System.Drawing.Size(233, 103);
            this.dayCell23.TabIndex = 37;
            this.dayCell23.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell22
            // 
            this.dayCell22.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell22.Day = 0;
            this.dayCell22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell22.Location = new System.Drawing.Point(233, 412);
            this.dayCell22.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell22.Name = "dayCell22";
            this.dayCell22.Size = new System.Drawing.Size(233, 103);
            this.dayCell22.TabIndex = 36;
            this.dayCell22.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell21
            // 
            this.dayCell21.BackColor = System.Drawing.Color.White;
            this.dayCell21.Day = 0;
            this.dayCell21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell21.Location = new System.Drawing.Point(0, 412);
            this.dayCell21.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell21.Name = "dayCell21";
            this.dayCell21.Size = new System.Drawing.Size(233, 103);
            this.dayCell21.TabIndex = 35;
            this.dayCell21.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell20
            // 
            this.dayCell20.BackColor = System.Drawing.Color.White;
            this.dayCell20.Day = 0;
            this.dayCell20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell20.Location = new System.Drawing.Point(1398, 309);
            this.dayCell20.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell20.Name = "dayCell20";
            this.dayCell20.Size = new System.Drawing.Size(238, 103);
            this.dayCell20.TabIndex = 34;
            this.dayCell20.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell19
            // 
            this.dayCell19.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell19.Day = 0;
            this.dayCell19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell19.Location = new System.Drawing.Point(1165, 309);
            this.dayCell19.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell19.Name = "dayCell19";
            this.dayCell19.Size = new System.Drawing.Size(233, 103);
            this.dayCell19.TabIndex = 33;
            this.dayCell19.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell18
            // 
            this.dayCell18.BackColor = System.Drawing.Color.White;
            this.dayCell18.Day = 0;
            this.dayCell18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell18.Location = new System.Drawing.Point(932, 309);
            this.dayCell18.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell18.Name = "dayCell18";
            this.dayCell18.Size = new System.Drawing.Size(233, 103);
            this.dayCell18.TabIndex = 32;
            this.dayCell18.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell17
            // 
            this.dayCell17.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell17.Day = 0;
            this.dayCell17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell17.Location = new System.Drawing.Point(699, 309);
            this.dayCell17.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell17.Name = "dayCell17";
            this.dayCell17.Size = new System.Drawing.Size(233, 103);
            this.dayCell17.TabIndex = 31;
            this.dayCell17.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell16
            // 
            this.dayCell16.BackColor = System.Drawing.Color.White;
            this.dayCell16.Day = 0;
            this.dayCell16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell16.Location = new System.Drawing.Point(466, 309);
            this.dayCell16.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell16.Name = "dayCell16";
            this.dayCell16.Size = new System.Drawing.Size(233, 103);
            this.dayCell16.TabIndex = 30;
            this.dayCell16.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell15
            // 
            this.dayCell15.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell15.Day = 0;
            this.dayCell15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell15.Location = new System.Drawing.Point(233, 309);
            this.dayCell15.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell15.Name = "dayCell15";
            this.dayCell15.Size = new System.Drawing.Size(233, 103);
            this.dayCell15.TabIndex = 29;
            this.dayCell15.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell14
            // 
            this.dayCell14.BackColor = System.Drawing.Color.White;
            this.dayCell14.Day = 0;
            this.dayCell14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell14.Location = new System.Drawing.Point(0, 309);
            this.dayCell14.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell14.Name = "dayCell14";
            this.dayCell14.Size = new System.Drawing.Size(233, 103);
            this.dayCell14.TabIndex = 28;
            this.dayCell14.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell13
            // 
            this.dayCell13.BackColor = System.Drawing.Color.White;
            this.dayCell13.Day = 0;
            this.dayCell13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell13.Location = new System.Drawing.Point(1398, 206);
            this.dayCell13.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell13.Name = "dayCell13";
            this.dayCell13.Size = new System.Drawing.Size(238, 103);
            this.dayCell13.TabIndex = 27;
            this.dayCell13.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell12
            // 
            this.dayCell12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell12.Day = 0;
            this.dayCell12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell12.Location = new System.Drawing.Point(1165, 206);
            this.dayCell12.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell12.Name = "dayCell12";
            this.dayCell12.Size = new System.Drawing.Size(233, 103);
            this.dayCell12.TabIndex = 26;
            this.dayCell12.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell11
            // 
            this.dayCell11.BackColor = System.Drawing.Color.White;
            this.dayCell11.Day = 0;
            this.dayCell11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell11.Location = new System.Drawing.Point(932, 206);
            this.dayCell11.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell11.Name = "dayCell11";
            this.dayCell11.Size = new System.Drawing.Size(233, 103);
            this.dayCell11.TabIndex = 25;
            this.dayCell11.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell10
            // 
            this.dayCell10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell10.Day = 0;
            this.dayCell10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell10.Location = new System.Drawing.Point(699, 206);
            this.dayCell10.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell10.Name = "dayCell10";
            this.dayCell10.Size = new System.Drawing.Size(233, 103);
            this.dayCell10.TabIndex = 24;
            this.dayCell10.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell9
            // 
            this.dayCell9.BackColor = System.Drawing.Color.White;
            this.dayCell9.Day = 0;
            this.dayCell9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell9.Location = new System.Drawing.Point(466, 206);
            this.dayCell9.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell9.Name = "dayCell9";
            this.dayCell9.Size = new System.Drawing.Size(233, 103);
            this.dayCell9.TabIndex = 23;
            this.dayCell9.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell8
            // 
            this.dayCell8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell8.Day = 0;
            this.dayCell8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell8.Location = new System.Drawing.Point(233, 206);
            this.dayCell8.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell8.Name = "dayCell8";
            this.dayCell8.Size = new System.Drawing.Size(233, 103);
            this.dayCell8.TabIndex = 22;
            this.dayCell8.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell7
            // 
            this.dayCell7.BackColor = System.Drawing.Color.White;
            this.dayCell7.Day = 0;
            this.dayCell7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell7.Location = new System.Drawing.Point(0, 206);
            this.dayCell7.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell7.Name = "dayCell7";
            this.dayCell7.Size = new System.Drawing.Size(233, 103);
            this.dayCell7.TabIndex = 21;
            this.dayCell7.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell6
            // 
            this.dayCell6.BackColor = System.Drawing.Color.White;
            this.dayCell6.Day = 0;
            this.dayCell6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell6.Location = new System.Drawing.Point(1398, 103);
            this.dayCell6.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell6.Name = "dayCell6";
            this.dayCell6.Size = new System.Drawing.Size(238, 103);
            this.dayCell6.TabIndex = 20;
            this.dayCell6.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell5
            // 
            this.dayCell5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell5.Day = 0;
            this.dayCell5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell5.Location = new System.Drawing.Point(1165, 103);
            this.dayCell5.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell5.Name = "dayCell5";
            this.dayCell5.Size = new System.Drawing.Size(233, 103);
            this.dayCell5.TabIndex = 19;
            this.dayCell5.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell4
            // 
            this.dayCell4.BackColor = System.Drawing.Color.White;
            this.dayCell4.Day = 0;
            this.dayCell4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell4.Location = new System.Drawing.Point(932, 103);
            this.dayCell4.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell4.Name = "dayCell4";
            this.dayCell4.Size = new System.Drawing.Size(233, 103);
            this.dayCell4.TabIndex = 18;
            this.dayCell4.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell3
            // 
            this.dayCell3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell3.Day = 0;
            this.dayCell3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell3.Location = new System.Drawing.Point(699, 103);
            this.dayCell3.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell3.Name = "dayCell3";
            this.dayCell3.Size = new System.Drawing.Size(233, 103);
            this.dayCell3.TabIndex = 17;
            this.dayCell3.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell2
            // 
            this.dayCell2.BackColor = System.Drawing.Color.White;
            this.dayCell2.Day = 0;
            this.dayCell2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell2.Location = new System.Drawing.Point(466, 103);
            this.dayCell2.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell2.Name = "dayCell2";
            this.dayCell2.Size = new System.Drawing.Size(233, 103);
            this.dayCell2.TabIndex = 16;
            this.dayCell2.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell1
            // 
            this.dayCell1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dayCell1.Day = 0;
            this.dayCell1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell1.Location = new System.Drawing.Point(233, 103);
            this.dayCell1.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell1.Name = "dayCell1";
            this.dayCell1.Size = new System.Drawing.Size(233, 103);
            this.dayCell1.TabIndex = 15;
            this.dayCell1.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // dayCell0
            // 
            this.dayCell0.BackColor = System.Drawing.Color.White;
            this.dayCell0.Day = 0;
            this.dayCell0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dayCell0.Location = new System.Drawing.Point(0, 103);
            this.dayCell0.Margin = new System.Windows.Forms.Padding(0);
            this.dayCell0.Name = "dayCell0";
            this.dayCell0.Size = new System.Drawing.Size(233, 103);
            this.dayCell0.TabIndex = 14;
            this.dayCell0.Click += new System.EventHandler(this.dayCell41_Click_1);
            // 
            // monLabel
            // 
            this.monLabel.BackColor = System.Drawing.Color.White;
            this.monLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.monLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.monLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.monLabel.Location = new System.Drawing.Point(0, 0);
            this.monLabel.Margin = new System.Windows.Forms.Padding(0);
            this.monLabel.Name = "monLabel";
            this.monLabel.Size = new System.Drawing.Size(233, 103);
            this.monLabel.TabIndex = 56;
            this.monLabel.Text = "Понедельник";
            this.monLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TaskCalendar
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.monthPanel);
            this.Controls.Add(this.calendarTableLauout);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "TaskCalendar";
            this.Size = new System.Drawing.Size(1636, 802);
            this.Load += new System.EventHandler(this.TaskCalendar_Load);
            this.monthPanel.ResumeLayout(false);
            this.monthPanel.PerformLayout();
            this.calendarTableLauout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DoubleBufferedTableLayout calendarTableLauout;
        private DayCell dayCell34;
        private DayCell dayCell33;
        private DayCell dayCell32;
        private DayCell dayCell31;
        private DayCell dayCell30;
        private DayCell dayCell29;
        private DayCell dayCell28;
        private DayCell dayCell27;
        private DayCell dayCell26;
        private DayCell dayCell25;
        private DayCell dayCell24;
        private DayCell dayCell23;
        private DayCell dayCell22;
        private DayCell dayCell21;
        private DayCell dayCell20;
        private DayCell dayCell19;
        private DayCell dayCell18;
        private DayCell dayCell17;
        private DayCell dayCell16;
        private DayCell dayCell15;
        private DayCell dayCell14;
        private DayCell dayCell13;
        private DayCell dayCell12;
        private DayCell dayCell11;
        private DayCell dayCell10;
        private DayCell dayCell9;
        private DayCell dayCell8;
        private DayCell dayCell7;
        private DayCell dayCell6;
        private DayCell dayCell5;
        private DayCell dayCell4;
        private DayCell dayCell3;
        private DayCell dayCell2;
        private DayCell dayCell1;
        private DayCell dayCell0;
        private System.Windows.Forms.Panel monthPanel;
        private System.Windows.Forms.Label sunLabel;
        private System.Windows.Forms.Label satLabel;
        private System.Windows.Forms.Label friLabel;
        private System.Windows.Forms.Label thuLabel;
        private System.Windows.Forms.Label wedLabel;
        private System.Windows.Forms.Label tueLabel;
        private DayCell dayCell39;
        private DayCell dayCell37;
        private DayCell dayCell36;
        private DayCell dayCell35;
        private DayCell dayCell38;
        private DayCell dayCell41;
        private DayCell dayCell40;
        private System.Windows.Forms.Label monLabel;
        private System.Windows.Forms.Label monthLabel;
        private System.Windows.Forms.Button prevMonthButton;
        private System.Windows.Forms.Button nextMonthButton;
        private System.Windows.Forms.MonthCalendar monthCalendar1;





    }
}
