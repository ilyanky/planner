﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Planner
{
    public enum LogViewType
    {
        ServerLog,
        UserLog,
        TaskLog,
        PerformanceLog
    }


    public partial class LogViewForm : Form
    {
        public LogViewType Type { get; private set; }

        private ClientPlanner _client;

        public int TaskID { get; private set; }



        public LogViewForm()
        {
            InitializeComponent();
        }


        public LogViewForm(ClientPlanner client, LogViewType type) : this()
        {
            Type = type;
            _client = client;
            InitializeListView();
        }


        public LogViewForm(ClientPlanner client, int taskID) : this()
        {
            Type = LogViewType.TaskLog;
            TaskID = taskID;
            _client = client;
            InitializeListView();
        }


        private void InitializeListView()
        {
            if ( Type == LogViewType.UserLog ) {
                mainListView.Columns.Add("UserName", "Имя пользователя");
                mainListView.Columns.Add("DateTime", "Дата и время");
                mainListView.Columns.Add("TypeName", "Тип записи");
                mainListView.Columns.Add("TaskName", "Название задачи");
                mainListView.Columns.Add("Description", "Описание");

                nameLabel.Text = "Лог пользователя";
            } else if ( Type == LogViewType.ServerLog ) {
                mainListView.Columns.Add("DateTime", "Дата и время");
                mainListView.Columns.Add("Description", "Описание");

                nameLabel.Text = "Лог сервера";
            } else if ( Type == LogViewType.TaskLog ) {
                mainListView.Columns.Add("TaskName", "Название задачи");
                mainListView.Columns.Add("DateTime", "Дата и время");
                mainListView.Columns.Add("TypeName", "Тип записи");
                mainListView.Columns.Add("Description", "Описание");
                mainListView.Columns.Add("ExitCode", "Код завершения");

                nameLabel.Text = "Лог задачи";
            }
        }




        private static void PaintBackground(Control control, Color first,
            Color second, Single angle, PaintEventArgs e)
        {
            Rectangle r = new Rectangle(control.Location, control.Size);
            using ( var brush =
                new LinearGradientBrush(r, first, second, angle) ) {
                e.Graphics.FillRectangle(brush, r);
            }
        }


        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            PaintBackground(topPanel,
                Color.DodgerBlue, Color.SteelBlue, 90F,
                e);
        }


        private void LogViewForm_Resize(object sender, EventArgs e)
        {
            //            topPanel.Invalidate();
            //            contentPanel.Invalidate();
            //            bottomPanel.Invalidate();

            this.Invalidate();
        }




        public void AddEntries(UserLogInfo[] entries)
        {
            if ( Type != LogViewType.UserLog )
                return;

            foreach ( var entry in entries ) {
                ListViewItem item = new ListViewItem(entry.UserName);
                item.SubItems.Add(entry.DateTime.ToString());
                item.SubItems.Add(entry.TypeName);
                item.SubItems.Add(entry.TaskName);
                item.SubItems.Add(entry.Description);
                this.mainListView.Items.Add(item);
            }
        }



        public void AddEntries(Tuple<DateTime, String>[] entries)
        {
            if ( Type != LogViewType.ServerLog )
                return;

            foreach ( var entry in entries ) {
                ListViewItem item = new ListViewItem(entry.Item1.ToString());
                item.SubItems.Add(entry.Item2);
                this.mainListView.Items.Add(item);
            }
        }


        public void AddEntries(TaskLogInfo[] entries)
        {
            if ( Type != LogViewType.TaskLog )
                return;

            foreach ( var entry in entries ) {
                ListViewItem item = new ListViewItem(entry.TaskName);
                item.SubItems.Add(entry.DateTime.ToString());
                item.SubItems.Add(entry.TypeName);
                item.SubItems.Add(entry.Description);
                item.SubItems.Add(entry.ExitCode);
                this.mainListView.Items.Add(item);
            }
        }


        private void mainListView_ItemSelectionChanged(object sender,
            ListViewItemSelectionChangedEventArgs e)
        {
            if ( !e.IsSelected ) {
                descriptionTextBox.Clear();
                return;
            }

            int column = mainListView.Columns.IndexOfKey("Description");
            descriptionTextBox.Text = e.Item.SubItems[column].Text;
        }



        private void RefreshView()
        {
            mainListView.Items.Clear();
            descriptionTextBox.Clear();

            String error;
            try {
                if ( Type == LogViewType.ServerLog ) {
                    Tuple<DateTime, String>[] serverLog;
                    if ( !_client.GetServerLog(out serverLog, out error) ) {
                        MessageBox.Show("Невозможно получить лог пользователя. Подробнее:\n" + error,
                            "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    AddEntries(serverLog);
                } else if ( Type == LogViewType.UserLog ) {
                    UserLogInfo[] userLog;
                    if ( !_client.GetUserLog(out userLog, out error) ) {
                        MessageBox.Show("Невозможно получить лог пользователя. Подробнее:\n" + error,
                            "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    AddEntries(userLog);
                } else if ( Type == LogViewType.TaskLog ) {
                    TaskLogInfo[] taskLog;
                    if ( !_client.GetTaskLog(TaskID, out taskLog, out error) ) {
                        MessageBox.Show("Невозможно получить лог задачи. Подробнее:\n" + error,
                            "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    AddEntries(taskLog);
                } else
                    ;
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }



        private void refreshToolStripButton_Click(object sender, EventArgs e)
        {
            RefreshView();
        }
    }
}
