﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Planner
{
    public partial class AddTaskForm : Form
    {
        private readonly ClientPlanner _client;

        private Panel _currentPanel;

        private List<ScheduleInfo> _currentScheds;
        public ScheduleInfo[] CurrentScheds
        {
            get
            {
                return _currentScheds.ToArray();
            }
        }

        public TaskInfo NewTask { get; private set; }


        public AddTaskForm()
        {
            InitializeComponent();

            ToggleNameDescriptionPanel();

            _currentScheds =
                new List<ScheduleInfo>();
        }


        public AddTaskForm(ClientPlanner client, String[] types) : this()
        {
            _client = client;

            foreach ( var type in types )
                taskTypeComboBox.Items.Add(type);

            taskTypeComboBox.SelectedIndex = 0;
        }


        private static void PaintBackground(Control control, Color first,
            Color second, Single angle, PaintEventArgs e)
        {
            Rectangle r = new Rectangle(control.Location, control.Size);
            using ( var brush =
                new LinearGradientBrush(r, first, second, angle) ) {
                e.Graphics.FillRectangle(brush, r);
            }
        }


        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            PaintBackground(topPanel,
                Color.DodgerBlue, Color.SteelBlue, 90F,
                e);
        }


        private void AddTaskForm_Resize(object sender, EventArgs e)
        {
            //            topPanel.Invalidate();
            //            contentPanel.Invalidate();
            //            bottomPanel.Invalidate();

            this.Invalidate();
        }




        private void ToggleNameDescriptionPanel()
        {
            if ( nameDescriptionPanel.Visible ) {
                nameDescriptionPanel.Visible = false;
                prevButton.Visible = true;
                chooseNameLabel.Font =
                    new Font(chooseNameLabel.Font, FontStyle.Regular);
            } else {
                _currentPanel = nameDescriptionPanel;
                nameDescriptionPanel.Visible = true;
                prevButton.Visible = false;
                chooseNameLabel.Font =
                    new Font(chooseNameLabel.Font, FontStyle.Underline);
            }
        }


        private void ToggleCreateTriggerPanel()
        {
            if ( createTriggerPanel.Visible ) {
                createTriggerPanel.Visible = false;
                addTriggerLabel.Font =
                    new Font(addTriggerLabel.Font, FontStyle.Regular);
            } else {
                _currentPanel = createTriggerPanel;
                createTriggerPanel.Visible = true;
                addTriggerLabel.Font =
                    new Font(addTriggerLabel.Font, FontStyle.Underline);
            }
        }


        private void ToggleTaskTypesPanel()
        {
            if ( taskTypesPanel.Visible ) {
                taskTypesPanel.Visible = false;
                chooseTaskTypeLabel.Font =
                    new Font(chooseTaskTypeLabel.Font, FontStyle.Regular);
            } else {
                _currentPanel = taskTypesPanel;
                taskTypesPanel.Visible = true;
                chooseTaskTypeLabel.Font =
                    new Font(chooseTaskTypeLabel.Font, FontStyle.Underline);
            }
        }


        private void ToggleAddTaskFinishPanel()
        {
            if ( addTaskFinishPanel.Visible ) {
                addTaskFinishPanel.Visible = false;
                finishLabel.Font =
                    new Font(finishLabel.Font, FontStyle.Regular);
                nextButton.Text = "Далее >";
            } else {
                _currentPanel = addTaskFinishPanel;
                addTaskFinishPanel.Visible = true;
                finishLabel.Font =
                    new Font(finishLabel.Font, FontStyle.Underline);
                nextButton.Text = "Готово";

                finalNameTextBox.Text = taskNameTextBox.Text;
                finalDescriptionTextBox.Text = descriptionTextBox.Text;

                String details = String.Empty;
                foreach ( var sched in _currentScheds )
                    details += " " + sched.GetDetails();
                if ( details.Length != 0 )
                    details = details.Remove(0, 1);
                finalTriggerTextBox.Text = details;
                finalTaskTypeTextBox.Text = taskTypeComboBox.Text;
            }
        }


        private bool SendAddRequest(out String error)
        {
            TaskInfo taskInfo = new TaskInfo {
                BusinessTaskName = taskTypeComboBox.Text.Trim(),
                Description = descriptionTextBox.Text.Trim(),
                Name = taskNameTextBox.Text.Trim(),
                UserName = _client.UserName,
                Active = true
            };
            foreach ( var sched in _currentScheds )
                taskInfo.AddSchedule(sched);

            //NewTask = taskInfo;

            try {
                return _client.AddTask(taskInfo, out error);
            } catch ( Exception exception ) {
                MessageBox.Show("Потеряно соединение с сервером. Приложение будет закрыто",
                    "Ошибка соединения!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                error = null;
                this.Close();
                return false;
            }
        }


        private void nextButton_Click(object sender, EventArgs e)
        {
            if ( _currentPanel.Name == nameDescriptionPanel.Name ) {
                if ( !CheckTaskName() )
                    return;
                ToggleNameDescriptionPanel();
                ToggleCreateTriggerPanel();
            } else if ( _currentPanel.Name == createTriggerPanel.Name ) {

                if ( !ChechSchedsActivity() ) {
                    MessageBox.Show("Нужно выбрать хотя бы одно активное расписание",
                        "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                ToggleCreateTriggerPanel();
                ToggleTaskTypesPanel();

            } else if ( _currentPanel.Name == taskTypesPanel.Name ) {
                ToggleTaskTypesPanel();
                ToggleAddTaskFinishPanel();
            } else // if ( _currentPanel.Name.Equals(
                   // addTaskFinishPanel.Name) ) 
            {
                String error;
                if ( !SendAddRequest(out error) )
                    MessageBox.Show(error, "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                else {
                    MessageBox.Show("Задача успешно добавлена.", "Успех!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }


        private void prevButton_Click(object sender, EventArgs e)
        {
            if ( _currentPanel.Name.Equals(
                createTriggerPanel.Name) ) {
                ToggleCreateTriggerPanel();
                ToggleNameDescriptionPanel();
            } else if ( _currentPanel.Name.Equals(
                taskTypesPanel.Name) ) {
                ToggleTaskTypesPanel();
                ToggleCreateTriggerPanel();
            } else // if ( _currentPanel.Name.Equals(
                   // addTaskFinishPanel.Name) )
            {
                ToggleAddTaskFinishPanel();
                ToggleTaskTypesPanel();
            }
        }


        private void createScheduleButton_Click(object sender, EventArgs e)
        {
            var childForm = new NewScheduleForm();
            childForm.ShowDialog();

            if ( childForm.DialogResult == DialogResult.OK ) {

                var sched = childForm.ScheduleInfo;
                sched.id = _currentScheds.Count;

                _currentScheds.Add(sched);

                ListViewItem item = new ListViewItem(sched.Name);
                item.Name = sched.id.ToString();
                item.SubItems.Add(sched.Description);
                item.SubItems.Add(sched.TriggerName);
                item.SubItems.Add(sched.Active.ToString());
                item.SubItems.Add(sched.GetDetails());
                this.schedListView.Items.Add(item);
            }
        }


        private void deleteSchedButton_Click(object sender, EventArgs e)
        {
            foreach ( ListViewItem v in schedListView.SelectedItems ) {
                _currentScheds.RemoveAt(
                    _currentScheds.FindIndex((x) => x.id.ToString() == v.Name)
                );
                schedListView.Items.Remove(v);
            }
        }


        private bool CheckTaskName()
        {
            if ( taskNameTextBox.Text.Length == 0 ) {
                MessageBox.Show("Введите имя задачи", "Оповещение!",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            return true;
        }



        private bool ChechSchedsActivity()
        {
            int count = 0;
            foreach ( ListViewItem item in schedListView.Items )
                if ( item.Checked )
                    ++count;

            if ( count < 1 )
                return false;

            return true;
        }



        private void schedListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            var found = _currentScheds.Find((x) => x.id.ToString() == e.Item.Name);
            if ( found != null ) {
                if ( e.Item.Checked )
                    found.Active = true;
                else
                    found.Active = false;
            }


            if ( !ChechSchedsActivity() )
                MessageBox.Show("Нужно выбрать хотя бы одно активное расписание",
                    "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
